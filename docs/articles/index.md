# Articles

Planned publications : 

<!--
2023 : 

- [Modern robotics : Application to digital fabrication (IEEE)](./modern.robotics.df.md)
- [Closed loop control of multi-material fused deposition modeling through real-time simulation (Additive manufacturing letters)](./closed.loop.MMFDM.md)

2024 : 

- [CraftGym : An additive manufacturing simulation framework for renforcement learning (Additive manufacturing)](./fa-gym.md)
- Real-time lava flow simulation on GPU (?)
- Real-time sedimentary erosion simulation on GPU (?)

-->


2024 : 

- [Fast Meshfree Simulation of the Debinding Process Using SPH and Position-Based Dynamics for Manufacturing Optimization](./debind.md)
