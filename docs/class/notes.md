# Introduction au Langage R

R est un logiciel disponible sur les systèmes d'exploitation Linux et Windows, utilisé notamment pour l'analyse de données. Durant ce cours, dispensé par Jonathan Lenoir de l'unité edysan (Ecologie et Dynamique des Systèmes Anthropisés), nous explorerons les bases de l'analyse de données avec le logiciel R.


## Question :
- Peut on charger un fichier en lecture seul ?
- Peut on charger des fichier depuis un repo ?
- Les calculs sont assez long même sur des petits datasets
  - Progression des calculs ?
  - R est il basé sur des fonction CPP ?
  - Si oui, peut-on les appelé direct depuis un code cpp ?
-> Julia et python vont être peut plus recomandé pour des gros set.
package lidR -> pour le traitement des lidar.








## Introduction

R est un langage interprété et orienté objet, ce qui signifie qu'il n'est pas compilé et qu'il permet de manipuler des objets dans un environnement interactif. Il offre une fenêtre de commande pour envoyer des instructions et une fenêtre graphique pour visualiser les résultats. Bien que RStudio offre un environnement de développement plus complet, il peut paraître plus complexe au premier abord.

R dispose d'un vaste écosystème de modules ou packages alimentés par une communauté active d'utilisateurs et de développeurs. Cela permet d'étendre les fonctionnalités de base de R selon les besoins spécifiques des utilisateurs. De plus, R offre la possibilité de générer des rapports en différents formats, y compris en Rmarkdown ou en PowerPoint.

Pour quitter R, il suffit d'utiliser la commande `q()`. Par défaut, RStudio stocke les informations utilisateur sur l'interface utilisateur (UI layout), ce qui peut rapidement occuper de l'espace. Cette fonctionnalité peut être désactivée dans les options globales. De plus, il est possible de configurer une marge dans les paramètres pour que le code revienne automatiquement à la ligne.

Pour exécuter une portion de code, il suffit de sélectionner le code à exécuter. Cependant, l'utilisation de chemins d'accès absolus peut rendre la compatibilité entre différents environnements impossible. Il est donc recommandé d'utiliser le concept de projet dans RStudio, ce qui permet de choisir l'emplacement de sauvegarde et de garantir une meilleure portabilité du code.

Dans la structure des dossiers d'un projet, il est intéressant de séparer les données brutes des données produites afin de faciliter la gestion et la traçabilité des transformations effectuées. En effet, le workflow d'analyse de données est crucial en science des données, d'où la nécessité d'avoir une traçabilité des transformations sans altérer les données d'origine.

## Manipulation des Données

Lors de l'analyse de données, il est souvent nécessaire de manipuler des valeurs manquantes (NA) ou des valeurs aberrantes. La fonction `mean` permet de calculer la moyenne d'un ensemble de données, avec la possibilité de "tronquer" les valeurs aberrantes en utilisant l'argument `trim`. Par exemple, en définissant `trim` à 0.25, seules les valeurs situées à 25% à gauche et à droite de la moyenne seront prises en compte.

R est compatible avec C++, ce qui permet d'utiliser du code C++ dans R via le package `Rcpp`. Cela ouvre la possibilité d'exécuter des fonctions R à l'intérieur du code C++, offrant ainsi une grande flexibilité dans le développement de logiciels statistiques complexes.


```R
# Exemple de Code avec Commentaires

# Affectation d'une valeur à une variable
myvar <- 5

# Affichage de la valeur de la variable
print(myvar)

# Affichage du répertoire de travail
getwd()

# Affichage du contenu du répertoire
dir()

# Tracé d'un graphique simple
plot(1:20/2)

# Commentaires expliquant l'affectation d'une valeur à une variable
# myvar <- 5
# myvar = 5

# Affichage des objets déjà créés
ls()

# Suppression de la variable myvar
rm(myvar)

## Opérateur %/% pour la division entière
## Opérateur %% pour le modulo
## Opérateur %*% pour la multiplication de matrices

## Définition du répertoire de travail
setwd()
```

Ce code contient des exemples d'instructions en langage R, accompagnés de commentaires explicatifs pour clarifier leur fonctionnement.


## Manipulation des Jeux de Données

R dispose de jeux de données intégrés, accessibles via la fonction `data()`. Par exemple, le jeu de données "pressure" peut être chargé en utilisant la commande `data(pressure)`. Ces jeux de données sont souvent structurés sous forme de data frames, qui sont des tableaux hétérogènes de données. Contrairement aux matrices, les data frames peuvent contenir différents types de données dans chaque colonne.

Pour explorer la structure d'un objet en R, la fonction `str()` est utile. Elle permet de visualiser la structure de l'objet, ce qui est particulièrement pratique lors de l'exploration de jeux de données complexes.

En résumé, lors de l'utilisation de R pour l'analyse de données, il est essentiel de comprendre les bases de la manipulation des données, y compris la gestion des valeurs manquantes et des valeurs aberrantes, ainsi que la structure des jeux de données. De plus, la compatibilité avec d'autres langages comme C++ offre des opportunités pour étendre les fonctionnalités de R et créer des solutions analytiques plus avancées.

```R
# Exemple de code avec description des fonctions

# Calcul de la moyenne en tronquant les valeurs aberrantes
# Arguments:
#   - x: Vecteur de données
#   - trim: Pourcentage de valeurs à tronquer des deux extrémités (par défaut = 0)
# Renvoie la moyenne tronquée
mean_trim <- function(x, trim = 0) {
  return(mean(x, trim = trim))
}

# Utilisation de la fonction mean_trim avec un jeu de données
data(pressure)
pressures <- pressure$pressure
mean_pressure <- mean_trim(pressures, trim = 0.25)
print(mean_pressure)
```

Ce code illustre une fonction `mean_trim` qui calcule la moyenne d'un vecteur de données en tronquant un pourcentage spécifié de valeurs aberrantes des deux extrémités. Ensuite, cette fonction est utilisée pour calculer la moyenne tronquée des pressions atmosphériques dans le jeu de données "pressure".


## Statistics on data frame

```R
# Exemple de Code avec Fonctions Statistiques Utiles

# Création d'un vecteur de données
data <- c(2, 4, 6, 8, 10, 12)

# Calcul du minimum
min_value <- min(data)
print(paste("Minimum:", min_value))

# Calcul du maximum
max_value <- max(data)
print(paste("Maximum:", max_value))

# Calcul de l'écart-type
standard_deviation <- sd(data)
print(paste("Écart-type:", standard_deviation))

# Calcul de l'erreur standard
standard_error <- standard_deviation / sqrt(length(data))
print(paste("Erreur standard:", standard_error))

# Calcul de la moyenne
mean_value <- mean(data)
print(paste("Moyenne:", mean_value))

# Calcul de la variance
variance <- var(data)
print(paste("Variance:", variance))

# Calcul de la médiane
median_value <- median(data)
print(paste("Médiane:", median_value))

# Calcul du mode
# Fonction Mode personnalisée
mode <- function(x) {
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}
mode_value <- mode(data)
print(paste("Mode:", mode_value))
```

Ce code présente des exemples de calculs statistiques courants réalisés sur un ensemble de données, notamment le minimum, le maximum, l'écart-type, l'erreur standard, la moyenne, la variance, la médiane et le mode.

## Tests statistique

C'est quoi le test de student ?

Pour executer le test de student utiliser la fonction 

```R
t.test(pressure$temperature, mu = 0)
```
Result:
```R
One Sample t-test

data:  pressure$temperature
t = 6.9714, df = 18, p-value = 1.641e-06
alternative hypothesis: true mean is not equal to 0
95 percent confidence interval:
 125.7544 234.2456
sample estimates:
mean of x 
      180  
```

Exercice slide 48

Calculer la longueur des pétals du set de données IRIS

On importe le set de données 

```R
data(iris)
```

On calcul la moyenne de la longueur des pétales.

```R
mean_petalLength = mean(iris$Petal.Length)
```

On calcul ensuite l'écart type de la longueur : sd en anglais pour standard deviation

```R
sd_petalLength = sd(iris$Petal.Length)
```

On calcul la taille de l'échantillon en utilisant la fonction length(data)


Petal

```R
meanPetalLength = mean(iris$Petal.Length)
sd_PetalLength = sd(iris$Petal.Length) 
n_PetalLength = length(iris$Petal.Length)
sError_PetalLength = sd_PetalLength/sqrt(n_PetalLength) 
print(sError_PetalLength)
```

Sepal

```R
meanSepalLength = mean(iris$Sepal.Length)
print(meanSepalLength)
sd_SepalLength = sd(iris$Sepal.Length)
print(sd_SepalLength)
n_SepalLength = length(iris$Sepal.Length)
print(n_SepalLength)
sError_SepalLength = sd_SepalLength/sqrt(n_SepalLength) 
print(sError_SepalLength)

```


On peut afficher les plots des valeurs des pétales et des sépales en utilisant la fonction suivante :

```R
par(mfrow=c(1,2))
hist(iris$Petal.Length)
hist(iris$Sepal.Length)
```

To stop the splittin behavior you can use the same command

```R
par(mfrow=c(1,1))
hist(iris$Petal.Length)
```

Nous devons maintenant tester si la longueur des pétales diffère de celles des sépales

```R
hist(iris$Petal.Length)
abline(v=mean(mean(iris$Petal.Length)), col="red")
```


```R
hist(iris$Sepal.Length)
abline(v=mean(mean(iris$Sepal.Length)), col="blue")
```


Finally : 

```R
par(mfrow=c(2,1))
hist(iris$Petal.Length, xlim=c(1,8))
abline(v=mean(mean(iris$Petal.Length)), col="red")

hist(iris$Sepal.Length, xlim=c(1,8))
abline(v=mean(mean(iris$Sepal.Length)), col="blue")
```
![](./images/stat.iris.png)

Pour verifier ce que l'on fait apparaitre on peut utiliser un test de Student apparaillée.

Il doit être apparaillé car ici les sépales et les pétales sont situés sur le même individu.

Pour simplifier le test on peut soustraire un échantillons du deuxième pour avoir une différence entre les deux échantillons.
Un test de student apparaillée sur 2 echantillons, il suffit de le faire sur un seul échantillon, la différence des deux.

Si on a pas fait de test d'égalité de variance avant le test de student, la fonction t.test va executer un test de Welsh.

```R
t.test(iris$Petal.Length, iris$Sepal.Length, paired=1 ,var.equal=FALSE)
```

On peut simplifier par :

```R
t.test(iris$Petal.Length - iris$Sepal.Length, paired=1 ,var.equal=FALSE)
```

On peut simplifier une regression linéaire mixte par un test de student.

Cela donne : 

```R
t = -22.813, df = 149, p-value < 2.2e-16
alternative hypothesis: true mean is not equal to 0
95 percent confidence interval:
 -2.265959 -1.904708
sample estimates:
mean of x
-2.085333
```

On remarque une grande valeur de p value ce qui indique une corrélation significative.

L'interval de confiance est centrée sur 2.08mm 
En moyen mes Sépales sont 2.08mm plus grand que les pétales 

Si on oublie de dire que les données sont apparaillées ,

Le resultat de 

```R
t.test(x=iris$Petal.Length, y=iris$Sepal.Length )
```

donnera

```R
        Welch Two Sample t-test

data:  iris$Petal.Length and iris$Sepal.Length
t = -13.098, df = 211.54, p-value < 2.2e-16
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 -2.399166 -1.771500
sample estimates:
mean of x mean of y
 3.758000  5.843333
```

Il y a bien une différence significative.

On ne peux pas faire un modèle liéaire simple car les données sont apparaillées.

Ici il faut faire un modèle mixte.

Si les donées sont du comtage il faut faire des modèles de poinsson.

Pour une distribution gaussienne.
Ici typiquement cela suis une régression linéaire d'une loi normal. Mais il faut dire que les pétales et sépales ne sont pas indépendant.

Dans la structure il faut donner une indication sur l'appareillement.
Dans le modèle mixte on va ajouter une variable aléatoire pour renseigner l'identifiant l'individu.

cela permet de farie des chose plus complexe. des covariable...
On pourrait prendre en compte la taille de la plante par exemple.

Modèle linéaire mixte bien plus utils pour les données de tout les jour.

Penser a réduire les format court à deux entrées, en format long  cad en trois collonne

Format court : 
| ID | Longeur Sépale | Longeur Pétale |
| -- | ------ | ---------- |
| 1  | 3.58  | 2.12 |
| 2  | 4.16  | 2.81 |


| ID | Valeur | Appendices |
| -- | ------ | ---------- |
| 1  | 3.58  | S |
| 1  | 2.12  | P |
| 2  | 4.16  | S |
| 2  | 2.81  | P |


On peut maintenant dans les données créer un identifiant pour passer en format long

```R
iris$ID <- seq(1:length(iris$Sepal.Length))
```

Mais les ID serait stocké comme des entier (int)

Donc pour eviter ca 

```R
iris$ID <- paste("p", seq(1:length(iris$Sepal.Length)), sep = "")
```

??? info
  Le séparateur par défaut est un espace. On peut donc indiqure sep="" pour l'enlever.

```R
> iris$ID <- paste("p", seq(1:length(iris$Sepal.Length)))
> head(iris$ID) 
[1] "p 1" "p 2" "p 3" "p 4" "p 5" "p 6"
> iris$ID <- paste("p", seq(1:length(iris$Sepal.Length)), sep="")
> head(iris$ID)
[1] "p1" "p2" "p3" "p4" "p5" "p6"
```


On va maintenant créer un nouveau jeu de données en utilisant la fonction Rbind

```R
new <- c(iris$ID, iris$ID)
```

equivalent à 

```R
new <- rep(iris$ID, 2)
```

Pour le transformer en dataFrame : 

```R
new <- as.data.frame(new)
```

Puis on renomme la colone

```R
colnames(new) <- "ID"
```

On ajoute les valeurs de longueurs

```R
new$Length <- c(iris$Petal.Length, iris$Sepal.Length)
```

On ajoute un appendice d'identifiacaiton petale sépale

```R
new$app <- c( rep("P", length(iris$Petal.Length)), rep("S", length(iris$Petal.Length))   )
```

La variable appendice est codé en character. Ils serait plus interressant de la codé en factor.

On va transformer le caractére en factor en utilisant 

```R
new$app <- as.factor(new$app)
```

On peut maintenant créer notre modèle.

```R
mod1 = lm(length~app, data=new)
summary(mod1)
```

Mais ce modèle est faux car il ne tient pas compte de l'apparaillement.

Pour cela nous avons besoin de la library LME4 installable en utilisant :

```R
install.packages("lme4")
library("lme4")
```

ensuite on peut utiliser la fonction :

```R
mod2 = lmer(Length ~ app + (1|ID), data=new)
```

## Types de données et objets

Il y différent types de données :

- logical (booléen : TRUE, FALSE)
- integer (entier : 1, 2, 3, 4, ...)
- double (0.1, 0.2, 0.3, ...)
- character (texte : "setosa", "versicolor", ...)

On peut creér des objets contenant plusieurs type d'objet différent.

On va maintenant travailler avec le set de données PlantGrowth

On peut utiliser la suite de commande suivante pour répondre à la première question

```R
data("PlantGrowth")
length(PlantGrowth)
```

On remarque que PlantGrowth a une taille de 2 col. 
Si on utilise la commande suivante : 

```R
str(PlantGrowth)
```

On remarque alors que la premiere colomne contient des poids (weights) et la seconde, des indentificaterus de groupe de type factor.

```R
str(PlantGrowth)
```

La première variable est un double et la seconde un factor (int)

La variable calitative prends les valeurs (ctrl, trt1, et 1)

```R
length(which(PlantGrowth$group == "ctrl"))
length(which(PlantGrowth$group == "trt1"))
length(which(PlantGrowth$group == "trt2"))
```

On peut aussi obtenir les niveau d'un factor en utilisant `levels(PlantGrowth$group)`

On peut réordoner les niveau d'un factor en utilisant `?relevel` 

On peut faire `dim(PlantGrowth)` pour avoir la dimension et `typeof(PlantGrowth)` pour avoir le type des objets.

On sait alors que chaque groupe posède 10 entrée. Ce qui correspond à a taille des poids qui est de 30

On peut aussi avoir les effectifs de chaque groupe en utilisant :

```R
table(PlantGrowth$group)
```


On peux ensuite isoler les groupes en utilisant : 

```R
group_ctrl = which(PlantGrowth$group == "ctrl")
group_trt1 = which(PlantGrowth$group == "trt1")
group_trt2 = which(PlantGrowth$group == "trt2")

mean(PlantGrowth$weight[group_ctrl])
[1] 5.032
mean(PlantGrowth$weight[group_trt1])
[1] 4.661
mean(PlantGrowth$weight[group_trt2])
[1] 5.526
which(PlantGrowth$group == "ctrl")
```

Pour appliquer les fonctions à tous les groueps d'un coup on peut utiliser la fonction `tapply`

```R
tapply(PlantGrowth$weigth, PlantGrowth$group, mean)
```



On peut utiliser le test suivant : 

```R
t.test(PlantGrowth$weight[group_trt1], PlantGrowth$weight[group_ctrl], var.equal = 0)


data:  PlantGrowth$weight[group_ctrl] and PlantGrowth$weight[group_trt2]
t = -2.134, df = 16.786, p-value = 0.0479
alternative hypothesis: true difference in means is not equal to 0    
95 percent confidence interval:
 -0.98287213 -0.00512787
sample estimates:
mean of x mean of y
    5.032     5.526

```

Pour conclure il faudrait faire le test sur toutes les conbinaison des group (ctrl et trt1, ctrl et trt2 ...)

Cependant cela augmentrait la probabiliter d'obtenir un restulat significatif au test et donc ce n'est pas conlcuant. Il serait plus rigoureux d'étre plus sévere sur la singnificance des test ou d'utiliser un modelèle linéaire.

```R
> mod3 = lm(weight ~ group, data = PlantGrowth)
> summary(mod3) 

Call:
lm(formula = weight ~ group, data = PlantGrowth)

Residuals:
    Min      1Q  Median      3Q     Max
-1.0710 -0.4180 -0.0060  0.2627  1.3690

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)   5.0320     0.1971  25.527   <2e-16 ***
grouptrt1    -0.3710     0.2788  -1.331   0.1944
grouptrt2     0.4940     0.2788   1.772   0.0877 .
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Residual standard error: 0.6234 on 27 degrees of freedom
Multiple R-squared:  0.2641,    Adjusted R-squared:  0.2096
F-statistic: 4.846 on 2 and 27 DF,  p-value: 0.01591

```


On peut en conclure qu'il n'y a pas une difference sinificative entre les séries ctrl et trt

## Tensors

A vector could be created using the following formula :

```R
vec <- seq(from = 1, to = 12, by 1)
```

In this exercices we need to create a vector using a binomial law : 

```R
mat <- rbinom(n = 9, size = 1, prob = 0.5) 
```

Then we can transform the value into boolean using

```R
mat <- as.logical(mat) 
```

Then to output the matrix in a 3x3 array :

```R
dim(mat) = col(3,3) 
```

Pour eviter de faire des boucles est ainsi acceler le calcul, on peut utiliser les fonction tapply... Pour executer des calculs sur els lignes ou les group de tableau.





Tidy verse.


D'accord, voici un exemple de code en R pour naviguer jusqu'à un dossier de travail et charger un fichier xlsx. Assurez-vous d'avoir les bibliothèques nécessaires installées (`readxl` pour lire les fichiers Excel).

### Étape 1: Installer les bibliothèques nécessaires (si elles ne sont pas déjà installées)

```R
install.packages("readxl")
```

### Étape 2: Charger les bibliothèques nécessaires

```R
library(readxl)
```

### Étape 3: Définir le dossier de travail

Vous pouvez définir le dossier de travail en utilisant `setwd()`. Assurez-vous de spécifier le chemin correct vers le dossier contenant votre fichier xlsx.

```R
setwd("/chemin/vers/votre/dossier")
```

### Étape 4: Charger le fichier xlsx

Utilisez la fonction `read_excel()` de la bibliothèque `readxl` pour charger le fichier xlsx. Remplacez `"votre_fichier.xlsx"` par le nom de votre fichier.

```R
data <- read_excel("votre_fichier.xlsx")
```

### Exemple complet

Voici un exemple complet rassemblant toutes les étapes:

```R
# Installer la bibliothèque readxl (à faire une seule fois)
install.packages("readxl")

# Charger la bibliothèque readxl
library(readxl)

# Définir le dossier de travail
setwd("/chemin/vers/votre/dossier")

# Charger le fichier xlsx
data <- read_excel("votre_fichier.xlsx")

# Afficher les premières lignes du fichier chargé
head(data)
```

Bien sûr, je vais expliquer comment écrire du code de manière simplifiée en utilisant le module `tidyverse` en R, avec l'exemple que vous avez fourni.

### Notation Traditionnelle

Dans la notation traditionnelle, chaque étape de transformation des données est effectuée séparément et stockée dans une nouvelle variable :

```r
egg_salt <- crack(egg, add_seasoning) 
egg_mix <- beat(egg_salt)
omelette <- cook(egg_mix, add_chives)
```

### Notation avec le Pipe Operator (`%>%`)

Le package `magrittr`, qui fait partie de `tidyverse`, introduit l'opérateur pipe (`%>%`) pour simplifier la chaîne de transformations. Voici comment réécrire le même code en utilisant cet opérateur :

```r
egg %>%
  crack(add_seasoning) %>%
  beat() %>%
  cook(add_chives) -> omelette
```

### Explication

- **Opérateur Pipe (`%>%`)** : Cet opérateur permet de passer le résultat d'une fonction directement à la suivante, réduisant ainsi le besoin de variables intermédiaires.
- **Lecture Séquentielle** : Le code se lit de haut en bas, ce qui rend la séquence des opérations plus claire et plus intuitive.
- **Écriture Concise** : En utilisant l'opérateur pipe, le code devient plus concis et élimine le besoin de créer des variables intermédiaires.

### Comparaison

1. **Code Traditionnel** :
    - Plus verbeux.
    - Chaque étape est stockée dans une variable intermédiaire.
    - La séquence des opérations est moins évidente à suivre.

    ```r
    egg_salt <- crack(egg, add_seasoning) 
    egg_mix <- beat(egg_salt)
    omelette <- cook(egg_mix, add_chives)
    ```

2. **Code avec Pipe** :
    - Plus concis.
    - Les transformations sont enchaînées de manière claire et fluide.
    - Facilite la lecture et la maintenance du code.

    ```r
    egg %>%
      crack(add_seasoning) %>%
      beat() %>%
      cook(add_chives) -> omelette
    ```

### Visualisation de la Séquence

Le diagramme que vous avez fourni illustre bien cette séquence de transformations :

```r
egg %>%
  crack(add_seasoning) %>%
  beat() %>%
  cook(add_chives) -> omelette
```

Chaque étape de transformation prend l'entrée du résultat précédent et applique la fonction spécifiée, facilitant ainsi la compréhension du flux de données.

### Conclusion

L'utilisation de l'opérateur pipe (`%>%`) dans le `tidyverse` simplifie grandement l'écriture et la lecture du code R en rendant la séquence des opérations plus intuitive et plus facile à suivre. Cela est particulièrement utile pour les manipulations de données complexes.


Pour importer des fichiers Excel en sélectionnant des feuillets spécifiques en R, vous pouvez utiliser le package `readxl`. Voici comment le faire.

### Étape 1: Installer et charger le package `readxl`

```r
install.packages("readxl")
library(readxl)
```

### Étape 2: Lire un fichier Excel et sélectionner un feuillet spécifique

Vous pouvez utiliser la fonction `read_excel()` en spécifiant le nom du fichier et le nom ou le numéro du feuillet que vous souhaitez importer.

#### Exemple 1: Sélectionner un feuillet par son nom

```r
data <- read_excel("votre_fichier.xlsx", sheet = "NomDuFeuillet")
```

#### Exemple 2: Sélectionner un feuillet par son numéro

```r
data <- read_excel("votre_fichier.xlsx", sheet = 2)
```

### Étape 3: Afficher les premières lignes du fichier importé

```r
head(data)
```

### Exemple complet

```r
# Installer la bibliothèque readxl (si nécessaire)
install.packages("readxl")

# Charger la bibliothèque readxl
library(readxl)

# Lire un fichier Excel et sélectionner un feuillet par nom
data <- read_excel("votre_fichier.xlsx", sheet = "NomDuFeuillet")

# Lire un fichier Excel et sélectionner un feuillet par numéro
data <- read_excel("votre_fichier.xlsx", sheet = 2)

# Afficher les premières lignes du fichier importé
head(data)
```

Cette méthode vous permet de spécifier précisément quel feuillet d'un fichier Excel vous souhaitez importer dans R.


La fonction readExcel transforme automatiquement les séparateur en . (compatibilité international).
Package base ne prends pas en charge cette fonctionnalité.s



Ce code en R utilise les bibliothèques `tidyverse` et `readxl` pour lire et manipuler un fichier Excel. Voici une explication détaillée de chaque étape :

### Chargement des bibliothèques nécessaires

```r
library(tidyverse)
library(readxl)
```
- `tidyverse` : Un ensemble de packages R conçu pour la science des données, comprenant `dplyr`, `ggplot2`, `tidyr`, et plus.
- `readxl` : Un package pour lire des fichiers Excel.

### Lecture du fichier Excel

```r
adia <- read_excel("docs/class/adiabatique.xlsx")
print(head(adia))
```
- `read_excel("docs/class/adiabatique.xlsx")` : Lit le fichier Excel situé à l'emplacement spécifié et le stocke dans la variable `adia`.
- `print(head(adia))` : Affiche les premières lignes du dataframe `adia` pour un aperçu des données.

### Renommage des colonnes

```r
adia <- rename(adia, 
               T_deg = "TmoyAnn",
               Alt_m = "Alt",
               Massif = "MassMont",
               Xlb_m = "Xlamb2",
               Ylb_m = "Ylamb2")
```
- `rename(adia, ...)` : Renomme les colonnes du dataframe `adia` pour les rendre plus compréhensibles.
  - `T_deg = "TmoyAnn"` : Renomme la colonne `TmoyAnn` en `T_deg`.
  - `Alt_m = "Alt"` : Renomme la colonne `Alt` en `Alt_m`.
  - `Massif = "MassMont"` : Renomme la colonne `MassMont` en `Massif`.
  - `Xlb_m = "Xlamb2"` : Renomme la colonne `Xlamb2` en `Xlb_m`.
  - `Ylb_m = "Ylamb2"` : Renomme la colonne `Ylamb2` en `Ylb_m`.

### Manipulation des données

```r
adia %>%
  select(c(Massif, T_deg, Alt_m)) %>%
  group_by(Massif) %>%
  summarize(across(where(is.numeric), mean),
            N = n()) -> summary_massif
```
- `adia %>%` : Utilisation de l'opérateur pipe (`%>%`) pour enchaîner les transformations.
- `select(c(Massif, T_deg, Alt_m))` : Sélectionne les colonnes `Massif`, `T_deg` et `Alt_m` du dataframe `adia`.
- `group_by(Massif)` : Grouper les données par la colonne `Massif`.
- `summarize(across(where(is.numeric), mean), N = n())` : Crée un résumé des données groupées :
  - `across(where(is.numeric), mean)` : Calcule la moyenne de toutes les colonnes numériques dans chaque groupe.
  - `N = n()` : Compte le nombre d'observations dans chaque groupe.
- `-> summary_massif` : Assigne le résultat final à la variable `summary_massif`.

### Affichage du résumé

```r
print(summary_massif)
```
- Affiche le dataframe `summary_massif` contenant les moyennes des colonnes numériques et le nombre d'observations par `Massif`.

### Exemple complet

```r
library(tidyverse)
library(readxl)

# Lire le fichier Excel
adia <- read_excel("docs/class/adiabatique.xlsx")
print(head(adia))

# Renommer les colonnes
adia <- rename(adia, 
               T_deg = "TmoyAnn",
               Alt_m = "Alt",
               Massif = "MassMont",
               Xlb_m = "Xlamb2",
               Ylb_m = "Ylamb2")

# Manipuler les données
adia %>%
  select(c(Massif, T_deg, Alt_m)) %>%
  group_by(Massif) %>%
  summarize(across(where(is.numeric), mean),
            N = n()) -> summary_massif

# Afficher le résumé
print(summary_massif)
```

Ce code lit les données d'un fichier Excel, renomme certaines colonnes pour les rendre plus compréhensibles, sélectionne des colonnes spécifiques, groupe les données par `Massif`, calcule les moyennes des colonnes numériques et compte le nombre d'observations par groupe, et enfin affiche le résumé des données.

La personnalisation des graphiques dans `ggplot2` est une fonctionnalité puissante qui permet d'adapter l'apparence de vos visualisations selon vos besoins. Voici une explication détaillée des thèmes, des palettes et de la fonction `scale_fill`, ainsi que la manière de créer votre propre thème.

### Thèmes dans `ggplot2`

Les thèmes dans `ggplot2` contrôlent l'apparence globale de vos graphiques, y compris les polices, les couleurs de fond, les bordures, les titres et les légendes.

#### Utilisation de thèmes prédéfinis

`ggplot2` propose plusieurs thèmes prédéfinis que vous pouvez utiliser pour donner un style particulier à vos graphiques. Voici quelques exemples :

- **theme_gray()** : Thème par défaut.
- **theme_bw()** : Thème avec fond blanc.
- **theme_minimal()** : Thème minimaliste.
- **theme_classic()** : Thème classique avec des lignes claires et un fond blanc.

Exemple d'utilisation d'un thème :

```r
p + theme_minimal()
```

#### Création de votre propre thème

Vous pouvez créer votre propre thème en utilisant la fonction `theme()` et en personnalisant ses arguments. Voici un exemple :

```r
my_theme <- theme(
  panel.background = element_rect(fill = "white", color = NA),
  panel.grid.major = element_line(color = "gray80"),
  panel.grid.minor = element_line(color = "gray90"),
  axis.title = element_text(size = 14, face = "bold"),
  axis.text = element_text(size = 12),
  plot.title = element_text(hjust = 0.5, size = 16, face = "bold")
)

p + my_theme
```

### Palettes de couleurs

Les palettes de couleurs dans `ggplot2` permettent de contrôler les couleurs utilisées pour les éléments du graphique. Il existe plusieurs palettes intégrées que vous pouvez utiliser.

#### Utilisation de palettes intégrées

- **scale_fill_brewer()** : Utilise les palettes de ColorBrewer.
- **scale_fill_viridis()** : Utilise les palettes de viridis.

Exemple d'utilisation d'une palette de couleurs :

```r
p + scale_fill_brewer(palette = "Set3")
```

### Fonction `scale_fill`

La fonction `scale_fill` est utilisée pour contrôler la façon dont les couleurs de remplissage sont mappées aux données dans vos graphiques.

#### Utilisation de `scale_fill_manual`

Vous pouvez définir manuellement les couleurs à utiliser pour les remplissages avec `scale_fill_manual` :

```r
p + scale_fill_manual(values = c("red", "blue", "green"))
```

### Exemple complet avec personnalisation

Voici un exemple complet de personnalisation d'un graphique `ggplot2` :

```r
library(ggplot2)
library(dplyr)

# Exemple de données
data(mpg)
mpg_summary <- mpg %>%
  group_by(manufacturer) %>%
  summarize(cty_mean = mean(cty), hwy_mean = mean(hwy))

# Création du graphique
p <- ggplot(mpg_summary, aes(x = manufacturer, y = cty_mean, fill = manufacturer)) +
  geom_bar(stat = "identity") +
  labs(title = "Consommation moyenne en ville par fabricant", x = "Fabricant", y = "Consommation moyenne en ville (mpg)") +
  theme_minimal()

# Personnalisation avec une palette et un thème personnalisé
my_theme <- theme(
  panel.background = element_rect(fill = "white", color = NA),
  panel.grid.major = element_line(color = "gray80"),
  panel.grid.minor = element_line(color = "gray90"),
  axis.title = element_text(size = 14, face = "bold"),
  axis.text = element_text(size = 12),
  plot.title = element_text(hjust = 0.5, size = 16, face = "bold")
)

p + scale_fill_brewer(palette = "Set3") + my_theme
```

### Résumé

- **Thèmes** : Contrôlent l'apparence globale des graphiques.
  - Utilisez des thèmes prédéfinis (`theme_minimal()`, `theme_bw()`, etc.).
  - Créez vos propres thèmes en personnalisant les éléments (`theme()`).
- **Palettes de couleurs** : Contrôlent les couleurs utilisées dans les graphiques.
  - Utilisez des palettes intégrées (`scale_fill_brewer()`, `scale_fill_viridis()`).
  - Définissez manuellement les couleurs (`scale_fill_manual()`).
- **Fonction `scale_fill`** : Utilisée pour mapper les couleurs de remplissage aux données.

En utilisant ces outils, vous pouvez créer des graphiques `ggplot2` qui sont non seulement informatifs, mais aussi esthétiquement plaisants et adaptés à vos besoins spécifiques.


Ce code crée un graphique en utilisant `ggplot2` pour visualiser la relation entre l'altitude (`Alt_m`) et la température (`T_deg`) pour différents massifs montagneux. Il utilise plusieurs fonctionnalités de personnalisation pour améliorer la lisibilité et l'esthétique du graphique. Voici une explication détaillée de chaque partie du code et de son utilité :

### Code Complet

```r
ggplot(adia_extended, aes(x = Alt_m, y = T_deg, col = Massif, fill = Massif)) +
  geom_point(alpha = 0.1) +
  geom_smooth(method = "lm", formula = y ~ poly(x, 1), se = FALSE) + 
  scale_fill_manual(values = palette(),
                    name = "Massif",
                    labels = c("Alpes", 
                               "Corse", 
                               "Jura",
                               "Massif Central",
                               "Pyrénées",
                               "Vosges")) +
  labs(title = "Température en fonction de l'altitude par massif",
       x = "Altitude (m)", y = "Temperature (°)") +
  theme_classic() -> p2
```

### Explication Détailée

1. **Initialisation de `ggplot`**

    ```r
    ggplot(adia_extended, aes(x = Alt_m, y = T_deg, col = Massif, fill = Massif))
    ```

    - **`ggplot(adia_extended, aes(...))`** : Crée un objet `ggplot` en utilisant le dataframe `adia_extended`.
    - **`aes(x = Alt_m, y = T_deg, col = Massif, fill = Massif)`** : Définit les mappings esthétiques :
        - `x = Alt_m` : Altitude (`Alt_m`) sur l'axe des x.
        - `y = T_deg` : Température (`T_deg`) sur l'axe des y.
        - `col = Massif` : Couleur des points en fonction du massif.
        - `fill = Massif` : Remplissage des éléments en fonction du massif (utile pour certains géométries comme `geom_bar`).

2. **Ajout des points**

    ```r
    geom_point(alpha = 0.1)
    ```

    - **`geom_point(alpha = 0.1)`** : Ajoute des points au graphique avec une transparence (`alpha`) de 0.1 pour éviter une superposition excessive des points.

3. **Ajout d'une courbe de tendance**

    ```r
    geom_smooth(method = "lm", formula = y ~ poly(x, 1), se = FALSE)
    ```

    - **`geom_smooth(method = "lm", formula = y ~ poly(x, 1), se = FALSE)`** : Ajoute une ligne de tendance utilisant une régression linéaire (`lm`).
        - `method = "lm"` : Utilise la méthode de régression linéaire.
        - `formula = y ~ poly(x, 1)` : Spécifie la formule pour la régression (polynôme de degré 1, ce qui équivaut à une régression linéaire simple).
        - `se = FALSE` : Désactive l'affichage de l'intervalle de confiance autour de la ligne de tendance.

4. **Personnalisation des couleurs de remplissage**

    ```r
    scale_fill_manual(values = palette(),
                      name = "Massif",
                      labels = c("Alpes", 
                                 "Corse", 
                                 "Jura",
                                 "Massif Central",
                                 "Pyrénées",
                                 "Vosges"))
    ```

    - **`scale_fill_manual(values = palette(), ...)`** : Définit manuellement les couleurs de remplissage pour chaque massif.
        - `values = palette()` : Spécifie les couleurs à utiliser (vous devez définir la fonction `palette()` ailleurs dans votre code).
        - `name = "Massif"` : Nom de la légende pour le remplissage.
        - `labels = c(...)` : Définit les étiquettes de la légende pour chaque massif.

5. **Ajout des étiquettes et du titre**

    ```r
    labs(title = "Température en fonction de l'altitude par massif",
         x = "Altitude (m)", y = "Temperature (°)")
    ```

    - **`labs(title = ..., x = ..., y = ...)`** : Ajoute un titre et des étiquettes aux axes.
        - `title` : Titre du graphique.
        - `x` : Étiquette de l'axe des x.
        - `y` : Étiquette de l'axe des y.

6. **Application d'un thème**

    ```r
    theme_classic()
    ```

    - **`theme_classic()`** : Applique le thème classique avec un fond blanc et des lignes de grille minimales.

7. **Stockage du graphique dans un objet**

    ```r
    -> p2
    ```

    - **`-> p2`** : Assigne le graphique créé à l'objet `p2` pour une utilisation ultérieure.

### Utilité

- **Visualisation des données** : Montre comment la température varie en fonction de l'altitude pour différents massifs.
- **Analyse de tendance** : Ajoute une ligne de tendance pour chaque massif pour observer les relations linéaires.
- **Personnalisation** : Utilise des couleurs spécifiques pour chaque massif et personnalise l'apparence générale du graphique pour le rendre plus clair et esthétique.

Résultat : 

![](./images/plot.end.png)


Pour afficher plusieurs datasets sur un même plot on peux définir le jeu de données du premier ggplot comme null et préciser les données pour chaque fonction de plot suivante :

```R
ggplot(NULL, aes(x = Alt_m, y = T_deg, col = Massif)) +
  geom_point(data = adia, alpha = 0.1) +
  geom_smooth(data = adia, method = "lm", formula = y ~ poly(x, 1), se = FALSE) + 
  scale_fill_manual(values = palette(),
                    name = "Massif",
                    labels = c("Alpes", 
                               "Corse", 
                               "Jura",
                               "Massif Central",
                               "Pyrénées",
                               "Vosges")) +
  labs(title = "Température en fonction de l'altitude par massif",
       x = "Altitude (m)", y = "Temperature (°)")  +
  geom_point(aes(x = Alt_m_mean, y = T_deg_mean), data = summary_massif, inherit.aes = FALSE) +
  theme_classic() -> p2
```

Resultat : 

![](./images/plot.end2.png)


---

## Cours du 01/07/2024

ggrepel : un library pour repousser des label automatique. (gource)



# Notes de cours : Analyse de la biodiversité des haies en Picardie avec R

## Introduction
Dans ce cours, nous allons explorer des notions avancées en utilisant les données d'une étude sur la biodiversité des haies en Picardie. L'objectif est de manipuler ces données pour réaliser une analyse en composantes principales (ACP), ce qui nous permettra de créer des modèles simplifiés de la répartition de la biodiversité en fonction des propriétés des haies.

### Bibliothèques nécessaires

Nous allons utiliser plusieurs bibliothèques pour cette analyse :
```r
library(tidyverse)
library(kableExtra)
library(ggplot2)
library(ggrepel)
library(patchwork)
library(sf)
library(leaflet)
library(ade4)
library(factoextra)
library(lme4)
library(MuMIn)
```

### Chargement et exploration des données
Nous commençons par charger les données de l'étude sur la biodiversité des haies :
```r
haie <- read.table("https://figshare.com/ndownloader/files/18058913", header = TRUE, sep = "\t")

print(dim(haie))
print(class(haie))
```
Les dimensions et la classe des données sont affichées pour vérification.

### Conversion en tibble
Nous convertissons ensuite les données en tibble pour faciliter la manipulation :
```r
haie %>%
  as_tibble() -> haie

print(class(haie))
```

### Transformation des coordonnées
Nous extrayons les coordonnées des haies et les transformons en un système de coordonnées géographiques (WGS84) :
```r
haie_coord_l93 <- tibble(id = haie$id_haie, 
                         lon = haie$l93_x, 
                         lat = haie$l93_y,
                         ann = haie$annee_med)

haie_coord_l93 %>%
  st_as_sf(coords = c("lon", "lat")) %>%
  st_set_crs(2154) %>%
  st_transform(4326) -> haie_coord_lonlat
```

### Visualisation des haies sur une carte
Nous utilisons `leaflet` pour visualiser les haies sur une carte avec un fond d'image satellite :
```r
leaflet(width = "80%") %>%
  addProviderTiles("Esri.WorldImagery") %>%
  addMarkers(data = haie_coord_lonlat,
             popup = ~ as.character(ann)) -> carte_haies

print(carte_haies)
```
Le fond de carte "Esri.WorldImagery" est utilisé pour cette visualisation.

### Sélection des variables pour l'ACP
Nous sélectionnons les variables pertinentes pour l'ACP :
```r
haie %>%
  select(c(annee_med,
           h_moy,
           longueur,
           largeur,
           dist_for,
           surf_for_500,
           surf_for_1000,
           ipa_adj)) -> haie_acp
```
Nous ajoutons les identifiants des haies en tant que noms de lignes pour faciliter l'interprétation :
```r
rownames(haie_acp) <- haie$id_haie
```

### Réalisation de l'ACP
Nous effectuons une analyse en composantes principales avec les données sélectionnées :
```r
haie_acp %>%
  dudi.pca(scannf = FALSE, nf = 2) -> res_acp

plot_vp <- fviz_eig(res_acp, addlabels = TRUE)
plot(plot_vp)
```
L'ACP permet de représenter les données dans un espace de dimension réduite tout en conservant un maximum d'information.

### Personnalisation du graphique
Nous définissons un thème pour améliorer la lisibilité du graphique :
```r
mon_theme <- theme(panel.grid.major = element_blank(),
                   panel.grid.minor = element_blank(),
                   panel.background = element_rect(fill = "white"),
                   panel.border = element_rect(colour = "black", 
                                               fill = NA,
                                               size = 2),
                   plot.title = element_text(size = 18, 
                                             face = "bold", 
                                             hjust = 0.5),
                   plot.caption = element_text(size = 10,
                                               face = "italic", 
                                               color = "grey"),
                   legend.title = element_text(size = 12,
                                               face = "bold"),
                   legend.text = element_text(size = 12),
                   axis.title = element_text(size = 14, 
                                             face = "bold"),
                   axis.text = element_text(size = 12))
```
Nous appliquons ce thème au graphique de l'ACP :
```r
plot_vp <- fviz_eig(res_acp, 
                    addlabels = TRUE,
                    xlab = "Axes de l'ACP",
                    ylab = "Pourcentage de variance expliquée (%)",
                    title = "Diagramme en éboulis des valeurs propres",
                    caption = "Source : Lenoir et al. (2021)")

plot(plot_vp + mon_theme)
```

### Interprétation des résultats
Les deux premiers axes de l'ACP expliquent environ 50% de la variance totale des données, ce qui est une représentation significative de l'information contenue dans les variables originales.

## Conclusion
Cette analyse en composantes principales nous permet de simplifier la représentation de la biodiversité des haies en Picardie. En réduisant la dimensionnalité des données, nous mettons en valeur les aspects les plus significatifs, facilitant ainsi l'interprétation et la modélisation des propriétés des haies.