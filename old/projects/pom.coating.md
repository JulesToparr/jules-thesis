# Impression multimatière POM - PETG

- [x] POM coating of functionnal part to lower friction coef of 3D printed parts

This experiment aim to print simple 3D parts using two different materials : PLA and POM.
These two materials have their own physical properties : 

- PLA is easy to print, cheap and strong enough to print functionnal parts such as gearing or loaded part.
- POM is super dificult to print but have a very low friction coefficient compared to other polymer material. Also this material is very resistent to chemicals.

I think it may be interesting to print a part using PLA but using POM to print external wall. 
Printing plastic bearings in one shot seems to be a good application