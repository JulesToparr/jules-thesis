---
hide:
  - navigation
---

# :material-home-variant: Homepage

Welcome to my website. Here you will find all the information related to my PhD thesis.

My research focuses on digital fabrication, with a particular emphasis on multi-material 3D printing. In short, my objective involves the integration of diverse printing methodologies and materials within a singular part. Concurrently, I'm trying to simulate material deposition processes, thereby striving to achieve an autonomous optimization of the fabrication procedure while seeking for insight about defects causes.

| __Title__ | Multi-Material Additive Manufacturing : Optimization through Real-Time Simulation|
| --- | --- |
| __PhD candidate__ | [Jules TOPART](https://fr.linkedin.com/in/jules-topart)|
| __Supervisor__ | [Stéphane PANIER](https://fr.linkedin.com/in/st%C3%A9phane-panier-27009919)|
| __Laboratory__ | [Laboratoire de technologies innovantes d&#39;Amiens (LTI)](https://www.u-picardie.fr/lti/)|
| __Key words__ | additive manufacturing, mecanical engineering, computationnal physics, material science, robotics, machine learning|

[Read Thesis](./thesis/index.md){.md-button .md-button--primary}
[Source Code](https://github.com/JulesTopart/Merlin.git){.md-button}