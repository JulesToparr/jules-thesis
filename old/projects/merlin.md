# Merlin engine

Le moteur de calcul Merlin est une librarie C++ ayant pour but de rendre accessible l'utilisation des GPU (Graphics Processing Unit).

![Merlin Engine](http://spaceflightnow.com/wp-content/uploads/2015/02/1794708_10155072039545131_283095504902052383_n.jpg)


![GPU Computing curve](https://www.nvidia.com/content/dam/en-zz/fr_fr/es_em/Solutions/about-us/about_nvidia/nvidia-about-the-time-graph-843-u.png)


Tableau GPU - CPU
Physics simualtion reviews :

- FEM
- FDM
- FVM
- PBD
- XPBD
- SPH
- CFD
- Meshfree
- Holistique
- Neohookian ?

Software :

Flow3D
OpenFoam
Simulia
Digimat