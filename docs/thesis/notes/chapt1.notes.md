
# I. Multimaterial additive manufacturing
## 1. Background
### 1.1 Additive Manufacturing

#### 1.1.1 History and Evolution

Early development
1950s CNC machines developpement
1980s Charles Hull invent SLA 
1990s Selective laser sintering
2000s Advancements in Direct metal laser sintering (DMLS) and Electron beam melting (EBM)
2010s Emergence of Multi-material printing 

2020s Current state main usages

Useful in diverse applications (e.g., on-site fabrication, mass customization, complex shapes).
Significant impact in custom tooling, maintaining obsolete parts, and supporting decentralized manufacturing.

Influence of the Maker Movement on the popularization of 3D printing.

Evolution of AM techniques and their transition to MMAM.


High costs of supplies.
AM services to reduce the learning curve.

Depstite a very good press industrials are still struggling with business models for AM. Many machine manufacturers bets of high price on supplies such as resins and powders.
Those price even dissuading users from experimenting.
Adoption is slowed.

Other companies sells 3D printing as a service/ Freeing manufacturers from learning curve of new tech.



subtractive manufacturing more precise 
3D printing developped rapidly in schools, clubs and computers enthusiats through the Maker movement



#### 1.1.2 Core Principles and Technologies

Layer-by-Layer

Digital Design and Fabrication

Terminologies
SLA, FDM, EBM ...

3D slicing

Advanced slicing
5 axis or other kinematics


### 1.2 Multi-material Additive Manufacturing
#### 1.2.1 Definition and Importance

Multi-material additive manufacturing (MMAM) refers to the process of using multiple materials in a single print to create objects with varying properties and functionalities. This capability distinguishes MMAM from traditional single-material AM processes.

Dynamic vs static parts

Generalization of AM

Mechatronics

IOT

Intelligent systems

Functionnaly graded parts


#### 1.2.2 Key Techniques and Methods

Co-extrusion

Inkjet

Hybrid and toolchanging

---

## 2. Recent Advance
### 2.1 Technological innovation in MMAM
 
Real-time Monitoring and Feedback Systems

Convergence of Robotics and AM
Robot that print
Printer that adapt to perturbation

IoT embed sensors and data collection

Computer vision
Defect analysis and detectinon


### 2.2 Advances in Material Science

New Composite Materials
Filled filament

Smart Materials

Biocompatible and Biodegradable Materials

Inverse design of materials

Functionally Graded Materials (FGMs)

Overview: Materials with gradual changes in composition or structure over volume, leading to variations in properties.
Recent Innovations: Development of FGMs using additive manufacturing to create parts with tailored mechanical, thermal, or electrical properties.


Self-Healing Materials


Metal Matrix Composites (MMCs)

### 2.3 Interdisciplinary innovations

Artificial Intelligence and Machine Learning

Knowledge infused softwares


Blockchain for Supply Chain Transparency

Digital Twins


---

## 3. Challenges and Opportunities
### 3.1 Technical Challenges

content

### 3.2 Economic and Industrial Challenges

content

### 3.3 Opportunities

content

---

## 4. Theoritical background in MMAM
### 4.1 Fundamentals of Additive manufacturing
#### 4.1.1 Basic principles

Layer by layer, material considerations, feeding or using recipient

Digital workflow CAD to Slicer to machines

### 4.2 Key Theories and Models

#### 4.2.1 Material science principles

Solidification and Melting

Crystallography

Mechanical Properties

Thermal Properties

#### 4.2.2 Continuum mechanics

Stress strain, deformation failures...

#### 4.2.3 Thermomechanical modeling

Heat transfer

Thermal Effects

Thermomecanical coupling

Warping Prediction: Thermomechanical models are used to predict and prevent warping, a common defect in MMAM.
Optimization: These models help in optimizing printing parameters to achieve desired mechanical properties.

#### 4.2.4 Interfacial bonding

Adhesion and Cohesion: Bonding between different materials in MMAM is influenced by adhesion (surface bonding) and cohesion (internal bonding).

Diffusion and Chemical Compatibility: Diffusion of atoms and molecules across material interfaces and the chemical compatibility of materials are critical for strong bonding.

Surface Preparation: Proper surface preparation can enhance bonding by increasing surface energy and promoting better adhesion.

Material Properties: Intrinsic properties of the materials, such as their thermal expansion coefficients and melting points, affect bonding quality.

#### 4.2.5 Numerical Methods

##### 4.2.5.1 Finite Element Method (FEM)
Overview and applications in MMAM

##### 4.2.5.2 Finite Volume Method (FVM)
Overview and applications in MMAM

##### 4.2.5.3 Finite Difference Method (FDM)
Overview and applications in MMAM
Clarification: FDM (Finite Difference Method) vs. FFF (Fused Filament Fabrication) vs. FPD (Fused Pellet Deposition)

##### 4.2.5.4 Meshfree Methods
Discrete Element Method (DEM)
Overview and applications in MMAM
Smoothed Particle Hydrodynamics (SPH)
Overview and applications in MMAM
Material Point Method (MPM)
Overview and applications in MMAM
