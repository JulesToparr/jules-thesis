---
hide:
  - navigation
---


# Multi-Material Additive Manufacturing : Optimization through real-Time meshfree simulation

## Abstract
The world is currently facing many challenges, including global pandemics, rapid environmental change, social challenges and geopolitical issues. Moreover, it is important to note that these challenges are occurring at an alarming and accelerating pace.
The urgency of modern problems has gone from critical to urgent in the space of a few months, testing our capacity to adapt and our resilience.
Thus, the demand for new manufacturing processes and new materials is increasing accordingly.
The development of computational physics and machine learning offers the possibility of revealing opportunities for innovation among a vast amount of synthetic data. 
This thesis paves the way for a new methodology for the design and programming of additive manufacturing machines using rapid simulation of material shaping processes. I demonstrate an open simulation framework benefiting from GPU mass parallelization.

## Acknowledgments
## Introduction
   ### Global Challenges and the Need for Agile Manufacturing
   ### Multimaterial Additive Manufacturing (MMAM)
   ### Inverse Design in MMAM
   ### Simulation and Optimization in MMAM
   ### Contributions

## Multimaterial additive manufacturing
   ### Background
      - Recent advances
      - Challenges and opportunities
   ### Theoretical Background in Multimaterial Additive Manufacturing 
      - Materials Science and Engineering: Explore the properties and interactions of materials used in MMAM.
      - Overview of Multimaterial Processes: Detailed examination of various MMAM processes.
      - Powder Bed Fusion (PBF): Discuss the use of multiple powders in PBF and the challenges involved.
      - Material Jetting: Explore how multiple materials are jetted and cured in material jetting processes.
      - Fused Deposition Modeling (FDM) with Multiple Materials: Discuss the integration of different filaments in FDM.
      - Directed Energy Deposition (DED): Explore the use of multiple material feeds in DED processes.
      - Process Dynamics in MMAM: Dynamics of layer deposition, curing, sintering in MMAM.
   ### Principles of Inverse Design in Additive Manufacturing
      - Conceptual Framework: Fundamental concepts and methodologies of inverse design specific to AM.
      - Design for MMAM: Addressing inverse design considerations in multimaterial fabrication.

## Real-time simulation
   ### Constitutive equations
   ### Simulation methods in additive manufacturing
      - FEM
      - FVM
      - DEM
      - SPH
      - Position based dynamics
   ### Meshfree Methods: Principles and Applications
      - DEM Formulation
      - SPH Formulation
   ### Real-time simulation stability analysis
      - Integration scheme
      - Energy conservation

## Implementation
   ### Design and architecture
      - Modularity, performance and readability balance
   ### GPU mass parallelization
      - GPU fundamentals
      - Technical constraints and design rules
   ### Performance analysis

## Application
   ### Case study : Warping
   ### Problem definition and hypothesis
   ### Highlighting the influence of toolpath
   ### Simulation based optimization
## Conclusion and Future Work
## References
## Appendices









# General introduction

## Additive manufacturing post-pandemic boost

The world is currently facing numerous challenges, including global pandemics, rapid environmental transformations, social challenges, and geopolitical issues. However, it is important to note that these challenges are occurring at an alarming and accelerating rate.
The urgency of modern challenges has increased from critical to urgent within a matter of months, testing our adaptability and resilience.
In this context, as an intelligent species, we must understand, adapt, and communicate with our peers.

Focusing on the most popular crisis of the decade, the impact of the pandemic on the global industry has been significant, with both immediate and long-term consequences. The lockdown has disrupted global supply chains, changed consumer behaviour, and forced industries to adapt or reconfigure in the best cases. We will not cover the full impact of COVID-19 on the global economy here not only because this is a complex ongoing process but because we are focusing on additive manufacturing. However, it is important for the reader to understand the underlying motivation of this work.

In the post-COVID era, the landscape of additive manufacturing (AM) has evolved significantly, marked by several key trends and advancements. One of the major shifts has been the record installations of AM systems, driven by the validation of applications in sectors with stringent regulatory criteria, such as healthcare and aerospace. This growth in AM is further supported by an increased focus on scalability, higher throughput 3D printing systems, and enhanced reliability and repeatability. 

The pandemic has also highlighted the role of 3D printing in supporting improved healthcare responses and as a cornerstone for a more environmentally friendly future. The flexibility of 3D printing, along with its ability to produce parts on demand, has the potential to reduce waste and inventory, making it a key player in the move towards greener manufacturing processes. This shift is likely to result in shorter and more fragmented supply chains, with an increased emphasis on decentralized manufacturing and local microgrids of 3D-printing factories. [Source](https://www.nature.com/articles/s41578-020-00234-3)

Even if additive manufacturing is still used predominantly for prototyping activities, there's a significant acceleration in materials development for AM. The industry is continually discovering new applications, particularly in end-use parts, attracting attention from material companies and opening up new manufacturing applications. Strategic partnerships and acquisitions are also playing a key role in expanding materials expertise. In addition to material developments, there's an industry-wide focus on scaling up AM, with a capability to make production-grade parts faster and more productively. This capability enables businesses globally to integrate AM into their production operations more effectively. [Source](https://3dprintingindustry.com/news/what-is-the-future-of-3d-printing-80-additive-manufacturing-experts-forecast-3d-printing-trends-for-2023-220261/)


<!-- Lastly, the integration of AI and automation in additive manufacturing is a notable trend. The use of AI tools, like those demonstrated by Nvidia for creating 3D models from text input, is expected to see widespread applications in AM. This integration of AI with AM is anticipated to bring about significant changes in how AM is utilized, enhancing productivity and enabling new possibilities. [Source](https://3dprintingindustry.com/news/what-is-the-future-of-3d-printing-80-additive-manufacturing-experts-forecast-3d-printing-trends-for-2023-220261/)  -->


These trends collectively paint a picture of an AM landscape that is rapidly advancing and diversifying, with significant implications for industries ranging from healthcare to aerospace, and beyond.


## The recent advances in robotics

The advancements in AI, reinforcement learning (RL), and parallel simulation have significantly influenced the field of robotics, particularly in digital fabrication and manufacturing. These technologies are enabling a new era of robotic capabilities and applications.

1. **Reinforcement Learning in Robotics:** 
   - RL has seen some game-changing developments in recent years. These include advancements in offline RL and pre-training for robotics models, which have broadened the application scope of RL in robotics [](https://twimlai.com/podcast/twimlai/ai-trends-2023-reinforcement-learning-rlhf-robotic-pre-training-and-offline-rl/). For instance, offline RL enables learning new tasks from a handful of trials, significantly reducing the data requirements and training time for robotic systems. 

2. **Applications in Manufacturing and Logistics:** 
   - Machine learning algorithms, a crucial part of AI in robotics, have introduced automation and efficiency in manufacturing and logistics. These applications include automated pick and place, assembly and quality control, packaging and palletization, and inventory management【40†source】. For instance, robots equipped with machine learning capabilities can automate complex assembly tasks with precision, as well as perform quality control checks to ensure products meet required standards.

3. **Human-Robot Collaboration:**
   - Collaborative robots (cobots) represent a significant advancement in robotics, designed to work safely and cooperatively with humans in shared workspaces. Machine learning and AI are pivotal in facilitating this collaboration, enabling cobots to learn from human interactions, adapt to their surroundings, and refine task execution strategies over time【40†source】. These cobots can take over repetitive or hazardous tasks, allowing human workers to focus on more complex tasks.

4. **Predictive Maintenance and Self-Learning:**
   - AI and machine learning algorithms are increasingly used for predictive maintenance in robotics. By analyzing sensor data, these algorithms can predict potential equipment failures, thereby preventing downtime and costly repairs. Furthermore, self-learning capabilities in robots allow them to continuously improve their performance by learning from their experiences and adapting to changing environments and task requirements【40†source】.

These advancements are transforming the landscape of robotics, making them more adaptable, efficient, and capable of handling complex tasks with greater autonomy. The integration of AI, particularly through reinforcement learning and machine learning algorithms, is enabling robots to perform a wider range of tasks more effectively and in closer collaboration with humans. These technologies are not only enhancing the capabilities of robots but also revolutionizing the way they are integrated into manufacturing and other industries.








![](https://t3.ftcdn.net/jpg/00/93/64/02/360_F_93640214_thJJsHbHkPRn4UUd6ajmqbMoJqt101xk.jpg){.img-medium}

# Multi-Material Additive Manufacturing : Optimization through real-Time meshfree simulation

---

<h2>Abstract</h2>

The world is currently facing many challenges, including global pandemics, rapid environmental change, social challenges and geopolitical issues. Moreover, it is important to note that these challenges are occurring at an alarming and accelerating pace.
The urgency of modern problems has gone from critical to urgent in the space of a few months, testing our capacity to adapt and our resilience.
Thus, the demand for new manufacturing processes and new materials is increasing accordingly.
The development of computational physics and machine learning offers the possibility of revealing opportunities for innovation among a vast amount of synthetic data. 
This thesis paves the way for a new methodology for the design and programming of additive manufacturing machines using rapid simulation of material shaping processes. It demonstrate an open simulation framework benefiting from GPU mass parallelization and meshfree methods applied to multimaterial digital fabrication.

__Thesis Supervisor__ : [Prof. Stéphane PANIER](https://fr.linkedin.com/in/st%C3%A9phane-panier-27009919)

---

<h2>Acknowledgments</h2>

- Supervisor
- Unilasalle
- LTI
- Phd Students
- Externs
- Family

---

<h2>Table of content</h2>

1. Introduction
	- 1.1 Global Challenges and the Need for Agile Manufacturing
	- 1.2 The Rise of Additive Manufacturing During and Post-COVID-19
	- 1.3 Multimaterial Additive Manufacturing (MMAM)
	- 1.4 Inverse Design in MMAM
	- 1.5 Simulation and Optimization in MMAM
	- 1.6 Contributions
2. Multimaterial additive manufacturing
	- 2.1 Background
	- 2.2 Recent advances
	- 2.3 Challenges and opportunities
	- 2.4 Theoretical Background in Multimaterial Additive Manufacturing 
3. Real-time simulation
	- 3.1 Constitutive equations
	- 3.2 Simulation methods in additive manufacturing
	- 3.3 Meshfree Methods: Principles and Applications
	- 3.4 Real-time consideration
4. Implementation
	- 4.1 Design and architecture
	- 4.2 GPU mass parallelization
	- 4.3 Performance analysis
5. Application
	- 5.1 Case study : Warping
	- 5.2 Problem definition and hypothesis
	- 5.3 Highlighting the influence of toolpath
	- 5.4 Simulation based optimization
6. Conclusion and Future Work
7. References
8. Appendices

---

## Introduction

Th world is facing many hard problems. From climate crisis to global health issues, those challenges are directly reshaping the industrial landscape. Furthermore, great challenges seems to be going from urgent to absolute emergency in a matter of months, testing our capacity to adapt and our resilience. The industry is constrained both from the global challenges and changing comsuption habits, enlighting the imperative for agile manufacturing. Agility in this context is not an ideal objective but rather a vital necessity. 
The imperative for agile manufacturing has never been more pronounced. These challenges, encompassing both natural and human-made phenomena, necessitate a radical rethinking of traditional manufacturing processes. It underscores the importance of swift adaptation in manufacturing techniques to respond to the ever-evolving demands and constraints of our world. This dynamism is particularly relevant in the development of new materials and innovative products, which are critical in addressing these global issues. The ability to efficiently optimize resources, minimize waste, and rapidly pivot in response to changing circumstances is not just beneficial but essential for sustainability and resilience in our interconnected global landscape.

Additive manufacturing (AM), commonly known as 3D printing, stands out as the quintessential example of agile manufacturing processes. This innovative technique allows for the rapid production of intricate designs and prototypes, facilitating iterative development and quick response to market demands. Its ability to fabricate complex geometries with minimal material waste and tooling requirements exemplifies the agility sought after in modern manufacturing. Additionally, the flexibility of additive manufacturing enables customization and on-demand production, further enhancing its position as a leading method for agile manufacturing in various industries.

In the post-COVID era, the landscape of AM has evolved significantly, marked by several key trends and advancements. [@cutchergershenfel] [@Choong2020TheGR]
The pandemic has also highlighted the role of 3D printing in supporting improved healthcare responses and as a cornerstone for a more environmentally friendly future. The flexibility of 3D printing, along with its ability to produce parts on demand, has the potential to reduce waste and inventory, making it a key player in the move towards greener manufacturing processes. This shift is likely to result in shorter and more fragmented supply chains, with an increased emphasis on decentralized manufacturing and local microgrids of 3D-printing factories. 
One of the major shifts in the last 10 years has been the record installations of AM systems, driven by the validation of applications in sectors with critical regulatory criteria, such as healthcare and aerospace. This growth in AM is further supported by an increased focus on scalability, higher throughput 3D printing systems, and enhanced reliability and repeatability. 

Even if the number of patent and publication in the AM sector is exploding, additive digital fabrication remain challenging. Anisotropy, porosity, residual stresses, warping are common defects that remain a common issues despite many technical repsonse available. Furthermore, modern goods often contain electronics, actuaros and sensors, that need more than only structural parts. This brings us to the developpement of multimaterial additive manufacutring (MMAM). MMAM is not related to multicolor parts production, even if it could be challenging technically, there is no methodological bottleneck in the combination of several color in one print. Many machine publicly available on the market have this feature. But the true engineering revolution start when combining material that don't share the same physical properties. [@Tibbits2021ThingsFT]

To harness the full potential of MMAM, the concept of inverse design becomes crucial, shifting the focus from traditional design methods to a performance-driven approach that seeks optimal solutions through computational models. The concept of agility in manufacturing was introduced earlier, but it requires agility in the design process has well. To harness the full potential of MMAM, the concept of inverse design becomes crucial, shifting the focus from traditional design methods to a performance-driven approach that seeks optimal solutions through computational models. 
Inverse design is a method where you start from the desired product performance and characteristics and you use physical, mathematical or computational model to find an optimal solution.
Such methods are employed in the material engineering sector nowadays. Designing new molecules and materials while predicting their effect is now possible using data driven approach (eg. Generative AI, Physics informed NN...).
This is both a challenge and an opportunity for MMAM. Many material are developped leading to rapid obscolecence of manufacturing machines that may need to be updated quite often. Fortunately, inverse design could be employed to accelerate manufacturing machines and processes design.
This thesis is based on the idea that materials exist in a given form (liquid, solid, granular) and are transformed into a solid (or soft) object using a MMAM machine. The design process of the object is out of scope. However, the process that map the input material into the object is yet to be found. This could be solved as an optimization problem. 
However, implementing inverse design in MMAM presents its own set of challenges, including the need for machine-specific adjustments and managing the complexity of control systems, highlighting the intricate balance between innovation and practicality
Given the diverse and complex nature of multimaterial additive manufacturing (MMAM) processes, where each material and process necessitates tailored machinery, tools, and corresponding software algorithms, leveraging inverse design emerges as a compelling strategy. Inverse design, whether automated or manually executed, fundamentally depends on the empirical testing of models. Such experimentation, while invaluable, is often marked by considerable labor and financial costs. Thus, the integration of a rapid simulation methodology is deemed advantageous for both approaches, facilitating more efficient and cost-effective design optimization.

However, the quest for an appropriate fast simulation framework for MMAM poses significant challenges. The intricacies of MMAM processes—ranging from the heterogeneous material properties and their interactions to the specificities of the manufacturing process itself—demand a simulation tool capable of accurately capturing the complex behavior of multimaterial systems under various conditions. Moreover, the simulation framework must be versatile enough to accommodate the vast array of materials and processes utilized in MMAM, ensuring both high fidelity and computational efficiency.

The development of such a fast simulation framework requires a multidisciplinary approach, blending expertise from materials science, mechanical engineering, computer science, and applied mathematics. It necessitates advanced algorithms that can model the physical phenomena at play in MMAM processes, such as thermal dynamics, phase changes, and mechanical stresses, with a high degree of accuracy. Additionally, these algorithms must be optimized for performance, enabling rapid iteration and experimentation within a feasible timeframe and resource allocation.

To address these challenges, researchers and practitioners in the field of MMAM are exploring various strategies, including the application of generative design principles, machine learning techniques for predictive modeling, and the utilization of high-performance computing platforms. By harnessing these technologies, the aim is to create a simulation framework that not only accelerates the design process but also enhances the reliability and quality of multimaterial additive manufacturing outputs.

Building upon the recognition of the pivotal role that multimaterial additive manufacturing (MMAM) plays in the future of manufacturing, and acknowledging the inherent complexities and challenges involved in optimizing such processes, my contribution to this field is threefold. Firstly, I aim to establish comprehensive guidelines that delineate the future trajectory of additive manufacturing (AM), offering a strategic framework for navigating the evolving landscape of materials, technologies, and applications. These guidelines are intended to serve as a beacon for both current and future research and development efforts, fostering innovation and sustainability within the AM domain.

Secondly, I have developed a fast simulation methodology specifically tailored to the needs of AM processes, underpinned by a robust GPU computing framework. This innovation significantly enhances the efficiency and effectiveness of simulating AM processes, enabling rapid iteration and refinement of designs. By leveraging the computational power of GPUs, this framework facilitates the high-fidelity modeling of complex physical phenomena associated with AM, such as heat transfer, material deposition, and phase transformations, in a fraction of the time required by conventional simulation tools.

Lastly, my work encompasses the automated optimization of the AM process to specifically address and mitigate the defect of warping. Warping, a prevalent issue in AM that affects the dimensional accuracy and mechanical integrity of printed parts, represents a significant barrier to the reliability and repeatability of AM technologies. Through the development of an automated optimization algorithm, my research demonstrates the potential to significantly reduce warping, thereby enhancing the quality and consistency of AM-produced components. This contribution not only advances the state of the art in AM process optimization but also provides a tangible solution to one of the most pressing challenges in the field.

Together, these contributions represent a holistic approach to advancing the field of additive manufacturing, addressing both the theoretical and practical challenges that currently constrain its potential. By offering a comprehensive framework for future research, enhancing the efficiency of simulation methodologies, and providing a solution to a critical defect in AM, this work lays the groundwork for the next generation of additive manufacturing technologies and applications.

## Multimaterial additive manufacturing
### Background

Combining different material in a single part during a complex manufacturing process is indeed a great challenge. This approach allows for the creation of objects from multiple materials within a single manufacturing process, enabling the production of components with varied or gradual properties, such as mechanical, thermal, and electrical characteristics, in distinct regions of the same object. This capability stands in contrast to traditional additive manufacturing techniques, which typically involve the use of a single material.
The core principle of MMAM involves the precise deposition of different materials according to a pre-defined multi-material digital model. This process requires sophisticated control systems and advanced material handling mechanisms to switch between materials seamlessly during the printing process. Technologies utilized in MMAM include, but are not limited to, fused deposition modeling (FDM), selective laser sintering (SLS), and stereolithography (SLA), each adapted to handle multiple materials.
One of the primary challenges in MMAM is the need to ensure compatibility between different materials, particularly in terms of their processing conditions and the adhesion between layers of dissimilar materials. The interaction between materials can significantly affect the final product's structural integrity and functional performance. Therefore, considerable research within the field focuses on the development of compatible material systems and the optimization of process parameters to enhance inter-material bonding.
Despite its potential, the widespread adoption of MMAM faces several hurdles, including the high cost of multimaterial printers, the limited availability of compatible material pairs, and the complexity of designing for multimaterial fabrication. Ongoing research and development efforts are aimed at overcoming these challenges by enhancing the material compatibility, improving the precision and efficiency of MMAM processes, and developing more sophisticated design tools to fully exploit the capabilities of MMAM.



Innovation :

- Big area
- Robot assisted
- Support removal
- coating and conformal 3D printing
- Embed device
- MMAM
- Hybrid AM

Many innovation in additive manufacturing went popular in the last few years. Ranging from house 3D printing to 3D printed wearable embed device, new use case and brillant idea are poping worldwide. 
Among those innovation many involve custom machines or process. 
Even if the underlying langage is similar, each new manufacturing process may need a custom software to generate toolpath and trajectory.
Specialized and Hybrid MMAM machine rely on modified CAM software to minimize the developpement time usually spent on the process itself.
Has all processes are similarly controlled and as they all rely on 3D models. It would be very efficient to use a modular slicer, and write custom instructions for adaptation.

Generative design : 

- Design support for residual stress
- Reduce volume
- Solving engineering problems

Generative design is also a rapidly developping field. Software as NTop of the dassault system suit offer very powerwul tools to generate 3D shapeds and models given predeterminend rules. Those rules coul be used to maximize material in weak zone or general lattice to minimize volume and weight. It could be used also to match the 3 model to the stress sollicitation.


Materials characterization :

- Evaluate the material properties of the printed objects
- Evaluate the performance 
- Optimize prining parameters
  
Material are a fundamental part of the AM proces, for most of the known AM process, this involve material special treatment 


Design for AM :

- Design guide
- one shot Assembly
- Standards
- manufacturability, costs, reliability...


- Challenges and opportunities

As AM is a very versatile process, addressing whole challenges at once would be very difficult. However, give the number of publication in material design, new manufacturing process or method and new toolpath strategy. It will be benefical to have a framework used both to educate newbie and test new approach in order to implement them and evalutate.

In robotics, as the field advance rapidly, it is common to setup simulation environments along with the robot program in order to test fundamental constrol model or at least to understand the robots behavior and data related to it.

In robotics a typical workflow would be to setup ROS along with a simulator possibely Gazebo or Ros Sim, this could be used to vizualize constrol models and algorithms to enable trials and error empirical approach but also to generate synthetic data crucial to miniminze developpment time during the experimentals parts.

As Additive manufacturing grow in conplexity, the global machine architecture is more and more looking like a robot. And a modular architecture wouldn"t be surprising in the future. Such architecture started to developp in their very initial state such as Klipper.

Programming langage such as well known Gcode is widely used and defined as the strandart for both precision production and AM. However, AM programm files are usually very large and contain thousands of instructions letting no felxibility for adaptative control. 
function as fillHilbertSurface of extrudePerimeter may replace long list of instructions and give better support for in situ control and adaptation.

For instance, an advanced 3D printer could in the future decide if it is best to print the perimeter or the infill first given the current configuration of the part. Taking into account temperature variation, environment perturbations or even part deformation.

Relativity space is a Californian startup specialized in 3D printing robkect tank. Their approach is conceptually equivalent similar but different in the implementation.
Due to the large nature of the manufactured parts, they used industrials robot that are therefore following a given set of instructions. Their approach incolve simulating the whole manufacturing process and optimizing toolpath to improve the final part. In their process, the tank printed in metled metal depositions is deposited onto a spiralized outer peritmer of a circular shape. This shape will deforme after coolign due to the Thermal expansions of the materials.
This progressive deformation can easily be modeled and compensated. Even if the implementation of a such process is indeed impressive, the underlying technology and known and matestered by one.

Schéma archi OS vs Microprogramm Marlin. 

KLIPPER


Modeling :

- for quality
- for resulting mechanical property
- predict resisual stress

Modeling is a great way to both understand a physical behavior and make predictions. It is indeed a way to demonstrate the behavior of the studyed system. 

Modeling in AM is important to reduce defect, predict residual stress or part emchanical properties.
Many publications focus on developping materials models or simulation while validating them using experimentals results. Those results are crucial to adress technological challenges wisely.



### Theoretical Background in Multimaterial Additive Manufacturing

- Materials Science and Engineering: Explore the properties and interactions of materials used in MMAM.
- Overview of Multimaterial Processes: Detailed examination of various MMAM processes.
- Powder Bed Fusion (PBF): Discuss the use of multiple powders in PBF and the challenges involved.
- Material Jetting: Explore how multiple materials are jetted and cured in material jetting processes.
- Fused Deposition Modeling (FDM) with Multiple Materials: Discuss the integration of different filaments in FDM.
- Directed Energy Deposition (DED): Explore the use of multiple material feeds in DED processes.
- Process Dynamics in MMAM: Dynamics of layer deposition, curing, sintering in MMAM.

Despite the diversity inherent in additive manufacturing (AM) techniques, a common procedural schema is discernible across the spectrum of these processes, suggesting a principle that could be extrapolated to encompass all digital fabrication methods. Initially, a raw material is supplied as input in various phases (granular, liquid, or solid filament) each adapted to the specificities of the AM technique employed. Subsequently, under the guidance of a predefined program, the machine orchestrates the transformation of this material into a solid form. This emergent solid, although constituting the primary output, often necessitates further post-processing to meet the desired specifications and quality standards. This underlying uniformity underscores the potential for a unified framework within the field of digital fabrication, despite the apparent heterogeneity of materials and processes involved.

From the perspective of the user, the additive manufacturing (AM) process commences with the conceptualization of a desired part, encompassing specific requirements such as topology, quality, material properties, performance metrics, and stress resistance. This conceptual part is brought to virtual life using Computer-Aided Design (CAD) software, followed by the slicing of the CAD model through a software "slicer." The slicer generates a program, typically in GCode or a proprietary language, which instructs the AM machine on how to fabricate the part. Upon execution of this program, a physical manifestation of the part is produced, which then undergoes post-processing and quality assurance checks. The output of this process is predominantly a solid object, which may vary in rigidity from stiff to pliable, depending on the material used and the specific AM process applied.

Thus, any AM process can be conceptualized as a series of transformations, converting data and raw materials into solid objects through a standardized workflow. The introduction of new materials often necessitates engineering adjustments (e.g., atomization for metal powders, filament production for Fused Deposition Modeling) to ensure compatibility with the fabrication process. To broaden the range of materials that can be utilized, modifications to the manufacturing process itself may be required. In the domain of multimaterial fabrication, this could entail the integration of multiple shaping processes within a single machine, enabling the production of parts with novel characteristics and functionalities, such as layer-wise CNC surface finishing or powder deposition techniques.

The quintessential challenge of AM can thus be distilled into the quest for a function that maps a set of materials, irrespective of their phase (granular, solid, liquid), onto a predefined solid part. This encapsulates the essence of AM, highlighting the intricacies of material compatibility, process adaptability, and the multifaceted transformations that underpin the realization of complex parts through digital fabrication techniques.

In simpler terms, this thesis focuses on applying the principles of inverse design to the process design of additive manufacturing (AM). Essentially, inverse design involves starting with a desired outcome or set of performance goals and working backwards to determine the optimal process parameters and material selections needed to achieve that outcome. In the context of AM, this means using machine learning and real-time simulation to reverse-engineer the manufacturing process. By defining the end characteristics of the part we wish to create, including its physical properties and functional requirements, we can then identify the most suitable materials and AM processes to realize the specified design. This reverse approach facilitates a more targeted and efficient path to achieving desired part specifications, optimizing the AM process for enhanced performance, material use, and compatibility.

Linking with modern robotics

[@totof]

The crux of the challenge in additive manufacturing (AM) lies in managing the dynamic behavior of materials during the fabrication process. The interactions between materials can significantly impact the final product. For instance, depositing melted metal onto a solid polylactic acid (PLA) part may result in an unsatisfactory finish. In addressing such complex operations, an empirical trial-and-error approach could prove invaluable, benefiting not only human operators but also enhancing the learning algorithms within machine learning frameworks. The development of a fast, unified simulation for the manufacturing process could offer substantial insights for handling these challenges effectively.

Turning to the field of material engineering, it is evident that data-driven methods are gaining traction and becoming a burgeoning area of research. However, the integration of these methods within the process workflow often receives attention only in later stages. The task of creating functional prototypes using experimental materials poses its own set of challenges, which could also stand to gain from the implementation of an empirical framework. The fundamental proposition here is to establish a framework that enables the virtual prototyping of manufacturing processes, thereby demonstrating the feasibility of producing multimaterial products.

The necessity for real-time capabilities in this framework underscores the importance of rapid iteration through trial and error. Such immediacy would allow for swift adjustments and optimizations, facilitating a more efficient and effective exploration of material and process compatibilities in the quest to realize complex multimaterial designs.


### Principles of Inverse Design in Additive Manufacturing

- Conceptual Framework: Fundamental concepts and methodologies of inverse design specific to AM.
- Design for MMAM: Addressing inverse design considerations in multimaterial fabrication.

### Guidelines and Future Directions for AM

- _Keywords:_ AM guidelines, Future of AM, Process optimization
- _Discussion Points:_ Laying out guidelines for the future of additive manufacturing, with a focus on process optimization and improvement.

## Real-time simulation
### Constitutive equations

Modeling the multimaterial additive manufacturing process presents a formidable challenge, as outlined in the previous chapter. A unified simulation framework for this domain necessitates a comprehensive consideration of various states of matter, including solids, fluids, and gases, each with distinct mechanical, thermal, and electrical properties.

In this chapter, we will delineate the known physical models integral to an ideal, versatile manufacturing process. This exploration will encompass the essential constitutive equations that govern the behavior of different materials under various manufacturing conditions. By systematically unraveling these equations, we aim to provide a robust theoretical foundation for simulating multimaterial additive manufacturing processes

Before delving into the specifics of mechanical constitutive models, it is crucial to establish a fundamental understanding of the nature of materials and the principles of continuum theory. This groundwork is essential for appreciating the subsequent discussion on modeling the behavior of materials in additive manufacturing processes

Continuum theory forms the backbone of material modeling in the field of engineering and physics. It operates on the premise that materials can be mathematically represented as a macroscopic continuous, even though they are made up of discrete particles like atoms and molecules. This theory allows us to describe material properties and behavior in a continuous manner, simplifying the complex interactions at a microscopic level to a manageable macroscopic level.

In academic discourse, a clear distinction is often drawn between solids and fluids based on their response to external forces. Fluids, as opposed to solids, are characterized by their ability to undergo continuous deformation when subjected to a constant solicitation. Therefore, we usually solve for velocity instead of deformation. This fundamental difference is pivotal in understanding their respective behaviors under various manufacturing conditions. However, in the context of our unified framework for multimaterial additive manufacturing, the interaction between these two states takes on an even more critical role, particularly when considering phase changes.

Solids: Characterized by their ability to resist deformations under applied forces. Solids maintain a definite shape and volume, and their particles are tightly packed in a fixed arrangement. In the context of additive manufacturing, the behavior of solids is often described in terms of elasticity, plasticity, and fracture mechanics.

Fluids: This state includes liquids and gases. Fluids flow and conform to the shape of their containers under the influence of external forces. They are characterized by properties such as viscosity, turbulence, and compressibility. In additive manufacturing, understanding fluid dynamics is crucial, especially when dealing with materials in molten states or in binder jetting techniques.

In additive manufacturing, we frequently encounter scenarios where materials transition between solid and fluid states. For instance, in processes like fused deposition modeling or selective laser sintering, materials are heated to a molten state (fluid) and then solidified to form the final product. This phase change is not just a simple state transition but a complex interplay of thermal, mechanical, and sometimes even chemical processes.

Phase Changes: Phase changes are transitions between solid, liquid, and gas states. In additive manufacturing, phase changes are critical, particularly in processes like sintering, melting, and solidification. Understanding the thermodynamics and kinetics of phase changes is key to predicting material behavior during the manufacturing process.

Aplying continuum theory to additive manufacturing involves utilizing these fundamental concepts to simulate the behavior of materials under various conditions. It allows for the prediction and analysis of how materials in different states interact during the manufacturing process, whether it be in the melting and solidification in metal printing or the curing in resin-based technologies.

#### Molecular dynamics

Molecular dynamics (MD) stands as a powerful computational technique that simulates the physical movements of atoms and molecules, offering a microscopic lens into material behavior. This method is particularly significant in the context of multimaterial additive manufacturing, where understanding the interplay between different materials at the atomic level is crucial. By employing Newtonian mechanics, MD enables the detailed prediction of the dynamic evolution of a material system over time, providing insights into properties like thermal conductivity, mechanical strength, and diffusion characteristics. Its ability to model interactions in a diverse range of conditions – from high temperatures to varying pressures – makes it an invaluable tool for exploring and optimizing the properties of new material combinations. As such, molecular dynamics serves not only as a bridge between theoretical physics and practical engineering but also as a key enabler for innovation in additive manufacturing technologies, driving forward the capabilities of this transformative field.

Simple analytical models but complex behavior emmerging.
Computationnaly intensive

This is interesting to predict material behaviors. However, it is not suitable for a manufacturing process simulation. Even if it is mandatory to understand the underlying of material behavior, we won't dive into the governing equation of molecular dynamics here. Howerver as we will learn later on in part 3.2, meshfree simulation are somehow related to molecular dynamics

#### Continuum mechanics

In the continuum mechanics realm, materials

Continuum mechanics, a fundamental branch of mechanics, plays a pivotal role in understanding and modeling the physical behavior of materials under various conditions. This field elegantly bridges the microscopic and macroscopic worlds, treating materials as continuous, even though they are, in reality, composed of discrete particles. By adopting this continuum hypothesis, it simplifies the complex interactions of particles into a more manageable form, allowing for the analysis of stress, strain, and deformation in materials. This theoretical framework is particularly indispensable in the realm of multimaterial additive manufacturing, where it aids in predicting and optimizing the mechanical properties and behavior of printed objects. Through a blend of mathematical rigor and physical insights, continuum mechanics provides the tools to delve into the intricate dance of forces and movements, serving as a cornerstone for advanced material design and engineering.


Very efficient, FEM... Solving require matrix inversion.
However, it needs a reference configuration X. And maps the reference configuration to the deformed configuration using a deformation function that is invertible.

With Additive manufacturing, dynamic remeshing is required. An eulerian persective could be challenging.


- Heat transfer
	- Conduction
	- Thermal contact
	- Convection approximation
- Elasticity 
- Thermoplasticity
- Vicsoelasticity


1. **Heat Transfer**:

Heat Transfer: A critical aspect in additive manufacturing, heat transfer is governed by conduction, thermal contact, and convection approximation equations. Conduction equations describe the transfer of heat within a material due to temperature gradients, pivotal in predicting and managing thermal stresses and deformations. Thermal contact equations, on the other hand, are crucial for understanding heat transfer between different materials, especially at interfaces where materials with different thermal properties meet. The convection approximation is integral in modeling the heat loss to the environment, a significant factor in cooling rates and solidification patterns.

- **Conduction**: The fundamental equation for conduction is Fourier's Law, given by \( q = -k \nabla T \), where \( q \) is the heat flux, \( k \) is the thermal conductivity of the material, and \( \nabla T \) is the temperature gradient. In additive manufacturing, this equation is crucial for predicting how heat will flow within a material, influencing layer bonding and thermal stresses.

- **Thermal Contact**: At the interface of two different materials, the heat flux is given by \( q = \frac{T_1 - T_2}{R_{\text{thermal}}} \), where \( T_1 \) and \( T_2 \) are the temperatures of the two materials, and \( R_{\text{thermal}} \) is the thermal contact resistance. This equation is vital for understanding how effectively heat is transferred between different materials in a printed object.

- **Convection Approximation**: The heat transfer due to convection can be expressed as \( q = h (T_{\text{surface}} - T_{\text{fluid}}) \), where \( h \) is the convective heat transfer coefficient, \( T_{\text{surface}} \) is the temperature of the material surface, and \( T_{\text{fluid}} \) is the temperature of the surrounding fluid (air, in most cases). This equation helps in modeling the cooling of the material after deposition.

2. **Elasticity**: 

Elasticity: Elasticity equations are fundamental in describing how materials deform under stress and return to their original shape upon the removal of load. This is particularly important in additive manufacturing, where residual stresses can develop during the printing process, impacting the dimensional accuracy and mechanical properties of the final product.

   - The basic equation of elasticity is Hooke's Law, represented as \( \sigma = E \varepsilon \), where \( \sigma \) is the stress, \( E \) is the Young's modulus (a measure of material stiffness), and \( \varepsilon \) is the strain. In additive manufacturing, this law helps predict how much a material will deform under a given load and is essential for ensuring that printed structures can withstand the intended mechanical stresses.

3. **Thermoplasticity**:

Thermoplasticity: In additive manufacturing, thermoplasticity equations capture the behavior of materials that undergo plastic deformation when subjected to high temperatures and stresses. These equations are crucial for materials that soften upon heating and harden upon cooling, dictating the conditions under which materials can be successfully layered and bonded during the printing process.

   - Thermoplastic behavior can be described by a constitutive model like the Prandtl-Reuss equations, which integrate the concepts of plasticity and temperature dependence. These models typically involve complex relationships between stress, strain, and temperature, and are used to predict how materials will deform permanently under high-temperature conditions, which is a common scenario in additive manufacturing processes like fused deposition modeling (FDM).

4. **Viscoelasticity**:

Viscoelasticity: Viscoelasticity equations describe materials that exhibit both viscous and elastic characteristics when undergoing deformation, a common scenario in polymers used in additive manufacturing. These equations help in understanding how materials respond to long-term loading and unloading, which is vital for predicting creep, relaxation, and recovery behaviors in printed objects.


   - A common viscoelastic model is the Maxwell model, represented by the differential equation \( \sigma + \lambda \dot{\sigma} = E (\varepsilon + \lambda \dot{\varepsilon}) \), where \( \lambda \) is a relaxation time parameter. This model captures the time-dependent behavior of materials under load, which is crucial for understanding how printed polymers will behave under long-term stress or during the cooling phase post-printing.

In multimaterial additive manufacturing, these equations are integrated into computational models to simulate and predict the behavior of printed objects. This integration allows for the optimization of printing parameters and the development of materials with tailored properties, ensuring the quality and reliability of the final products.


- Overture on mesh discretization and difficulties to have a everything solver.

### Simulation methods in additive manufacturing
- FEM
- FVM

Need for meshfree methods

### Meshfree Methods: Principles and Applications

- Explain particle systems

- DEM Formulation
- SPH Formulation


Initially conceptualized for astrophysical simulations [@price_smoothed_2012;@monaghan_smoothed_2005], SPH has evolved into a versatile tool, adept at handling complex fluid flows and interactions between different materials. The novelty of this method lies in the smooth interpolation and differentiation of quatity fields in an irregular grid of moving particles. 
This mesh-free, Lagrangian method utilizes particles to represent fluid elements, enabling it to capture free-surface flows and multiphase dynamics with high fidelity. Its inherent adaptability to deformable and fragmenting materials makes SPH particularly suitable for simulating the intricate processes in additive manufacturing, such as material deposition and melting. In this thesis, we delve into the nuances of the SPH method, scrutinizing its theoretical foundations, algorithmic implementations, and its specific adaptations for the challenges posed by multimaterial additive manufacturing.

Starting from continuum mechanics formulation, two variable representation are possible. The eulerian 


- Parameters influence : Smoothing radius > bin size
- If particle move outside of the bin during the time step then result strange







### Real-time simulation stability analysis
- Integration scheme
- Energy conservation

Real time -> Implicit solvers or ! Position based solver inconditionnaly stable.

- Position based dynamics

## Implementation
### Design and architecture
- Modularity, performance and readability balance

Algomesh architecture of the modular solver

- focus on Erin -> Modular Digital twin

### GPU mass parallelization
- GPU fundamentals

-	GPU architecture
- 	Software solution
- 	Cross platform + vizualisation coherence = OpenGL easy to adapt (OpenGL ES, WebGL...) and not propriatery
- 	Threads, Workgroup, SM, shader etc...

- Technical constraints and design rules
	- Modularity for model implementation got easy
	- POO
	- AoS vs SoA
	- Performance consideration

### Experimental validation of the models

Pulling test ? Strain stress curve ?

### Performance analysis

Nvidia NSight, Bandswidth, number of particles comparison with a simple model

## Application
### Case study : Warping
Intro to warping, detection, prevention, optimization...

### Problem definition and hypothesis

- Thermal expansion
- Nozzle pulling on the top layer
- consideration in MMAM -> gradient  of thermomechanical properties

### Highlighting the influence of toolpath

Experiment to demonstrate the influence of toolpath

Simulation of the experiment and comparison

### Simulation based optimization

closed loop for the warping.
2D and 3D

## Conclusion and Future Work
## References
## Appendices



Hello everyone. Thank's for being here for my PhD thesis defense.
I hope you'll appreciate my work and find it interesting.

Today we are going to talk about additive manufacturing. More precisely about Multi-material additive manufacturing. I will introduce this concept later on. We will also talk about optimizing the multimaterial process using real-time simulation. I will also introduce what it means and we will deal about the application and implication.
But before we start. I think it's neccessary to introduce the context and my humble understanding of our world. The purpose of an engineer, (and this is the sense of what we are doing here since we are building engineers step by step), is to create, imagine and build machines. Nowadays engineers are more or less the people that solve problem with inginuity and technology. 
It seems that we are facing many challenges at once. From climate changes to geopolitical issues. Casualites are going from important to critical in a matter of months, testing our ability to adapt and be resilient. 
As technology advances, it seems the problems arising 





