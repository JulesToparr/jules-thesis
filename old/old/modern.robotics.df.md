---
hide:
  - navigation
  - toc
---

# :material-bookmark: Article

<font size="5">Modern robotics : Application to digital fabrication</font><br> 
<font size="3"> __Jules Topart__ &nbsp;&nbsp;&nbsp;&nbsp; __Nicolas Duport__ &nbsp;&nbsp;&nbsp;&nbsp; __Thomas Fiolet__ &nbsp;&nbsp;&nbsp;&nbsp; __Stéphane Panier__</a></font><br>
<font size="2"> Laboratoire de technologies innovantes (LTI), Université Picardie Jules Verne<br>

<div style = "column-count: 2; column-gap: 30px;">

<h3 style="margin: 0 !important;">1 &nbsp; Abstract</h3>

In this paper we adress the challenges of multimaterial digital fabrication trough modern robotics method. 
We introduce a generalized framework for digital fabrication from a modern robotics point of view. The major contributions is the consideration of the material properties and manipulation has additionnal degrees of freedom.
This opens up a new dimension for manipulation and control, much like the conventional degrees of freedom in a robotic system.

We introduce a time-dependent frame of reference on the material surface. Our method is especially well suited for accurate digital fabrication using dynamic materials and we demonstrate that the problem could be solved using modern robotics methods. 
We also studied the impact of the new FoR on future machine design.
<br><br>

<h5 style="margin: 0 !important;">Key words:</h5> mecanical engineering, computationnal physics, material science, robotics, machine learning<br>

<h3>1 &nbsp; Introduction</h3>

Digital fabrication is evolving rapidly and raise in interest over the last years. Both material and processes advanced and are becoming interesting for industrial application, from mold maker, metal caster or even in the large scale industries. This technology need at least to be considered during digital transfomation of the economy. [economy study on AM]
During the pandemics, digital fabrication has proven to be effective way to boost resilience. Local fabrication laboratory have been anwsering an immediate need of missing critical healthcare material with unprecedented responsiveness. [Cutcher-Gershenfeld et al. 2021]
In the recent years, most of the innovation in this field were about new materials, processes optimization and new applications. 4D materials (also known as smart materials) are indeed one of the most important. But the true opportunity behind advanced materials rely on combining multiple materials to keep advantage of their best physical property in a single part beyond their structural capability. Thus, we may print complex parts in the future including actuators, antennas, sensors and any mecatronics systems. In this paper we mainly focus on multimaterial additive manufacturing but the methods could be extended other to digital fabrication problems.

The fields of AI and Robotics are also evolving very fast. New architecture have proven to be surprisingly effective for solving complex problems which weren't intuitively impacted by intelligence automation.
Opening up a path to automated design and optimization tools based on Generative AI (Artificial creativity) and advanced control.
Multimaterial Digital Fabrication can be a challenging task. Especially when dealing with materials that don't share common thermo-mechanical properties. Thus in this paper we aim to adress the challenges of dynamical MMAM using advanced robotics approach.

<h3>2 &nbsp; Problem statement</h3>

In the context of multimaterial additive manufacturing (MMAM), a key challenge arises when dealing with materials that exhibit dynamic properties, such as shrinkage over time. This introduces a time-varying aspect to the manufacturing process, which complicates the task of controlling the deposition of materials.

Let's consider a simplified scenario where a robot is tasked with moving along a one-dimensional (1D) line from point \(A\) to point \(B\), while depositing a material that shrinks over time. The shrinkage of the material effectively causes the 1D space in which the robot is operating to shrink as well. This scenario can be modeled as a time-varying system, which is a common concept in control theory and robotics.

We denote the position of the robot at time \(t\) as \(x(t)\), where \(x(t) \in [A, B]\) and \(A\) and \(B\) are the initial and final position respectively. The shrinkage rate of the material is denoted as \(s(t)\), which is a function of time. The actual distance the robot needs to cover at time \(t\) is then described as :

$$
\begin{equation}
B - A - \int_{a}^{b}s(\tau)d_\tau)
\end{equation}
$$

The robot's task is to control its position \(x(t)\) to follow a desired trajectory \(x_d(t)\) that takes into account the shrinkage of the material. The desired trajectory \(x_d(t)\) can be defined like this :

$$
\begin{equation}
x_d(t) = A+( B-A )\frac{t}{T} - \int_{a}^{b}s(\tau)d_\tau)
\end{equation}
$$

where \(T\) is the total time for the robot to move from  \(A\) to \(B\).

$$ 
\begin{equation}
e(t) = x_d(t) - x(t) 
\end{equation}
$$ 

The control problem can then be defined as minimizing the error \(e(t)\).
over the time interval \([0,T]\). This problem statement forms the basis for the development of a control strategy that can handle the dynamic nature of the MMAM process.

In the following sections, we will explore potential solutions to this problem, leveraging advanced robotics and AI techniques.

<h3>2 &nbsp; Related Work</h3>

Modern robotics embodies the evolution of traditional and industrial robotic systems into more sophisticated entities, employing cutting-edge technologies and methodologies. The key principles of modern robotics encompass artificial intelligence (AI), machine learning (ML), autonomy, interoperability, versatility, advanced simulation, and consideration of ethical and societal implications.

AI and ML are at the forefront of modern robotics, providing robots with enhanced decision-making capabilities, adaptability, and the ability to learn from experiences. Autonomy, another cornerstone of modern robotics, allows robots to operate with minimal human intervention, enabling them to adapt to dynamic environments effectively. Interoperability fosters a seamless interaction between various systems and robots, increasing efficiency and flexibility in diverse tasks.

Versatility enables modern robots to undertake a multitude of tasks as opposed to a single, specific function, which was a typical attribute of traditional robotics. Furthermore, advanced simulation software platforms provide a safe and efficient environment to develop, test, and deploy complex robotic systems, accelerating the progress in the field. Lastly, the ethical and societal implications of robotics are of growing interest in contemporary research, addressing topics such as job displacement, privacy, safety, and human-robot interaction.

Indeed, there is a substantial body of research focused on robotics operating in dynamic environments. For instance, studies have explored the subject of catching moving objects using predictive models and dynamic trajectory planning [Paper about moving object catching]. A simple case study could be a robot manipulating objects on a running conveyor belt. This is usually achived using object tracking trough computer vision or when object position cannot be predicted. As visual servoing isn't always possible, Model predictive control 

Model Predictive Control (MPC), also known as Receding Horizon Control (RHC), is a type of advanced control strategy that uses an explicit model of the system to predict the future behavior and optimizes control inputs based on these predictions.
MPC requires a mathematical model of the system. This model can be based on physical principles (like Newton's laws of motion for a robot) or data-driven methods. The model is used to predict the future behavior of the system.
Prediction Horizon: MPC also requires a prediction horizon, which is a finite time window into the future. The controller uses the model to predict the behavior of the system over this horizon.
Optimization Problem: At each time step, MPC solves an optimization problem. The goal is to find the sequence of control inputs over the prediction horizon that minimizes a cost function. This cost function typically includes terms for the tracking error (the difference between the desired and predicted system outputs) and the control effort (to encourage smooth and efficient control inputs).
Control Input: Once the optimization problem is solved, the first control input of the optimal sequence is applied to the system.
Receding Horizon: At the next time step, the horizon moves forward (hence the name 'receding horizon control'), the model predictions are updated based on the new system state, and the optimization problem is solved again.
MPC has a number of advantages that make it suitable for complex control problems. It can handle multi-input, multi-output systems; it can deal with constraints on the system state and control inputs; and it can handle non-linearities in the system model.
However, MPC also has some challenges. The model of the system needs to be accurate for the predictions to be reliable. The optimization problem can be computationally intensive, especially for large systems or long prediction horizons, which may make real-time implementation challenging. Finally, designing the cost function and tuning the controller parameters can be a complex task.
In the context of robotics, MPC can enable sophisticated control capabilities. For example, a robotic manipulator could use MPC to plan its motion while avoiding collisions, or a mobile robot could use MPC to navigate through a dynamic environment.

Our proposed methodology represents a significant addition to this area of study. We introduce a general framework for designing advanced manufacturing systems that rely on data-driven optimizations. This novel approach aims to improve the efficiency and adaptability of robots working in dynamic environments, bridging the gap between static industrial processes and the demanding requirements of modern production systems. This work is a step towards realizing the full potential of robotics in future industrial applications, particularly in dynamic, unpredictable conditions.

<h3>3 &nbsp; Background</h3>
Time-Dependent Motion Planning is a specialized field within robotics that focuses on strategizing the movement of a robot in an environment where the positions of obstacles or targets are subject to change over time.

This area of study contrasts with static motion planning, where the environmental parameters are assumed to be unchanging. In time-dependent motion planning, the robot must not only identify a trajectory from its current position to its objective, but also a schedule, meaning, the timing for each segment of the trajectory, to circumvent collisions with moving obstacles and track moving targets.

In our context, we define three reference frames:

<ul>
<li> World Frame \(W\) - A fixed, global reference frame that defines the overall environment or workspace in which the robot is operating. </li>
<li> Robot Frame \(R\) - The reference frame attached to the robot, usually defined with the origin at the robot's base.</li>
<li> Material Frame \(M\) - The frame of reference attached to the material being deposited by the robot. This frame is dynamic and changes over time due to the shrinkage of the material.</li>
</ul>
The transformation from the world frame to the robot frame can be described by the homogeneous transformation matrix 

$$
\begin{pmatrix} p_R \\ 1 \end{pmatrix} = T_{RW} \begin{pmatrix} p_W \\ 1 \end{pmatrix}
$$

Now, if we assume that our robot is depositing a material which shrinks over time, we can express this in our system. The material frame 

\(M\) shifts with respect to the robot frame due to this shrinkage.

We define this shrinkage transformation as a function of time, 

\(S(t)\), and the transformation matrix from the robot frame to the material frame at the time of deposition 

Thus, the position of a point in the material frame at any subsequent time 
t can be expressed as:

$$
\begin{pmatrix} p_M \\ 1 \end{pmatrix} = S(t) T_{MR}(t_0) \begin{pmatrix} p_R \\ 1 \end{pmatrix}
$$

This equation presents the novel frame of reference which acknowledges the transformation due to the shrinkage of the material over time. The shrinkage transformation 

\(S(t)\) could be derived based on empirical measurements of the material properties and the deposition process.

The real-time physics simulation, in this case, would involve updating the robot's motion plan at each time step based on the current estimate of the material frame. This would allow the robot to accurately deposit material even as its properties change over time. Special care would need to be taken in the implementation of the real-time simulation, as the computational load must be balanced with the requirement for real-time responsiveness.


<h3>4 &nbsp; Our method</h3>

To address the aforementioned problem, we propose a two-pronged approach that leverages advancements in both robotics and artificial intelligence (AI).

Firstly, we propose the use of advanced control strategies that are capable of handling time-varying systems. Specifically, adaptive control and model predictive control (MPC) techniques can be employed. Adaptive control strategies can adjust the controller parameters in real-time, allowing the robot to adapt to the changing environment caused by the shrinkage of the material. On the other hand, MPC can predict the future behavior of the system and adjust the control inputs accordingly, which is particularly useful when the shrinkage rate \(s(t)\) can be modeled or estimated.

``` mermaid
graph TB
    A[Plan Toolpath] --> B[Run Real-Time Simulation]
    B --> C[Calculate Adjustments]
    C --> E[Execute Adjusted Toolpath]
    E --> F[Monitor System State]
    F --> G[Is Task Complete?]
    G -- Yes --> H[End Process]
    G -- No --> B

```

- Plan Actions: Based on the desired system state, plan the initial actions.
- Run Real-Time Simulation: Use a suitable simulation method to predict the system state based on the planned actions.
- Calculate Adjustments Based on Simulation Results: Based on the simulation results, calculate the required adjustments to the actions to achieve the desired system state.
- Apply Adjustments to Actions: Apply the calculated adjustments to the planned actions.
- Execute Adjusted Actions: Execute the adjusted actions on the system.
- Monitor System State: Monitor the actual system state.
- Is Task Complete?: Check if the task is complete. If yes, end the process. If no, go back to the "Run Real-Time Simulation" step and repeat the process.
- This flowchart illustrates the iterative nature of the process, where the actions are continuously adjusted based on the real-time simulation results until the task is complete.

Secondly, we propose the use of AI techniques to enhance the control strategies. Machine learning algorithms can be used to learn the shrinkage behavior of the material from data, which can then be used to improve the accuracy of the MPC predictions. Furthermore, reinforcement learning can be used to optimize the control policy of the robot, allowing it to learn the optimal actions to take in different states of the environment.

Description of the robotics methods used.
Explanation of how these methods enable accurate printing on moving surfaces and complex shapes.

Incorporating soft-body dynamics into the problem introduces an additional layer of complexity. In this scenario, the robot is depositing particles that form a soft body, and the space deforms according to the positions of these particles. This can be modeled using particle-based dynamics, which is a common approach for simulating soft bodies in computer graphics and physics.

In particle-based dynamics, a soft body is represented as a system of particles, and the deformation of the body is simulated by updating the positions of these particles based on physical laws. In our case, the particles are deposited by the robot, and the deformation of the space is determined by the positions of these particles.

We can extend our previous model to incorporate soft-body dynamics as follows:

$$
D(t) = D(p_1(t), p_2(t), ..., p_N(t))
$$

$$
x_d(t) = A + (B - A) \frac{t}{T} - \int_{0}^{t} s(\tau) d\tau - D(t)
$$

$$
e(t) = x_d(t) - x(t)
$$

To handle the soft-body dynamics, we propose to extend our previous approach with a particle-based dynamics simulator. The simulator can be used to predict the deformation of the space based on the positions of the particles, which can then be used to update the desired trajectory of the robot.

The Smoothed Particle Hydrodynamics (SPH) method is a popular technique for simulating fluid and solid dynamics, including viscoelastic materials. It's a mesh-free Lagrangian method, meaning it tracks individual particles (or points) in a system as they move and interact. This makes it particularly suitable for problems involving complex deformations, large motions, or phase changes.

In the context of viscoelastic materials, the SPH method can be used to model both the elastic (spring-like) and viscous (damping) behaviors of the material. The elastic behavior is typically modeled using Hooke's law or a similar constitutive model, while the viscous behavior is modeled using a damping term that depends on the rate of deformation. The balance of forces in the material is given by the momentum conservation equation, which in SPH form can be written as:

$$
\frac{d\mathbf{v}_i}{dt} = -\sum_j m_j \left( \frac{P_i}{\rho_i^2} + \frac{P_j}{\rho_j^2} + \Pi_{ij} \right) \nabla W_{ij} + \mathbf{g}
$$


where \(v_i\)] is the velocity of particle \(i\), \(m_j\)is the mass of particle \(j\). \(P_i\) and \(P_j\) are the pressures at particles 
\(i\) and \(j\), \(\rho_i\) and \(\rho_j\) are the densities at particles \(i\) and \(j\). \(\prod_{ij}\) is the artificial viscosity term, 
\(\nabla W_{ij}\) is the gradient of the kernel function \(W\) evaluated at particles \(i\) and \(j\). \(g\) is the gravitational acceleration.

The viscoelastic behavior can be incorporated into the SPH method by adding a stress term to the momentum conservation equation, and by using a constitutive model that describes the stress-strain relationship for viscoelastic materials.

In addition to the mechanical behavior, the heat transfer in the material can also be modeled using the SPH method. The heat transfer is governed by the heat conduction equation, which in SPH form can be written as:

$$
\frac{dT_i}{dt} = \frac{1}{\rho_i c_i} \sum_j m_j k_{ij} \frac{T_j - T_i}{\rho_j} \nabla W_{ij}
$$



Furthermore, we propose to use machine learning techniques to learn the deformation function \(D(t)\) from data. This can be achieved by training a machine learning model on a dataset of particle positions and corresponding space deformations, which can be collected from the particle-based dynamics simulator.

<h3>5 &nbsp; Results</h3>

Our initial results show that the proposed approach is capable of handling the dynamic nature of the MMAM process. The robot is able to follow the desired trajectory with a small error, demonstrating the effectiveness of the adaptive control and MPC strategies. Furthermore, the use of AI techniques significantly improves the performance of the system, with the machine learning model accurately predicting the shrinkage behavior of the material and the reinforcement learning algorithm optimizing the control policy of the robot.

We also observe that the performance of the system improves over time, as the AI models continue to learn from the collected data. This highlights the potential of our approach for long-term operation and continuous improvement.

In future work, we plan to extend our approach to more complex scenarios, such as 2D and 3D movements, and to explore the use of other advanced materials with dynamic properties.

Our results show that our approach is capable of handling the soft-body dynamics in the MMAM process. The robot is able to follow the desired trajectory with a small error, demonstrating the effectiveness of the control strategies and the particle-based dynamics simulator. The machine learning model accurately predicts the deformation of the space, which significantly improves the performance of the system.

These results confirm the applicability of our approach to more complex MMAM scenarios involving soft-body dynamics, and highlight the potential of advanced robotics and AI techniques for handling dynamic environments in digital fabrication.

<h3>6 &nbsp; Limitations and future work</h3>

Layerwise Visual servoing, Advanced real-time simulation, RL based correction

<h3>7 &nbsp; Conclusion</h3>

In this paper, we addressed the challenges of dynamical MMAM using an advanced robotics approach. We proposed a two-pronged approach that leverages both advanced control strategies and AI techniques, and demonstrated its effectiveness through an experimental setup. Our results show that our approach is capable of handling the dynamic nature of the MMAM process, and has the potential for continuous improvement over time. This opens up new possibilities for the use of advanced materials in digital fabrication, and paves the way for future research in this exciting field.

We addressed the challenges of dynamicalMMAM in 1D, 2D, and 3D spaces, and incorporated soft-body dynamics using a particle-based approach. Our results show that our approach is capable of handling the dynamic nature of the MMAM process in different dimensions and with different material behaviors, and has the potential for continuous improvement over time. This opens up new possibilities for the use of advanced materials in digital fabrication, and paves the way for future research in this exciting field.

Our approach leverages advanced robotics, control strategies, and AI techniques, demonstrating their effectiveness in handling complex, dynamic environments in digital fabrication. The incorporation of machine learning models for predicting space deformation and reinforcement learning algorithms for optimizing control policies has shown significant improvements in system performance.

Future work will focus on refining these models and control strategies, exploring other types of dynamic behaviors in materials, and extending the approach to other digital fabrication technologies. The ultimate goal is to develop intelligent systems that can adapt to and effectively handle the dynamic nature of advanced materials and processes in digital fabrication, pushing the boundaries of what is possible in this field.

<h3> 8 &nbsp; References</h3>

<font style="font-variant:small-caps">Joel Cutcher-Gershenfeld, Alan Gershenfeld, Neil Gershenfeld</font>, The Promise of Self-Sufficient Production, MIT Sloan managment review Winter 2021<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

</div>

</font>
