
# A Meshfree Simulation of the Debinding Process in Metal Fused Filament Fabrication with 316L Stainless Steel

![Alt text](./files/debind.pdf){ type=application/pdf style="height:120vh;width:100%" }
