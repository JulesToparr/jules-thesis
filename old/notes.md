# Notes


## Real-time control 

Simulation
Gen image feed into the net (multiple images of cross section ?)
Neural network with convnet gives an offset X, Y, Z, E




## Real-time control 

Simulation
Gen image feed into the net (multiple images of cross section ?)
Neural network with convnet gives an offset X, Y, Z, E



## Path planning using token generation

Feeding island polygon (image ?) into a Gennet that generate operation tokens. -> See :
	Neural Representation of Polygon Layers
	Jos´e L. Silv´an (jlsilvan@txstate.edu)
	Department of Geography, Texas State University-San Marcos, TX 78666
	June 15, 2005

Feeding a few slices n-2 to n+2 as voxel layers. Miss small features ?

To reward ongoing agent, we can use another model that would predict the probability of things going well based on similar data that actually went well 


Reading : 
Prompt
You're a research assistant in the field of Additive manufacturing. Here is the paper idea.
I want to describe a methodology of using AI in additive manufacturing. And  propose a proof o

Source :
https://www.datacamp.com/tutorial/reinforcement-learning-python-introduction?dc_referrer=https%3A%2F%2Fwww.google.com%2F
https://github.com/rlcode/reinforcement-learning
https://arxiv.org/pdf/2305.10455

















## OLD 

Multimaterial additive manufacturing may require multiple deposition technic in a single process.
This require to analyze the compatibility of such process as well as the compatibility between material.

Thus when a new material is discovered, we will need to both simulate it's behavior during fabrication but also experiment with existing manufacturing process.
New fabrication process will probably appear and combining multiple process as once is basically a new fabrication process.

There is lot's of way to create modular machine nowadays by using modular structure or even agile solution such as anthropomorphic robots. The last barrier of multimaterial research and development is basically a software problem. Each time researcher want to test a new process or a new material, they have to find the correct fabrication parameters and also find innovative way to generate toolpath based on the fabrication behavior.

This lead to a new way of seeing 3D printing. 3D printing new material and especially multimaterial 3D printing require both advanced fabrication simulation and modular slicing software to enable fast automated material discovery.

Chemist will basically be able to create new material by combining existent polymer or by creating new molecule and may have to quickly validate their fabricability and usage. An automatic parameters characterization is a good way to simplify their work. Also, a modular 3D printer will enable quicker manufacturing process engineering.

The thesis will mainly focus on theses two aspects :
- Studying the most suitable machine configuration to enable fast manufacturing process engineering
    This package include : 
    - Test and develop several multimaterial 3D printing machine to classify key parameters
    - Classify machine kinematics and identify implications
    - Identify and classify existing monomaterial manufacturing process
    - Identify compatibility between manufacturing process
    - Identify and classify material compatibility
- Modular slicing software development
    - Meta learning
    - Modular programming
    - Custom slicer
    - Node editor




Software modules :

C++ & OpenGL :

- Rendering engine
- 2D Graphics
- 3D Graphics
- Voxel rendering
- Physics simulation
- Slicing algorithm
    - A GPU-based parallel slicer for 3D printing
- Raster algorithm
- Toolpath generation
- Language Interpreter


Python :

- Image processing
- Machine learning
- Tcp - Communication
- Serial - Communication
- Specific communication
- Modularity
- Visual programming
- Node editor
- Process execution


https://www.codeproject.com/Articles/820116/Embedding-Python-program-in-a-C-Cplusplus-code


In addition this kind of solution may earn profits from meta-learning algorithm. Using modular manufacturing process it is possible to generate automatically the associated slicing algorithm automatically using both parameters optimization and graph generation.



## Challenges in multimaterial 3D printing
Multimaterial 3D printing is developing increasingly thanks to new material available as a filament.
Thus new machines are appearing on the market such as the motion-system from E3D or more recently the PrusaXL from Prusa. Those machines are providing tool changing capabilities. Thus, different tools and materials can be used in a single layer. 
This leads us to a new manufacturing technique combining additive manufacturing with substractive methods.
If this new hybrid manufacturing method develop in a way they became both accessible and easy to use, opportunities are countless.

However, I don't think additive manufacturing is developping rapidly only because of the costs. To my mind the softwares also change a lot the market.
By bringing automatic toolpath generation to everyone, sofware such as Cura on Slic3r bring manufacturing to every one, breaking both the costs and the time to market.

To reveal the real power of hybrid multimaterial manufacturing new software solution are needed.
In the top 10 challenges in 3D printing [2] more challenges are proprosed about hybrid digital fabrication.

### Design tools.
First of all, CAX design tools where not designed for additive manufacturing. Even if this is changing, most of them where created with subtrative manufacturing in head. Additive manufacturing is often an additionnal feature. If the fabrication il generalized, the design tools need to be update to detect automatically wich feature will be manufactured best using which methods. 
Also, those software were mostly tought to design monomaterial parts. It is possible to design an assembly but graded parts are difficult to model.

Alternative exists.Voxel design is a promising technology to listen about. It is simple in its nature and countless in capabilities. Therefor, using voxel in a design workflow is not very convenient.
In other word, we need a CAD generalisation to use both monomaterial CAD performance and voxel versatiliy.
Combining those technology is really the key here.

A key element while designing is the ability to simulate the behavior of the designed parts. In the design workflow, many iteration are made repeating cad and simulation to converge to a robust design. In multimaterial manufacturing, simulation is also a great challenge and again, voxels are a pretty good candidate. Finite elements methods should be easily adaptable to voxels.


Combining different manufaturing methods in a single machine is quite tricky and difficult to generalized. Each shaping process require a specific environment and transform the material in different ways. Combining those also affect the machine kinematics.
Still Conventionnal cartesian fdm printer may handle lot's of other manufacturing process such as material jetting. But some of the methods require a material ether such as stl or binder jetting. Therefor, it is possible to design a complex machine using the versatily of a 6 axis robot to use both methods at the same time. Even if this may slow down the manufaturing process it is possible.
An other way of seing this problem is to deposit this ether along the print. We want to demonstratethat depositing powder while incrinsingly rainsing wall container is possible.


Les défis :

Le design
    Test voxelCAD
    Test hybrid design
la simulation
    Simulate Voxel parts
La machine
    Tester plusieurs cinématique
    - Robot 6 axes
    - Cartésian
    - Polar
    - ...
    Tester plusieurs méthodes
    - Material jetting...
    Tester la fabrication hybrid et l'hybridation des procédés.
Les outils
- Réunir les outils au sein d'une machines
Le firmware
- Firmware hybrid ?
    Les limites du GCode
Le pre-processing
    Trancheur modulaire
La simulation de la fabrication
La simulation de la pièce simulé
Le post-processing
    A voir...



_


Les ressources : 
Salle résine ok

Des matériaux
Des composant
Inkjet
Pompe
Seringue
Platre
Liant

Multimaterial additive manufacturing is definetly a large field of research, It may be very unproductive to developp a such framework alone. In the fields of software developpement, a classical approach to community tool engineering is to use the principle of global intelligence.
The mai aim of the thesis isn't to developp the perfect solution fitting with every application in multimaterial fabrication, but instead it is to developp a framework where every reasearcher can build their own usage according their needs.
Algomesh is born to satified this need. It is a collaborative project where everyone can purpose features, or even implements them, like a lot of commercial 3D slicer, the opensource community has widely participate in the initial developpement and are responsible of lot of marketing decision.

To acheive collaboration between distant peer, a lot has to be done. First of all, the code sould be modular. Which mean thinking beyond the software architecture to create a fully dynamic code environment. 
Interpreted language like Python or NodeJS seems appealing for this purpose, but advance manufacturing will probably involve advanced simulation. Thus, building those simulation to be fast and accurate may require low-level software core.

In other word we need to create an hybrid architecture between python and C++. As a modular architecture may be unrevelant to the new users, a visual programming approach may allow new researcher to join the project. 

From the user point of view, digital fabrication is juste a matter of file conversion. We pass a 3D mesh trough a bunch of software until we get the code instruction for a robot to make our part.
We can think about any slicer as a visual dataflow structure. Each file is converted in another format repeatedly until the final fabrication.

## Program structure
Let's imagine the feature required to acheive complex fabrication. 

### Import
First of all we need a 3D mesh. This mesh may be presented in various format.
Vertex based mesh for instance is a triangle approximation of the external shell of a model. Instead voxel based model are more or less a stack of bitmap aproximating the final part using 3D Pixel nammed voxel.

Those two mesh format mean two different renderer by the way.

Mesh vertex based file format :
- STL
- OBJ
- AMF
- 3MF
- 
Those file are well know is the video game industry and are alsowidely used by 3D printing enthousiast

Voxels based file format : 
DICOM
Those file format are widely use in healthcare sector since it is the standard for 3D medical imaging. Those file are them voxelised to match software requirements.

This step include several function to read data from a model file and parse them into something tangible for human eyes and for incoming transformation and manupulation

I'll not discuss on what file format suits the best to advanced multimaterial fabrication but having more than one class of data structure implied a basic level of modularity

### Transform
The second step when you already got the mesh, is to transform it. 
By transformation I include any modification possible on a model. Scaling, moving in the build area, bending, adjust material distribution, color and so on. 
Those transformation are really limited by the mesh format and may also be specific. The modificition that you can apply on a voxel model are really different from the one you'll appy to a mesh, in the way they'll transform the data for instance.

Typical transformation include :
 - Scaling
 - Translation
 - Rotation
 - Material
 - Density
 - Boolean operation
 - Simplyfication
 - Repair
 - Deformation

### Fabrication configuration

In this part goes every settings a slicer should have to create custom parts. The model stay the same but toolpath are generated differently. Typical function in this part include :
- Layer Height
- Infill density
- Perimeter count
- Speed
- Strategy
- ...

### Slicing and toolpath generation

The best way to generate path and trajectories for additive manufacturing machines is to slice the 3D model into several layer. The, the machine will create the 3D part layer by layer.
A classical challenge in additive manufacturing is to acheive non plannar slicing. Which mean that slice generation need to be modular.
Creating custom slice imply to create custom surface intersecting with the solid which may seems difficult to implement..





## Contexte

![!](https://www.sansmachining.com/wp-content/uploads/2021/08/history-of-cnc-machining.jpg){align=right}

La fabrication numérique (FN) est au coeur des projets de modernisation des systèmes de production, depuis les années 60s, les machine de FN, en majorité les centres d'usinage, permettent d'automatiser la production de pièces monomatériaux le plus souvent en métal. Ce procédé de fabrication est efficace mais ne permet de fabriquer que des produits passifs, c'est à dire des pièces mécaniques inertes pouvant être traitées ou assemblées à posteriori. 

Depuis les années 90s un autre méthode de fabrication à vu le jour, la fabrication additive (FA). Ce processus de fabrication s'est notamment développé grâce aux avancées technologiques dans le domaine des polymères et de la robotique.
Il s'agit ici d'un processus de mise en forme par empilement de couches successive. A l'origine, le procédé d'impression 3D, tirait profit des photopolymères et du procédé SLA (vat photopolymerisation). 

L'impression 3D s'est principalement développée durant les 10 dernières années. Le procédé par dépôt de matière en fusion FDM ou FFF (Fused Filament Fabrication) s'est diffusé largement sur le marché public grace à une forte communauté de "Maker" ce qui à provoqué une baisse importante des coûts des machines de FA.

Toutefois il existe une large gamme d'autre procédès moins populaire permettant de fabriquer des pièces plastique mais aussi métalique ou céramique.
D'une manière général, le marché de la FA se concentre sur la fabrication de pièce monomatériaux. Bien que plusieurs constructeurs de machine proposent des solutions dîtes multimatériaux, il s'agit souvent de matériaux d'une même classification et utilisant le même procédés de mise en forme. On parlera plutôt d'impression multicolore ou multifilament. Or un des vecteurs de progression de la méthode réside dans l'utilisation de matériaux dit intelligents et de l'intéraction entre différent matériaux n'ayant par neccessairement un procéssus de mise en forme commun.

Dans un contexte où le numérique transforme à la fois les méthodes de production et les attentes de l'utilisateurs, la demande des produits manufacturé se dirige plutôt vers des systèmes composites complexes intégrant de plus en plus d'intelligence (Logique, électronique, actionneurs ...).

Même si cela complexifie grandement les études de fiabilité des systèmes ainsi produit, les processus de fabrications sont tous grandement maitrisé, mais requiert l'assemblage de plusieurs composants indépendants parfois produient sur des sites éloignés les uns des autres. Dans le cadre des programmes usines du futur, trois axes de développement de dégagent en faveur de la FA : 

- La performance
- L'agilité
- L'eco-responsabilité

![](https://i.pinimg.com/originals/ba/ae/f5/baaef5c72222348dfb163ddb7bb480ec.jpg){align=right style="height:240px"}

D'abord, contrairement à la fabrication soustractive (FS), la FA permet d'utiliser presque exclusivement (exp : Support, Adhesion ..) de la matière utile, c'est à dire faisant partie du volume final du produit. Ensuite, elle permet de simplifier grandement la chaîne d'approvisionnement. Plutôt que d'acheminer des composants d'un site de production à l'autre, il est possible d'acheminer les fichiers de fabrication et de produire sur place.
Car oui l'intêret de la FA c'est de pouvoir produire n'importe quel pièces dans une large gamme de matériaux. Ce qui est plus difficile en FS du fait des restriction de forme induite par les déplacements limité de la broche d'usinage.

Depuis un peu plus de 30 ans, l'impression 3D à évolué de manière fulgurante, notamment grâce à la baisse des coûts des machines et à l'expiration de plusieurs brevet. On peut identifier 5 axes principaux dans cette évolutions :

- Materiaux
- Qualité impression
- Vitesse d'impression
- Accessibilité
- Coût

Le marché global de la FA est estimé à 17.4 Miliards de dollar en 2022. En europe, on estime qu'il atteindra 6.8 Miliard d'euro en 2029. C'est pourquoi de plus en plus d'entreprise et nottamment les grands compte investissent dans la R&D autour de la FA.

En France, la FA est un des 8 sujet clés du programme Usine du futur. Ce programme ayant pour but de faciliter la modernisation du tissu industriel Français par du constat que le pays acuse un retard technologique important dans le secteur industriel. Plus locallement, dans les Haut de France, le programme REV3 supporte des projet en lien avec l'usine du futur.

On commence même à voir apparaitre des machines grand public intégrant des solutions d'impression multimatériaux. Ces machines grands public, essentiellement basé sur procédé FDM, tire profit d'un changement de filament voire d'un changeur d'outils. 
D'autre machines industrielle utilise d'autre méthode de fabrication comme les solutions Inkjet par exemple.

//TODO : Neotech & NScrypt








![](https://t3.ftcdn.net/jpg/00/93/64/02/360_F_93640214_thJJsHbHkPRn4UUd6ajmqbMoJqt101xk.jpg){.img-medium}

# Multi-Material Additive Manufacturing : Optimization through real-Time meshfree simulation

---

## Abstract

The world is currently facing many challenges, including global pandemics, rapid environmental change, social challenges and geopolitical issues. Moreover, it is important to note that these challenges are occurring at an alarming and accelerating pace.
The urgency of modern problems has gone from critical to urgent in the space of a few months, testing our capacity to adapt and our resilience.
Thus, the demand for new manufacturing processes and new materials is increasing accordingly.
The development of computational physics and machine learning offers the possibility of revealing opportunities for innovation among a vast amount of synthetic data. 
This thesis paves the way for a new methodology for the design and programming of additive manufacturing machines using rapid simulation of material shaping processes. I demonstrate an open simulation framework benefiting from GPU mass parallelization.

__Thesis Supervisor__ : [Prof. Stéphane PANIER](https://fr.linkedin.com/in/st%C3%A9phane-panier-27009919)

---

## Acknowledgments

- Supervisor
- Unilasalle
- LTI
- Phd Students
- Externs
- Family

---

## Table of content

1. Abstract
2. Acknowledgments
3. Table of content
4. Introduction
	- 4.1 Global Challenges and the Need for Agile Manufacturing
	- 4.2 The Rise of Additive Manufacturing During and Post-COVID-19
	- 4.3 Multimaterial Additive Manufacturing (MMAM)
	- 4.4 Inverse Design in MMAM
	- 4.5 Simulation and Optimization in MMAM
	- 4.6 Contributions
5. Multimaterial additive manufacturing
	- 5.1 Background
	- 5.2 Recent advances
	- 5.3 Challenges and opportunities
	- 5.4 Theoretical Background in Multimaterial Additive Manufacturing 
6. Real-time simulation
	- 6.1 Constitutive equations
	- 6.2 Simulation methods in additive manufacturing
	- 6.3 Meshfree Methods: Principles and Applications
	- 6.4 Real-time consideration
7. Implementation
	- 7.1 Design and architecture
	- 7.2 GPU mass parallelization
	- 7.3 Performance analysis
8. Application
	- 8.1 Case study : Warping
	- 8.2 Problem definition and hypothesis
	- 8.3 Highlighting the influence of toolpath
	- 8.4 Simulation based optimization
9. Conclusion and Future Work
10. References
11. Appendices

---

## Introduction

### Global Challenges and the Need for Agile Manufacturing
- _Keywords:_ Global challenges, Agility, Resource optimization
- _Discussion Points:_ The necessity to adapt manufacturing processes in response to global challenges and the importance of developing new materials and products.


Th world is facing many hard problems. From climate crisis to global health issues, those challenges are directly reshaping the industrial landscape. Furthermore, great challenges seems to be going from urgent to absolute emergency in a matter of months, testing our capacity to adapt and our resilience. The industry is constrained both from the global challenges and changing comsuption habits, enlighting the imperative for agile manufacturing. Agility in this context is not an ideal objective but rather a vital necessity. 


In an era marked by an array of global challenges, ranging from environmental crises to rapid technological advancements, the imperative for agile manufacturing has never been more pronounced. These challenges, encompassing both natural and human-made phenomena, necessitate a radical rethinking of traditional manufacturing processes. Agility in this context is not merely a desirable attribute but a vital necessity. It underscores the importance of swift adaptation in manufacturing techniques to respond to the ever-evolving demands and constraints of our world. This dynamism is particularly relevant in the development of new materials and innovative products, which are critical in addressing these global issues. The ability to efficiently optimize resources, minimize waste, and rapidly pivot in response to changing circumstances is not just beneficial but essential for sustainability and resilience in our interconnected global landscape. This chapter delves into the crux of these challenges, underscoring the role of multimaterial additive manufacturing as a cornerstone in the pursuit of agile, responsive, and sustainable manufacturing solutions.


### The Rise of Additive Manufacturing During and Post-COVID-19
- _Keywords:_ Additive manufacturing, COVID-19 resilience, Industrial R&D, Patent surge
- _Discussion Points:_ The development and growth of additive manufacturing as a resilient manufacturing process during the COVID-19 pandemic, and the subsequent industrial investment and innovation in this field.
### Multimaterial Additive Manufacturing (MMAM)
### Inverse Design in MMAM
### Simulation and Optimization in MMAM
### Contributions

In the post-COVID era, the landscape of additive manufacturing (AM) has evolved significantly, marked by several key trends and advancements. 
[@cutchergershenfel] [@Choong2020TheGR]
The pandemic has also highlighted the role of 3D printing in supporting improved healthcare responses and as a cornerstone for a more environmentally friendly future. The flexibility of 3D printing, along with its ability to produce parts on demand, has the potential to reduce waste and inventory, making it a key player in the move towards greener manufacturing processes. This shift is likely to result in shorter and more fragmented supply chains, with an increased emphasis on decentralized manufacturing and local microgrids of 3D-printing factories. 

One of the major shifts has been the record installations of AM systems, driven by the validation of applications in sectors with critical regulatory criteria, such as healthcare and aerospace. This growth in AM is further supported by an increased focus on scalability, higher throughput 3D printing systems, and enhanced reliability and repeatability. 

!!! info "Synthese : "
	There are global challenge that we must face. Thus we must adapt and be more agile while optimizing ressources. We must developp new materials and product and thus new manufacturing methods. Additive manufacturing is known to be a versatile manufacturing process and it developp quite well during covid19 pandemics as a resilience vector. After the pandemics, industrials invest widely in R&D developped during lockdown and many patents apperas in that field. 

	The products we must manufature include sensors and actuators now and not only static structural parts. Thus additive manufacturing as to go multimaterial. This brings a lot of challenges including material interaction, combining several processes and designing the products.

	Inverse design is a method where you start from the desired product performance and characteristics and you use physical, mathematical or computational model to find an optimal solution. It is indeed a key opportunity to design MM products and MMAM process. 
	As each processes require a specific machine or tool and the machines requiring specific slicers and control. MMAM is indeed challenging and could be leveraged using inverse design.

	Inverse design could be realized manually or automaticaly, recently, generative AI have demonstrated unprecedent performance in this area. Thus machine learning is also an opportunity. 
	Rather the inverse design approach is automated or not, it rely on experiment of models. As experiment can be laborious and expensive, a fast simulation method would be benefical for both. This is where my PhD arrive. My contribution to this is :
	- Guidelines for future of AM
	- Fast simulation of AM processes and GPU computing framework
	- Automated optimization of AM process to reduce a demonstrating defect : warping.

!!! info "Structure : "
	**Global Introduction**
	1. **Global Challenges and the Need for Agile Manufacturing**
	  

	**Multimaterial Additive Manufacturing (MMAM)**
	3. **Challenges and Opportunities in Multimaterial AM**
	  - _Keywords:_ Multimaterial, Sensor integration, Actuator integration, Material interaction
	  - _Discussion Points:_ The shift towards multimaterial additive manufacturing to include sensors and actuators, the challenges involved such as material interaction, and the need for combining multiple processes in design.

	**Inverse Design in MMAM**
	4. **The Concept and Importance of Inverse Design**
	  - _Keywords:_ Inverse design, Product performance, Computational models, Optimal solutions
	  - _Discussion Points:_ Introduction to inverse design, starting from desired product performance to achieve optimal manufacturing solutions, its significance in MMAM.

	5. **Challenges in Implementing Inverse Design for MMAM**
	  - _Keywords:_ Machine-specific requirements, Slicer specificity, Control complexity
	  - _Discussion Points:_ The complexity of implementing inverse design in MMAM due to the need for process-specific machines and control systems.

	**Simulation and Optimization in MMAM**
	6. **Leveraging Machine Learning and Fast Simulation**
	  - _Keywords:_ Generative AI, Machine learning, Fast simulation, Experimentation efficiency
	  - _Discussion Points:_ The role of machine learning and AI in automating inverse design, the need for fast simulation methods to support both manual and automated design processes.

	**Contributions of the PhD**
	7. **Addressing Warping in Additive Manufacturing**
	  - _Keywords:_ Warping defect, Automated optimization, GPU computing framework
	  - _Discussion Points:_ The focus of the PhD on developing a fast simulation method and GPU computing framework to address warping, a common defect in AM processes.


## Multimaterial additive manufacturing
### Background
- Recent advances

Modeling : 
	- for quality
	- for resulting mechanical property
	- predict resisual stress

Modeling is a great way to both understand a physical behavior and make predictions. It is indeed a way to demonstrate the behavior of the studyed system. 

Modeling in AM is important to reduce defect, predict residual stress or part emchanical properties.
Many publications focus on developping materials models or simulation while validating them using experimentals results. Those results are crucial to adress technological challenges wisely.

Innovation : 
	- Big area
	- Robot assisted
	- Support removal
	- coating and conformal 3D printing
	- Embed device
	- MMAM
	- Hybrid AM

Many innovation in additive manufacturing went popular in the last few years. Ranging from house 3D printing to 3D printed wearable embed device, new use case and brillant idea are poping worldwide. 
Among those innovation many involve custom machines or process. 
Even if the underlying langage is similar, each new manufacturing process may need a custom software to generate toolpath and trajectory.
Specialized and Hybrid MMAM machine rely on modified CAM software to minimize the developpement time usually spent on the process itself.
Has all processes are similarly controlled and as they all rely on 3D models. It would be very efficient to use a modular slicer, and write custom instructions for adaptation.





Generative design : 
	- Design support for residual stress
	- Reduce volume
	- Solving engineering problems

Generative design is also a rapidly developping field. Software as NTop of the dassault system suit offer very powerwul tools to generate 3D shapeds and models given predeterminend rules. Those rules coul be used to maximize material in weak zone or general lattice to minimize volume and weight. It could be used also to match the 3 model to the stress sollicitation.


MAterials characterization :
	- Evaluate the material properties of the printed objects
	- Evaluate the performance 
	- Optimize prining parameters
  
Material are a fundamental part of the AM proces, for most of the known AM process, this involve material special treatment 


Design for AM :
	-	 Design guide
	- one shot Assembly
	- Standards
	- manufacturability, costs, reliability...


- Challenges and opportunities

As AM is a very versatile process, addressing whole challenges at once would be very difficult. However, give the number of publication in material design, new manufacturing process or method and new toolpath strategy. It will be benefical to have a framework used both to educate newbie and test new approach in order to implement them and evalutate.

In robotics, as the field advance rapidly, it is common to setup simulation environments along with the robot program in order to test fundamental constrol model or at least to understand the robots behavior and data related to it.

In robotics a typical workflow would be to setup ROS along with a simulator possibely Gazebo or Ros Sim, this could be used to vizualize constrol models and algorithms to enable trials and error empirical approach but also to generate synthetic data crucial to miniminze developpment time during the experimentals parts.

As Additive manufacturing grow in conplexity, the global machine architecture is more and more looking like a robot. And a modular architecture wouldn"t be surprising in the future. Such architecture started to developp in their very initial state such as Klipper.

Programming langage such as well known Gcode is widely used and defined as the strandart for both precision production and AM. However, AM programm files are usually very large and contain thousands of instructions letting no felxibility for adaptative control. 
function as fillHilbertSurface of extrudePerimeter may replace long list of instructions and give better support for in situ control and adaptation.

For instance, an advanced 3D printer could in the future decide if it is best to print the perimeter or the infill first given the current configuration of the part. Taking into account temperature variation, environment perturbations or even part deformation.

Relativity space is a Californian startup specialized in 3D printing robkect tank. Their approach is conceptually equivalent similar but different in the implementation.
Due to the large nature of the manufactured parts, they used industrials robot that are therefore following a given set of instructions. Their approach incolve simulating the whole manufacturing process and optimizing toolpath to improve the final part. In their process, the tank printed in metled metal depositions is deposited onto a spiralized outer peritmer of a circular shape. This shape will deforme after coolign due to the Thermal expansions of the materials.
This progressive deformation can easily be modeled and compensated. Even if the implementation of a such process is indeed impressive, the underlying technology and known and matestered by one.

Schéma archi OS vs Microprogramm Marlin. 

KLIPPER




Introduce AI agent Gym
Tesla is training AI models into Dojo
Robot Anymal

It wouldn't be reasonable to control the full machine using an AI agent due to the very large number of instruction in a program. Instead we could rely on AI assisted decision making and a high level abstraction of the manufacturing process.

We could for instance consider using operation such as CNC lanufacturing provide. 2D Pocket, surfacing etc... and the AI agent select them and control them.

Thus their is different level of control happening in the manufacturing process. 
The architecture need to be redefined.



High level control, compare the actual printed part with the objective and make decicion about the next move. 
Mid level control, control the operation itselft using real time operation simulation. (paper from closed loop simulation using pbd)
Low level control (sensor, force feedback ...)




### Theoretical Background in Multimaterial Additive Manufacturing 
- Materials Science and Engineering: Explore the properties and interactions of materials used in MMAM.
- Overview of Multimaterial Processes: Detailed examination of various MMAM processes.
- Powder Bed Fusion (PBF): Discuss the use of multiple powders in PBF and the challenges involved.
- Material Jetting: Explore how multiple materials are jetted and cured in material jetting processes.
- Fused Deposition Modeling (FDM) with Multiple Materials: Discuss the integration of different filaments in FDM.
- Directed Energy Deposition (DED): Explore the use of multiple material feeds in DED processes.
- Process Dynamics in MMAM: Dynamics of layer deposition, curing, sintering in MMAM.

### Principles of Inverse Design in Additive Manufacturing
- Conceptual Framework: Fundamental concepts and methodologies of inverse design specific to AM.
- Design for MMAM: Addressing inverse design considerations in multimaterial fabrication.

### Guidelines and Future Directions for AM
- _Keywords:_ AM guidelines, Future of AM, Process optimization
- _Discussion Points:_ Laying out guidelines for the future of additive manufacturing, with a focus on process optimization and improvement.

## Real-time simulation
### Constitutive equations

Modeling the multimaterial additive manufacturing process presents a formidable challenge, as outlined in the previous chapter. A unified simulation framework for this domain necessitates a comprehensive consideration of various states of matter, including solids, fluids, and gases, each with distinct mechanical, thermal, and electrical properties.

In this chapter, we will delineate the known physical models integral to an ideal, versatile manufacturing process. This exploration will encompass the essential constitutive equations that govern the behavior of different materials under various manufacturing conditions. By systematically unraveling these equations, we aim to provide a robust theoretical foundation for simulating multimaterial additive manufacturing processes

Before delving into the specifics of mechanical constitutive models, it is crucial to establish a fundamental understanding of the nature of materials and the principles of continuum theory. This groundwork is essential for appreciating the subsequent discussion on modeling the behavior of materials in additive manufacturing processes

Continuum theory forms the backbone of material modeling in the field of engineering and physics. It operates on the premise that materials can be mathematically represented as continuous, even though they are made up of discrete particles like atoms and molecules. This theory allows us to describe material properties and behavior in a continuous manner, simplifying the complex interactions at a microscopic level to a manageable macroscopic level.

In academic discourse, a clear distinction is often drawn between solids and fluids based on their response to external forces. Fluids, as opposed to solids, are characterized by their ability to undergo continuous deformation when subjected to a constant solicitation. This fundamental difference is pivotal in understanding their respective behaviors under various manufacturing conditions. However, in the context of our unified framework for multimaterial additive manufacturing, the interaction between these two states takes on an even more critical role, particularly when considering phase changes.

Solids: Characterized by their ability to resist deformations under applied forces. Solids maintain a definite shape and volume, and their particles are tightly packed in a fixed arrangement. In the context of additive manufacturing, the behavior of solids is often described in terms of elasticity, plasticity, and fracture mechanics.

Fluids: This state includes liquids and gases. Fluids flow and conform to the shape of their containers under the influence of external forces. They are characterized by properties such as viscosity, turbulence, and compressibility. In additive manufacturing, understanding fluid dynamics is crucial, especially when dealing with materials in molten states or in binder jetting techniques.

In additive manufacturing, we frequently encounter scenarios where materials transition between solid and fluid states. For instance, in processes like fused deposition modeling or selective laser sintering, materials are heated to a molten state (fluid) and then solidified to form the final product. This phase change is not just a simple state transition but a complex interplay of thermal, mechanical, and sometimes even chemical processes.

Phase Changes: Phase changes are transitions between solid, liquid, and gas states. In additive manufacturing, phase changes are critical, particularly in processes like sintering, melting, and solidification. Understanding the thermodynamics and kinetics of phase changes is key to predicting material behavior during the manufacturing process.

Aplying continuum theory to additive manufacturing involves utilizing these fundamental concepts to simulate the behavior of materials under various conditions. It allows for the prediction and analysis of how materials in different states interact during the manufacturing process, whether it be in the melting and solidification in metal printing or the curing in resin-based technologies.



There is already a significant amoun t of work done in real time solid and fluid simulation. However, such work were mainly motivated by the computer graphics industry where visual effect are more important than physical accuracy. Even if, researcher in this field tend to improve their models, they most trade of between accuracy and big timestep to ensure efficient usage of computer ressources for video games or to shorten movies rendering time.

However some very interesting approach emmerged and are important to consider.

Real time simulation of fluids -> PBF by Macklin and Muller
Implicit viscosity solver -> Takashi
Bender et al SPH

Computational fluid dynamics is widely used nowadays in every industry sector. Their is plenty of methods depending of the simiulation scenario.
In solid mechanics however, for engineering application we rely mainly on the Finite element method. Exept for large deformation where other methods are envisaged.
In granular mechanics however, the common method is DEM. 

A unified solver for every aspect of the simulaiton seems not so easy.

To tackle this it could be interesting to start from scratch. During this PhD we started to study the microscopic behaviour and letter wame to the macroscopic model. we also studied model order reduction to improve computation efficycy.

This work was a bit disorganised because none of the method perfectly fits our needs but the overral approach was the following

Learn about molecular dynamics -> Create a basic solver for molecural dynamics
Learn about DEM -> Adapth the solver for it
Learn about continuum mechanics -> Implement SPH to adapth the particle solver for macroscopics effects.


Molecular dynamics consist in siulating explicit particles such as atoms, ions...
The particels interact using simple set of behaviors relying on simple analitycal solutions
The newtons equation of motion are then appield to the systems. This involve simulating between 100 and 1M object simulataneously
The time scale is ranging from 10 ps to 1µs depending on the lmodel.

Includes temperature and pressure e ects
Able to treat comparatively large systems for a relatively
long time
Help interpret experiments, and to provide alternative
interpretations
Test theories of intra- and intermolecular interaction
Obtain detailed molecular level information
Structure and dynamics (compared to QC, MM, MC . . . )
Improve our understanding of nature through
model-building 


Motion of the system through phase space is governed by
Hamiltonian equations of motion


Hamiltonian formalism defined as : Eq
Usually very efficient to simulate small scale systems, more flexible to change variables, 
Hamilton JAcobi theory
conseve volume





- Heat transfer
	- Conduction
	- Thermal contact
	- Convection approximation
- Elasticity 
- Thermoplasticity
- Vicsoelasticity

- Overture on mesh discretization and difficulties to have a everything solver.

### Simulation methods in additive manufacturing
- FEM
- FVM

Need for meshfree methods

### Meshfree Methods: Principles and Applications

- Explain particle systems

- DEM Formulation
- SPH Formulation


Initially conceptualized for astrophysical simulations [@price_smoothed_2012;@monaghan_smoothed_2005], SPH has evolved into a versatile tool, adept at handling complex fluid flows and interactions between different materials. The novelty of this method lies in the smooth interpolation and differentiation of quatity fields in an irregular grid of moving particles. 
This mesh-free, Lagrangian method utilizes particles to represent fluid elements, enabling it to capture free-surface flows and multiphase dynamics with high fidelity. Its inherent adaptability to deformable and fragmenting materials makes SPH particularly suitable for simulating the intricate processes in additive manufacturing, such as material deposition and melting. In this thesis, we delve into the nuances of the SPH method, scrutinizing its theoretical foundations, algorithmic implementations, and its specific adaptations for the challenges posed by multimaterial additive manufacturing.

Starting from continuum mechanics formulation, two variable representation are possible. The eulerian 


- Parameters influence : Smoothing radius > bin size
- If particle move outside of the bin during the time step then result strange







### Real-time simulation stability analysis
- Integration scheme
- Energy conservation

Real time -> Implicit solvers or ! Position based solver inconditionnaly stable.

- Position based dynamics

## Implementation
### Design and architecture
- Modularity, performance and readability balance

Algomesh architecture of the modular solver

- focus on Erin -> Modular Digital twin

### GPU mass parallelization
- GPU fundamentals

-	GPU architecture
- 	Software solution
- 	Cross platform + vizualisation coherence = OpenGL easy to adapt (OpenGL ES, WebGL...) and not propriatery
- 	Threads, Workgroup, SM, shader etc...

- Technical constraints and design rules
	- Modularity for model implementation got easy
	- POO
	- AoS vs SoA
	- Performance consideration

### Experimental validation of the models

Pulling test ? Strain stress curve ?

### Performance analysis

Nvidia NSight, Bandswidth, number of particles comparison with a simple model

## Application
### Case study : Warping
Intro to warping, detection, prevention, optimization...

### Problem definition and hypothesis

- Thermal expansion
- Nozzle pulling on the top layer
- consideration in MMAM -> gradient  of thermomechanical properties

### Highlighting the influence of toolpath

Experiment to demonstrate the influence of toolpath

Simulation of the experiment and comparison

### Simulation based optimization

closed loop for the warping.
2D and 3D

## Conclusion and Future Work
## References
## Appendices


