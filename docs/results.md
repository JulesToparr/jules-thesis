# Results

<iframe width="1024" height="729" src="https://www.youtube.com/embed/1L2GvhnGi-0" title="Fast SLS 3D printing simulation on the GPU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="1103" height="729" src="https://www.youtube.com/embed/D5faQSWvSAc" title="(Near) Real-time - FDM 3D Printing (SPH on GPU)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="1029" height="729" src="https://www.youtube.com/embed/BP9LTYwc8HY" title="(Near) Real time CFD simulation - Archimedes screw water turbine (GPU)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="893" height="324" src="https://www.youtube.com/embed/tbh0WMRqAjs" title="SPH Fluid simulation with wave generation" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="893" height="500" src="https://www.youtube.com/embed/cGslKXFDyVQ" title="Position based fluid with XPSH viscosity (GPU)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="893" height="576" src="https://www.youtube.com/embed/4yzzOgJ-WYU" title="Granular behavior with Heat transfer (GPU)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="819" height="676" src="https://www.youtube.com/embed/APlOThCjS3g" title="Granular material with Heat transfer in rotation drum 2D (GPU)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<br>