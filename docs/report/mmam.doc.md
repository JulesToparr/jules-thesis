---
template: lonely-page.html
---

# GCode Comparative analysis

## Horizontal Parts

The gcodes for horizontal samples look very similar despite having different values for external perimeters extrusion width. It is unlikely that this variation has a measurable influence on the overall adhesion.

<div class="grid" markdown>
```raw hl_lines="2"
; Laia' gcode
; external perimeters extrusion width = 0.42mm
; perimeters extrusion width = 0.46mm
; infill extrusion width = 0.44mm
; solid infill extrusion width = 0.46mm
; top infill extrusion width = 0.44mm
; first layer extrusion width = 0.60mm
```

```raw hl_lines="2"
; Jules' gcode
; external perimeters extrusion width = 0.44mm
; perimeters extrusion width = 0.46mm
; infill extrusion width = 0.44mm
; solid infill extrusion width = 0.46mm
; top infill extrusion width = 0.44mm
; first layer extrusion width = 0.60mm
```

</div>

The toolpaths for both samples are very similar, as shown in the images below. This indicates that despite the minor difference in external perimeters extrusion width, the overall printing pattern and material deposition remain consistent. This consistency suggests that the difference in external perimeter widths does not significantly affect the adhesion and structural integrity of the printed parts.

### A00

<div class="grid" markdown>
![](./images/A00.png)
![](./images/A00-Lay2.png)
</div>

### A90

<div class="grid" markdown>
![](./images/A90.png)
![](./images/A90-Lay2.png)
</div>

---

## Vertical Parts

The vertical samples, however, present a different scenario. Upon inspection, it is evident that something is wrong with the toolpath. Some features are missing, leading to a drastic reduction in interlocking and overall part integrity.

![](./images/A45V.png)

This image of the A45V sample clearly shows the missing features, which are critical for ensuring strong interlayer adhesion and part strength. The absence of these features compromises the mechanical properties of the printed part.

The likely cause of this issue is that SuperSlicer attempted to repair the `.3mf` file upon loading. This repair process may have inadvertently discarded essential features, even though the same settings were applied. This resulted in the incomplete toolpaths and subsequent defects in the printed parts.


## Next steps and recommandations

- Quality control : Implementing a thorough quality control process to inspect toolpaths and printed samples for any discrepancies or missing features would be a great move. We could for instance produce a reference part for each series with only the interlocking layer. So we can check them visually ?

- Settings consistency : I'm looking forward solution for slicing settings sharing. It seems even when loading a settings backup (Which I did when we came to Amiens) is not sufficient to share the same settings due to temporary settings stored into the machines. I'm trying to use git for that purpose in order to track changes in the settings files. Another solution would be to control the machine using my own software and thus avoid such mistakes...

I can print more samples (with the appropriate settings and toolpath...) if needed. Feel free to ask !

## Downloads

Here are the gcodes for comparison

- [SuperSlicer_Horizontal_JT.gcode](./files/SuperSlicer_Horizontal_JT.gcode)
- [SuperSlicer_Horizontal_LF.gcode](./files/SuperSlicer_Horizontal_LF.gcode)
- [SuperSlicer_Vertical_JT.gcode](./files/SuperSlicer_Vertical_JT.gcode)
- [SuperSlicer_Vertical_LF.gcode](./files/SuperSlicer_Vertical_LF.gcode)
- [SuperSlicerAllModelsHorizontalAndVertical_LF.gcode](./files/SuperSlicerAllModelsHorizontalAndVertical_LF.gcode)

