# Chapter 3 : GPU powered real-time physics engine

The Fused Filament Fabrication process (FFF) also known as Fused Deposition Modeling (FDM), is a popular additive manufacturing technology where a thermoplastic filament is heated and extruded through a nozzle, layer by layer, to create a three-dimensional object based on a digital model. 
This process is widely used and well documented for monomaterial application.
Recently a lot of "multimaterial" capable machines reach the market. However they usually use the same polymer in different color or sharing close physical properties (thermal expansion, melting temperature). Multimaterial fabrication in 3D printing is the use of multiple types of materials, often different types of polymers, in a single print. This is typically done either for aesthetic reasons, to achieve different mechanical properties, or to create complex structures that wouldn't be possible with a single material. However more complex compound could be made in FDM and even if common 3D slicers are optimized for PLA and ABS, it could be possible to use other kind of materials and thus enhancing the design possibilites. 
FDM produced parts usually present defect of all kind. All those defect are very well documented and developpers continuously improve the 3D slicers performance.
However their limited by the amount of materials available and by the time needed to test and characterize all possible conpound and conditions.
Each printing job resulting in one experiment to design the most cost-effective part.
Generative design is becomming popular in mechanical engineering. And making mechanical designer work with simulation engineers continuously improve the overral performance.
To enhance both the design performance and to enable future machine learning based approache. I decided to start working on a realtime physic solver capable of simulating basic material physics for the additive manufacturing context.

To reduce the thesis objectives we decided to focus on the Warping effect. Warping is a common issue in FFF 3D printing. It refers to the deformation that happens in a printed part when different sections of the part cool at different rates, leading to internal stress. When the lower layers cool down and contract while the upper layers are still hot and expanded, this creates a kind of tug-of-war. The result is often that the corners and edges of the part lift up from the print bed, distorting the intended shape of the object.

Warping becomes more visible and problematic in multimaterial fabrication because of the different thermal properties of the materials used. Each material has its own specific thermal expansion coefficient, glass transition temperature, and cooling rate. This means that different materials will contract at different rates as they cool, leading to increased internal stress and a greater risk of warping.
For example, if one polymer cools and contracts faster than another, it can exert a pulling force on the slower-cooling material, leading to deformation. Additionally, if one material has a higher glass transition temperature (the temperature at which it changes from a hard, glassy material to a soft, rubbery one) than the other, it may remain expanded for longer, also leading to warping.

There are several ways to mitigate warping in multimaterial 3D prints. These include:

- Ensuring the print bed is level and properly calibrated
- Using a heated print bed to ensure the bottom layers of the print remain warm for longer, reducing the temperature gradient
- Using adhesion helpers like glue, tape, or specialized print surfaces to improve bed adhesion
- Choosing materials with similar thermal properties for multimaterial prints
- Designing the object with warping in mind, such as by avoiding large, flat areas or sharp corners, which are more susceptible to warping
- Implementing proper cooling strategies, like adjusting the cooling fan speed and direction.

Warping is a complex issue in 3D printing and can be especially troublesome in multimaterial prints, but with the right strategies, it can be minimized.