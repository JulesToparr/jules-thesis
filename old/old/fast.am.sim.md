---
hide:
  - navigation
  - toc
---

<font size="5"> Framework for fast simulation of additive manufacturing processes </font><br> 
<font size="3"> __Jules Topart__ &nbsp; __Thomas Fiolet__ &nbsp; __Jérome Fortin__ &nbsp;__Stéphane Panier__</a></font><br>

<!-- https://www.sciencedirect.com/journal/additive-manufacturing/publish/guide-for-authors -->

<font size="3">
Laboratoire de technologies innovantes (LTI), Université Picardie Jules Verne<br>

<div markdown="1" style="column-count: 2; column-gap: 30px;">
<h3 style="margin: 0 !important;">Abstract</h3>

Lorem Ipsum

### 1. Introduction
- **Objective**: Introduce the need for real-time simulation in additive manufacturing, highlighting the objectives and the background of the research.
- **Background**: Discuss the evolution and importance of additive manufacturing, especially multimaterial AM, and the challenges that necessitate real-time simulation solutions.

### 2. Material and Methods
#### 2.1 Study Design
- Describe the overall research design, including the approach taken to simulate additive manufacturing processes in real time.
#### 2.2 Simulation Framework
- Detail the software and hardware used for simulation, including any specific algorithms or computational methods.
#### 2.3 Modifications to Existing Methods
- Explain any alterations made to standard simulation methods to enable real-time performance, with appropriate references and justifications.

### 3. Theory/Calculation
#### 3.1 Theoretical Background
- Extend the discussion on the theoretical foundations of additive manufacturing simulation, focusing on constitutive equations and the rationale for real-time simulation.
#### 3.2 Real-time Simulation Principles
- Detail the principles and calculations behind real-time simulation, differentiating from traditional methods.

### 4. Results
#### 4.1 Simulation Performance
- Present the results of the simulation's performance, including computational speed and accuracy.
#### 4.2 Case Study: Warping
- Report findings from the case study on warping, demonstrating the effectiveness of real-time simulation in predicting and mitigating issues.

### 5. Discussion
- **Significance**: Analyze the significance of the findings, particularly the impact of real-time simulation on additive manufacturing processes.
- **Comparison**: Discuss how the results compare with existing methods, highlighting improvements and potential areas for further research.

### 6. Conclusions
- Summarize the main findings and their implications for the field of additive manufacturing, emphasizing the contributions of real-time simulation.

### Appendices
#### Appendix A: Detailed Simulation Algorithms
- Provide the mathematical formulations and algorithms used in the simulation, with separate numbering (e.g., Eq. (A.1)).
#### Appendix B: Additional Case Studies
- If applicable, detail other case studies or experiments conducted, with corresponding tables and figures (e.g., Table B.1, Fig. B.1).

### References
- Include a comprehensive list of references cited throughout the paper, following the appropriate citation style.

This structure and content guide follows the provided guidelines for subdivision, section numbering, and the presentation of material, ensuring that your research paper is well-organized and adheres to academic standards.
<h3> 8 &nbsp; References</h3>

<font style="font-variant:small-caps">Joel Cutcher-Gershenfeld, Alan Gershenfeld, Neil Gershenfeld</font>, The Promise of Self-Sufficient Production, MIT Sloan managment review Winter 2021<br><br>

<font style="font-variant:small-caps">Mauricio A. Sarabia-Vallejos, Fernando E. Rodríguez-Umanzor, Carmen M. González-Henríquez and Juan Rodríguez-Hernández</font>, Innovation in Additive Manufacturing Using Polymers: A Survey on the Technological and Material Developments, Polymers 2022<br><br>

</div>
</font>
