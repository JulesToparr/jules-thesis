
# II. Advanced Manufacturing Control Strategies

## 2.1 The Need for Advanced Control in MMAM

In the previous chapter, we explored the fundamentals of digital fabrication, highlighting the transformative potential of additive manufacturing (AM) and multimaterial additive manufacturing (MMAM). As we delve deeper, it becomes clear that alongside pivotal hardware developments, software innovations play a important role in overcoming some of the most persistent challenges faced in AM. This chapter will focus on dissecting these software-related challenges, particularly in the realms of toolpath generation, closed-loop control, and decision-making processes.
















One of the primary computational methods to enhance MMAM is the advancement of toolpath generation, especially through non-planar slicing. This technique allows for the creation of parts with complex geometries and improved mechanical properties. We will examine the latest developments in this area, discussing both the challenges and potential solutions that could further enhance this technology.

Another significant aspect of computational improvements in AM is the use of simulations for residual stress reduction and defect prevention. These simulations are integral to refining the manufacturing process, ensuring higher quality and consistency in the final products.

Furthermore, closed-loop control systems represent a revolutionary approach in AM. These systems incorporate real-time monitoring and adaptive feedback mechanisms, dynamically adjusting the manufacturing parameters in response to any deviations observed during the process. Such advancements promise significant improvements in the precision and efficiency of AM operations.

The impact of machine learning, particularly reinforcement learning, parallels these advancements by enhancing the capabilities of robotic systems, which are often integral components of AM setups. Traditional robotic control systems, which rely heavily on closed-loop control mechanisms, are increasingly being supplemented with algorithms that enable robots to learn and adapt in real time. This shift from static to dynamic, learning-based control systems not only enhances the flexibility and autonomy of robots but also their applicability in complex manufacturing environments.

Through a comprehensive review of these topics, this chapter will not only highlight the critical role of software in modern AM systems but also discuss how these innovations contribute to more precise, efficient, and effective manufacturing outcomes. By exploring the synergies between these software solutions and the physical processes of AM, we aim to provide a holistic view of the future directions and possibilities within the field.

















## 2.2 3D slicing and Toolpath generation

### 2.2.1 Introduction to 3D Slicing

Definition and Basics: Explain what 3D slicing is and its role in translating 3D digital models into instructions that an AM printer can execute.
Importance in AM: Discuss the significance of slicing in achieving high-quality prints, focusing on its impact on print success, material usage, and final part properties.


{--

In chapter 2

#### 1.1.3 3D slicing and toolpath generation 

AM machines, just like CNC machines, operate by reading pre-determined instructions encoded in a specific syntax. These instructions dictate each movement or command that the machine executes. The most widely used syntax for controlling various machines, from milling machines to lathes and AM printers, is the G-code.

``` gcode
; Start G-code
G21 ; Set units to millimeters
G90 ; Use absolute positioning
M104 S200 ; Set extruder temperature to 200°C
G28 ; Home all axes

; Print rectangle
G1 F1500 E0 ; Reset extruder
G92 E0 ; Reset extruder position
G1 Z0.3 F1200 ; Move to layer height
G1 X0 Y0 F3000 ; Move to start position
G1 E10 F150 ; Start extrusion
G1 X50 Y0 E20 ; Draw first side
G1 X50 Y50 E30 ; Draw second side
G1 X0 Y50 E40 ; Draw third side
G1 X0 Y0 E50 ; Draw fourth side
G1 E60 F150 ; Extrude a bit more to seal the edge

; End G-code
G1 Z10 F1200 ; Move nozzle up
M104 S0 ; Turn off extruder
M84 ; Disable motors
```

G-code syntax is relatively straightforward and can be written manually. Historically, manufacturing processes were directly encoded by operators who followed detailed blueprints. While manual coding is still feasible for many end products today, software tools have been developed to significantly reduce programming time and increase manufacturing quality and accuracy. These tools allow users to define a series of manufacturing operation within a Computer-Aided Manufacturing (CAM) software. The latter automatically generates the machine's instructions code.

For AM, the process is more complex. Even simple parts can consist of hundreds of thousands of instructions, making manual generation impractical and highly inefficient. To address this, 3D slicers, akin to CAM software, were developed. These slicers are responsible for generating the layers of the 3D model and, crucially, creating the toolpath as machine instructions.

The slicing software works by converting a digital 3D model into a series of thin horizontal layers. The 3D model, generally described using the STL or 3MF format, is represented by a surface triangular mesh depicting its external surfaces. This mesh has to be sliced by computing the intersection of the slicing plane with the mesh facets (triangles). The avoid slicing errors due to erroneous meshes, modern slicers repair the mesh before the slicing procedure by filling gaps and holes.

From this points, the toolpath generation higly depends on the AM process used. For instance, a slicer the DLP Process will generate a series of 2D bitmaps while the FFF process would require a series of precise toolpaths.

After computing the slices, the resulting 2D perimeters are used has layers boundaries for the toolpath generation. The perimeters 

```mermaid
%%{init: { 'logLevel': 'debug', 'theme': 'base', 'gitGraph' : {'mainBranchName' : 'Slicing workflow'}}}%%
gitGraph
  commit id: "3D Model Creation"
  commit id: "Model Import"
  commit id: "Error Checking & Repair"
  commit id: "Support Structure Generation"
  commit id: "Slicing into Layers"
  commit id: "Contour Generation"
  commit id: "Infill Pattern Generation"
  commit id: "Toolpath Planning"
  commit id: "G-code Generation"
  commit id: "3D Printing"
```

??? info
    1. **3D Model Creation**: The 3D object is created using a CAD software.
    2. **Model Import**: The 3D object is imported into the machine specific software (Slicer).
    3. **Model Preparation**: Ensure the 3D model is error-free and optimized for printing.
    4. **Error Checking & Repair**: Ensuring the 3D model is error-free and optimized for printing.
    5. **Support Structure Generation**: Automatically or manually placing support structures where needed.
    6. **Layer Slicing**: Dividing the model into horizontal layers, determining layer thickness, and generating cross-sections.
    7. **Contour Generation**: Generate inner and outer perimeter of each layer
    8. **Infill Pattern Generation**: Generate infill patterns for each layer
    9. **Toolpath Planning**: Calculating the optimal path for the print head, including infill patterns and perimeter definitions.
    10. **G-code Generation**: Translating the 2D layer profiles and toolpaths into G-code instructions.



<!-- Detailed in chapter 2 -->

--}


### Mathematical Representation of Slicing

#### Surfacic Mesh Representation

A 3D model in the context of slicing can be represented as a mesh composed of triangular facets. Each triangle is defined by three vertices. Let \( V_1, V_2, V_3 \) be the vertices of a triangle, and each vertex is represented by a vector in 3D space:

\[
V_1 = (x_1, y_1, z_1), \quad V_2 = (x_2, y_2, z_2), \quad V_3 = (x_3, y_3, z_3)
\]

#### Intersection of Plane and Triangles

To slice the model, we intersect it with a series of horizontal planes at different heights \( z = h \), where \( h \) ranges from the minimum \( z \)-coordinate to the maximum \( z \)-coordinate of the model in steps of the layer thickness \( \Delta h \).

For a given plane \( z = h \), the intersection with each triangle is computed as follows:

1. **Edge Intersection Calculation**:
   - Each edge of the triangle is checked for intersection with the plane. An edge between vertices \( V_i \) and \( V_j \) intersects the plane if \( z_i \) and \( z_j \) straddle \( h \) (i.e., one is above and one is below or one is exactly on the plane).
   
   The parameter \( t \) for the intersection along the edge can be found using linear interpolation:

   \[
   t = \frac{h - z_i}{z_j - z_i}
   \]

   The intersection point \( P \) on the edge can then be calculated as:

   \[
   P = V_i + t (V_j - V_i)
   \]

   Expanding this for the coordinates, we get:

   \[
   P_x = x_i + t (x_j - x_i), \quad P_y = y_i + t (y_j - y_i), \quad P_z = h
   \]

   Since \( P_z \) is always \( h \), we only need to compute \( P_x \) and \( P_y \).

2. **Set of Intersection Points**:
   - Each triangle can yield 0, 1, or 2 intersection points depending on its orientation relative to the slicing plane. If a triangle is parallel to the slicing plane and lies exactly on it, it will be considered as producing a line segment (which is typically avoided in normal slicing operations).

3. **Forming Intersection Segments**:
   - For each triangle, if there are two intersection points, an intersection line segment is formed. Let these points be \( P_1 \) and \( P_2 \).

#### Finding Closed Contours

To form closed contours from the set of intersection segments:

1. **Segment Collection**:
   - Collect all intersection line segments from all triangles at a given slicing plane height \( h \).

2. **Segment Linking**:
   - The endpoints of these segments are then linked to form closed loops (contours). This involves:
     - Starting from an arbitrary segment.
     - Connecting it to the next segment that shares an endpoint with it.
     - Continuing this process until the starting point is reached, forming a closed loop.

3. **Handling Multiple Contours**:
   - There can be multiple disjoint closed contours in a single layer. The algorithm must track and form each closed loop independently.

### Example and Explanation

Consider a simple triangular mesh intersected by a plane \( z = h \):

1. **Mesh Triangles**:
   - Triangle 1: \( V_1 = (1, 1, 0) \), \( V_2 = (3, 1, 2) \), \( V_3 = (2, 3, 1) \)
   - Triangle 2: \( V_4 = (4, 4, 0) \), \( V_5 = (6, 4, 2) \), \( V_6 = (5, 6, 1) \)

2. **Intersection Calculation**:
   - For Triangle 1 and plane \( z = 1 \):
     - Edge \( V_1 \)-\( V_2 \): \( z_1 = 0 \), \( z_2 = 2 \), intersects at \( t = \frac{1 - 0}{2 - 0} = 0.5 \)
     - Intersection point: \( P = V_1 + 0.5 (V_2 - V_1) = (1, 1, 0) + 0.5 ((3, 1, 2) - (1, 1, 0)) = (2, 1, 1) \)
     - Similar calculations are done for other edges, resulting in intersection points \( (2, 1, 1) \) and \( (2.5, 2, 1) \).

3. **Closed Contours**:
   - Once all intersection points are calculated for a layer, they are linked to form contours.
   - If \( (2, 1, 1) \) and \( (2.5, 2, 1) \) are linked correctly, they form part of a contour.

### Diagram to Visualize the Process

1. **3D Model with Slicing Planes**:
   - Show a 3D model with horizontal planes intersecting it.

2. **Intersection Points and Segments**:
   - Show a plane intersecting a triangle and resulting intersection points and segments.

3. **Forming Closed Contours**:
   - Show the linking of intersection segments to form closed contours.

### Mermaid Diagram of Slicing Workflow

```mermaid
graph TD
    A[Model Preparation] --> B[Support Structure Generation]
    B --> C[Layer Slicing]
    C --> D[Contour Generation]
    D --> E[Toolpath Planning]
    E --> F[G-code Generation]

    subgraph Slicing Workflow
        A
        B
        C
        D
        E
        F
    end
```


Slicers not only create the toolpath but also manage various parameters to optimize print quality and efficiency. This automation is essential for handling the complexity and volume of instructions required for producing AM parts. The advancements in slicing software have made it possible to produce intricate designs with high precision, significantly expanding the capabilities and applications of additive manufacturing.

3D slicing is a crucial step in the additive manufacturing (AM) process, converting a digital 3D model into machine instructions that an AM printer can execute to create a physical object. This involves dividing the model into thin horizontal layers and generating a toolpath for each layer. The process requires several computational steps and algorithms to ensure accuracy, efficiency, and high-quality output.
 
The 3D model, usually in `.stl` (Stereolithography) format (or more recently `.amf` and `.3mf` formats), is imported into the slicing software. The STL file represents the object's surface geometry as a mesh of interconnected triangles.

The imported model is checked for errors such as non-manifold edges, holes, or intersecting faces. These errors are corrected to ensure the model is watertight (i.e., no gaps in the surface).

The model is divided into thin horizontal layers. The slicing algorithm involves intersecting the 3D model with a series of parallel planes.




3D slicing

Advanced slicing

5 axis or other kinematics


AM focuses on end-user parts, with challenges in quality, repeatability, and manufacturing time. while rapid prototyping is refered as the use of digital fabrication to make prototypes (not final products)

### 2.2.2 Traditional vs. Non-Planar Slicing

Overview of Traditional Slicing: Describe traditional planar slicing processes and their limitations, such as issues with layer adhesion, anisotropy in mechanical properties, and surface roughness.
Introduction to Non-Planar Slicing: Explain the concept of non-planar slicing, how it differs from traditional methods, and its benefits in overcoming those limitations.
Visual Comparisons: Use schematics to illustrate the differences between planar and non-planar slicing approaches.

### 2.2.3 Algorithms and Computational Models for Non-Planar Slicing

Algorithmic Foundations: Describe the algorithms that underpin non-planar slicing, focusing on how they calculate layer paths and manage overhangs without support structures.
Case Studies of Algorithm Implementation: Provide examples of how these algorithms are implemented in software, possibly referencing open-source projects or proprietary systems.
Challenges and Solutions: Discuss computational challenges such as increased slicing time, complexity in path planning, and integration with existing CAD/CAM software.

### 2.2.4 Software and Tools for 3D Slicing

Review of Available Tools: List and describe software tools that are popular in the industry for both traditional and non-planar slicing, noting any significant features or limitations.
Comparison of Performance: Analyze the performance of different tools based on criteria such as speed, accuracy, and the quality of the output.

### 2.2.5 Application and Case Studies

Industrial Applications: Discuss how advanced slicing techniques are being applied in various industries, such as aerospace, automotive, and healthcare.
Case Studies: Provide detailed case studies where non-planar slicing has been used to solve specific manufacturing problems or to enhance product performance.

### 2.2.6 Future Directions in 3D Slicing

Research Trends: Highlight ongoing research areas that may influence the development of new slicing techniques, such as machine learning for adaptive slicing strategies or integration with real-time monitoring systems.
Potential Innovations: Speculate on future innovations that could further enhance 3D slicing or disrupt traditional methodologies.

### 2.2.7 Summary

Recap of Key Points: Summarize the key information presented in this section, emphasizing the advancements and their implications for the field of AM.
Link to Subsequent Sections: Provide a bridge to the next section, possibly linking how improvements in 3D slicing could enhance other aspects of AM, such as closed-loop control and material optimization.


### Section 2.3: Simulation-Driven Design in Additive Manufacturing

#### 2.3.1 Introduction to Simulation-Driven Design
- **Definition and Overview**: Define simulation-driven design and its importance in the context of AM. Discuss how simulations are used to predict and optimize the manufacturing process from the earliest stages of design.
- **Benefits of Simulation-Driven Design**: Outline the advantages, such as reduced prototyping costs, improved product performance, and enhanced innovation through feasible complex geometries.

#### 2.3.2 The Role of Simulations in AM
- **Process Simulations**: Describe different types of simulations used in AM, such as thermal, mechanical, and fluid dynamics simulations that predict how the material behaves during the printing process.
- **Impact on Design**: Explain how these simulations influence design decisions, focusing on the ability to anticipate problems and adjust designs preemptively to avoid manufacturing issues.

#### 2.3.3 Integration of Simulation with Design Tools
- **CAD to Simulation Workflows**: Discuss how modern CAD tools integrate with simulation software to provide a seamless design-to-production workflow.
- **Case Examples**: Provide examples of tools or software suites that exemplify this integration and show how they are used in industry.

#### 2.3.4 Enhancing Simulation Speed and Accuracy
- **Challenges in Simulation**: Address common challenges in simulation, such as long computation times and the need for high accuracy in complex simulations.
- **Physics-Informed Neural Networks (PINNs)**:
  - **Introduction to PINNs**: Briefly introduce what physics-informed neural networks are and their role in enhancing simulation processes.
  - **Benefits of PINNs in AM**: Discuss how PINNs can potentially reduce simulation times significantly while maintaining or improving accuracy, particularly in complex AM processes.
  - **Current Research and Limitations**: Mention the current state of research in PINNs, highlighting some leading studies and their findings, while also noting that this technology is not a primary focus of your work but represents a significant trend in the field.

#### 2.3.5 Impact of Faster Simulations on Product Development
- **Reducing Iteration Time**: Elaborate on how faster and more accurate simulations can reduce the number of physical prototypes needed, thereby decreasing iteration times and overall time to market.
- **Enabling Design Creativity**: Discuss how improved simulation processes allow engineers more freedom to experiment with complex and innovative designs without the extensive time penalty traditionally associated with iterative testing and redesign.

#### 2.3.6 Summary
- **Recap of Key Points**: Summarize the benefits and advancements discussed in the section, emphasizing the strategic impact of simulation-driven design on the AM industry.
- **Link to Future Directions**: Suggest how these advancements might influence future developments in AM technologies and the potential integration with other innovative computational methods.

### 2.3.7 References
- **Comprehensive List of Cited Works**: Ensure all sources discussed in the section are properly cited, providing a basis for further reading.




### Section 2.4: Implementing Closed-Loop Control in Additive Manufacturing

#### 2.4.1 Introduction to Closed-Loop Control Systems
- **Definition and Fundamentals**: Define closed-loop control systems and explain their role in automated processes. Briefly introduce basic control theory concepts relevant to AM.
- **Importance in AM**: Discuss why closed-loop control is particularly critical in AM for achieving high-quality, consistent outputs.

#### 2.4.2 Challenges in AM Control Systems
- **Complex Material Behaviors**: Detail how the materials used in AM often exhibit highly nonlinear behaviors that are difficult to predict and model analytically, complicating the control process.
- **Lack of Real-Time Data**: Explain the challenges associated with obtaining real-time, in-situ data during the manufacturing process, which is essential for effective closed-loop control.

#### 2.4.3 Current Technologies and Approaches
- **Sensing Technologies**: Introduce the various types of sensors and data acquisition systems currently used in AM to gather necessary process data.
- **Data Processing and Feedback Mechanisms**: Describe how the collected data is processed and utilized to adjust the manufacturing parameters dynamically, ensuring adherence to design specifications.

#### 2.4.4 Case Studies and Applications
- **Examples of Closed-Loop Control in AM**: Provide real-world examples or case studies where closed-loop control systems have been successfully implemented in AM settings.
- **Benefits Realized**: Highlight the improvements achieved in these examples, such as enhanced precision, reduced waste, or improved mechanical properties of the final products.

#### 2.4.5 Integrating Advanced Control Theories
- **Role of Advanced Algorithms**: Discuss how more sophisticated control algorithms, including predictive and adaptive control theories, are being integrated into AM processes to handle the complexity and variability of the manufacturing environment.
- **Potential for AI and Machine Learning**: Touch on how AI and machine learning are beginning to play a role in enhancing control strategies by predicting system behaviors and optimizing control parameters in real-time.

#### 2.4.6 Future Directions in AM Control Systems
- **Innovations on the Horizon**: Speculate on future technological advancements that could further enhance closed-loop control in AM, such as more sophisticated sensors and smarter AI-driven control systems.
- **Challenges to Overcome**: Outline ongoing challenges that need to be addressed to fully realize the potential of closed-loop control in AM, focusing on the integration of real-time data analysis and the development of reliable models for complex material behaviors.

#### 2.4.7 Summary
- **Recap of Key Points**: Summarize the critical aspects of closed-loop control in AM discussed in this section, emphasizing the importance of continuous innovation in control strategies.
- **Link to Subsequent Sections**: Provide a transition to the next section, suggesting how improvements in closed-loop control could influence other areas of AM research and application.

### 2.4.8 References
- **Comprehensive List of Cited Works**: Ensure all theoretical frameworks, case studies, and innovative technologies discussed are properly cited.

This section will effectively cover the theoretical, practical, and future potential of implementing closed-loop control systems in additive manufacturing, providing a comprehensive view of both current capabilities and areas ripe for further research and development.






### Section 2.5: The Convergence of AI and Robotics in Digital Fabrication

#### 2.5.1 Introduction to AI in Robotics
- **Overview of AI Technologies in Robotics**: Begin by describing the types of AI technologies commonly integrated into robotics, such as machine learning, computer vision, and natural language processing.
- **Significance for Robotics**: Explain how AI enhances robotic capabilities with examples like improved decision-making, adaptability, and autonomy.

#### 2.5.2 Advancements in AI-Driven Robotics
- **Recent Innovations**: Highlight key advancements in AI that have transformed robotics, focusing on developments such as reinforcement learning and generative AI.
- **Case Studies**: Provide real-world examples of robotics innovations powered by AI that have significantly impacted industries, emphasizing those related to manufacturing.

#### 2.5.3 AI's Role in Enhancing Robotic Automation in Manufacturing
- **Automation and Precision**: Discuss how AI enhances the precision and efficiency of robotic systems in manufacturing settings, including the role of AI in programming and control systems.
- **Adaptive Manufacturing**: Explain how AI enables robots to adapt to varying manufacturing conditions and tasks, leading to more flexible production lines.

#### 2.5.4 Impact of AI-Driven Robotics on Digital Fabrication
- **Integration with Additive Manufacturing**: Dive into how AI-driven robotics can be specifically applied in the context of digital fabrication, particularly in terms of optimizing manufacturing processes and enhancing product customization.
- **Enhancing Design-to-Production Workflows**: Discuss how AI-driven robotics can streamline the workflow from design to production, reducing time-to-market and enabling more complex product designs.

#### 2.5.5 Future Directions and Challenges
- **Potential Future Innovations**: Speculate on future advancements in AI and robotics within digital fabrication, considering emerging trends and technologies.
- **Challenges and Considerations**: Address potential challenges in adopting AI-driven robotics in digital fabrication, such as integration complexity, cost implications, and workforce training needs.

#### 2.5.6 Summary
- **Recap of Key Points**: Summarize the insights shared about the impact of AI in robotics and its transformative potential for digital fabrication.
- **Link to Subsequent Sections**: Establish how this discussion leads into further exploration of specific AI applications in upcoming sections of the thesis.

### 2.5.7 References
- **Comprehensive List of Cited Works**: List all sources and research studies mentioned, providing a solid foundation for further exploration and validation.

This structured discussion not only outlines the technological interplay between AI, robotics, and digital fabrication but also sets the stage for deeper analysis on how these technologies collectively advance the manufacturing industry. By detailing current innovations and forecasting future trends, this section will contribute significantly to understanding the broader implications of AI and robotics in enhancing digital fabrication processes.






















Contributions

GCode Predictor corrextor -> Deform the input GCode to maximize quality.

RL Based control
Train an AGent by feeding the 3D Texture ? 2D Texture ? final state or current state ? 
Fix the texture size for the test


Python receive a GCode, After applying the predictor corrector we start the machine by feeding velocity 0 and 0 in the texture.
Place nozzle at first point. Then start control loop. 

Let the Agent control the nozzle and see what it does. while printing, 

if the agent fill a voxel that should be filled, raise the score ?




In this chapter, we delve into two pivotal components developed to enhance the control and quality of additive manufacturing processes: the GCode Predictor-Corrector and a reinforcement learning (RL) based control system.

#### GCode Predictor-Corrector

The first component, the GCode Predictor-Corrector, is designed to predict the output of the printing process and adjust the input GCode accordingly to match the desired 3D mesh. This predictor-corrector mechanism deforms the initial GCode instructions to compensate for any discrepancies between the expected and actual print outcomes, ensuring the printed object closely adheres to the intended design. By pre-processing the GCode, this system aims to minimize errors and optimize the initial conditions for the printing process.

#### RL-Based Control System

Building on the corrected GCode, the second component involves an RL-based control system that operates in real-time during the printing process. The goal of this system is to dynamically maximize print quality by allowing an agent to control the nozzle based on the current and objective 3D textures.

1. **Initialization and Setup**:
    - The process begins with Python receiving the optimized GCode from the Predictor-Corrector.
    - The machine starts with the nozzle positioned at the first point, and the control loop is initiated with an initial velocity of zero in the texture.

2. **Agent Control and Training**:
    - The RL agent is provided with two key pieces of information: the current 3D texture of the print in progress and the objective 3D texture, which includes voxelated representations of material presence.
    - The agent's task is to fill the voxels in the objective 3D texture accurately, avoiding the filling of voxels that should remain empty.

3. **Real-time Adjustments and Feedback**:
    - As the printing process unfolds, the agent continuously adjusts the nozzle's position and extrusion parameters to align the current texture with the objective texture.
    - The agent receives feedback based on its performance: if it successfully fills a voxel that should be filled, its score is increased, reinforcing correct behavior. Conversely, if the agent fills a voxel that should be empty, its score is penalized.

4. **Performance Optimization**:
    - Through iterative training, the agent learns to optimize its control strategies, improving its ability to produce high-quality prints.
    - The real-time nature of this system allows for immediate corrections and adjustments, ensuring that any deviations from the desired print quality are promptly addressed.

By integrating the GCode Predictor-Corrector with the RL-based control system, we create a robust framework that not only preemptively optimizes the GCode but also actively manages the printing process to maximize quality. This dual approach ensures that the printed object matches the intended design as closely as possible, leveraging both pre-processing adjustments and dynamic, intelligent control during printing.

Overall, this framework represents a significant advancement in the numerical control of manufacturing machines, providing a sophisticated method for enhancing the precision and quality of additive manufacturing processes through intelligent, real-time optimization.