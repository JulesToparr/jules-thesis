# Introduction au Langage R

R est un logiciel disponible sur les systèmes d'exploitation Linux et Windows, utilisé notamment pour l'analyse de données. Durant ce cours, dispensé par Jonathan Lenoir de l'unité edysan (Ecologie et Dynamique des Systèmes Anthropisés), nous explorerons les bases de l'analyse de données avec le logiciel R.


## Question :
- Peut on charger un fichier en lecture seul ?
- Peut on charger des fichier depuis un repo ?
- Les calculs sont assez long même sur des petits datasets
  - Progression des calculs ?
  - R est il basé sur des fonction CPP ?
  - Si oui, peut-on les appelé direct depuis un code cpp ?
-> Julia et python vont être peut plus recomandé pour des gros set.
package lidR -> pour le traitement des lidar.








## Introduction

R est un langage interprété et orienté objet, ce qui signifie qu'il n'est pas compilé et qu'il permet de manipuler des objets dans un environnement interactif. Il offre une fenêtre de commande pour envoyer des instructions et une fenêtre graphique pour visualiser les résultats. Bien que RStudio offre un environnement de développement plus complet, il peut paraître plus complexe au premier abord.

R dispose d'un vaste écosystème de modules ou packages alimentés par une communauté active d'utilisateurs et de développeurs. Cela permet d'étendre les fonctionnalités de base de R selon les besoins spécifiques des utilisateurs. De plus, R offre la possibilité de générer des rapports en différents formats, y compris en Rmarkdown ou en PowerPoint.

Pour quitter R, il suffit d'utiliser la commande `q()`. Par défaut, RStudio stocke les informations utilisateur sur l'interface utilisateur (UI layout), ce qui peut rapidement occuper de l'espace. Cette fonctionnalité peut être désactivée dans les options globales. De plus, il est possible de configurer une marge dans les paramètres pour que le code revienne automatiquement à la ligne.

Pour exécuter une portion de code, il suffit de sélectionner le code à exécuter. Cependant, l'utilisation de chemins d'accès absolus peut rendre la compatibilité entre différents environnements impossible. Il est donc recommandé d'utiliser le concept de projet dans RStudio, ce qui permet de choisir l'emplacement de sauvegarde et de garantir une meilleure portabilité du code.

Dans la structure des dossiers d'un projet, il est intéressant de séparer les données brutes des données produites afin de faciliter la gestion et la traçabilité des transformations effectuées. En effet, le workflow d'analyse de données est crucial en science des données, d'où la nécessité d'avoir une traçabilité des transformations sans altérer les données d'origine.

## Manipulation des Données

Lors de l'analyse de données, il est souvent nécessaire de manipuler des valeurs manquantes (NA) ou des valeurs aberrantes. La fonction `mean` permet de calculer la moyenne d'un ensemble de données, avec la possibilité de "tronquer" les valeurs aberrantes en utilisant l'argument `trim`. Par exemple, en définissant `trim` à 0.25, seules les valeurs situées à 25% à gauche et à droite de la moyenne seront prises en compte.

R est compatible avec C++, ce qui permet d'utiliser du code C++ dans R via le package `Rcpp`. Cela ouvre la possibilité d'exécuter des fonctions R à l'intérieur du code C++, offrant ainsi une grande flexibilité dans le développement de logiciels statistiques complexes.


```R
# Exemple de Code avec Commentaires

# Affectation d'une valeur à une variable
myvar <- 5

# Affichage de la valeur de la variable
print(myvar)

# Affichage du répertoire de travail
getwd()

# Affichage du contenu du répertoire
dir()

# Tracé d'un graphique simple
plot(1:20/2)

# Commentaires expliquant l'affectation d'une valeur à une variable
# myvar <- 5
# myvar = 5

# Affichage des objets déjà créés
ls()

# Suppression de la variable myvar
rm(myvar)

## Opérateur %/% pour la division entière
## Opérateur %% pour le modulo
## Opérateur %*% pour la multiplication de matrices

## Définition du répertoire de travail
setwd()
```

Ce code contient des exemples d'instructions en langage R, accompagnés de commentaires explicatifs pour clarifier leur fonctionnement.


## Manipulation des Jeux de Données

R dispose de jeux de données intégrés, accessibles via la fonction `data()`. Par exemple, le jeu de données "pressure" peut être chargé en utilisant la commande `data(pressure)`. Ces jeux de données sont souvent structurés sous forme de data frames, qui sont des tableaux hétérogènes de données. Contrairement aux matrices, les data frames peuvent contenir différents types de données dans chaque colonne.

Pour explorer la structure d'un objet en R, la fonction `str()` est utile. Elle permet de visualiser la structure de l'objet, ce qui est particulièrement pratique lors de l'exploration de jeux de données complexes.

En résumé, lors de l'utilisation de R pour l'analyse de données, il est essentiel de comprendre les bases de la manipulation des données, y compris la gestion des valeurs manquantes et des valeurs aberrantes, ainsi que la structure des jeux de données. De plus, la compatibilité avec d'autres langages comme C++ offre des opportunités pour étendre les fonctionnalités de R et créer des solutions analytiques plus avancées.

```R
# Exemple de code avec description des fonctions

# Calcul de la moyenne en tronquant les valeurs aberrantes
# Arguments:
#   - x: Vecteur de données
#   - trim: Pourcentage de valeurs à tronquer des deux extrémités (par défaut = 0)
# Renvoie la moyenne tronquée
mean_trim <- function(x, trim = 0) {
  return(mean(x, trim = trim))
}

# Utilisation de la fonction mean_trim avec un jeu de données
data(pressure)
pressures <- pressure$pressure
mean_pressure <- mean_trim(pressures, trim = 0.25)
print(mean_pressure)
```

Ce code illustre une fonction `mean_trim` qui calcule la moyenne d'un vecteur de données en tronquant un pourcentage spécifié de valeurs aberrantes des deux extrémités. Ensuite, cette fonction est utilisée pour calculer la moyenne tronquée des pressions atmosphériques dans le jeu de données "pressure".


## Statistics on data frame

```R
# Exemple de Code avec Fonctions Statistiques Utiles

# Création d'un vecteur de données
data <- c(2, 4, 6, 8, 10, 12)

# Calcul du minimum
min_value <- min(data)
print(paste("Minimum:", min_value))

# Calcul du maximum
max_value <- max(data)
print(paste("Maximum:", max_value))

# Calcul de l'écart-type
standard_deviation <- sd(data)
print(paste("Écart-type:", standard_deviation))

# Calcul de l'erreur standard
standard_error <- standard_deviation / sqrt(length(data))
print(paste("Erreur standard:", standard_error))

# Calcul de la moyenne
mean_value <- mean(data)
print(paste("Moyenne:", mean_value))

# Calcul de la variance
variance <- var(data)
print(paste("Variance:", variance))

# Calcul de la médiane
median_value <- median(data)
print(paste("Médiane:", median_value))

# Calcul du mode
# Fonction Mode personnalisée
mode <- function(x) {
  ux <- unique(x)
  ux[which.max(tabulate(match(x, ux)))]
}
mode_value <- mode(data)
print(paste("Mode:", mode_value))
```

Ce code présente des exemples de calculs statistiques courants réalisés sur un ensemble de données, notamment le minimum, le maximum, l'écart-type, l'erreur standard, la moyenne, la variance, la médiane et le mode.

## Tests statistique

C'est quoi le test de student ?

Pour executer le test de student utiliser la fonction 

```R
t.test(pressure$temperature, mu = 0)
```
Result:
```R
One Sample t-test

data:  pressure$temperature
t = 6.9714, df = 18, p-value = 1.641e-06
alternative hypothesis: true mean is not equal to 0
95 percent confidence interval:
 125.7544 234.2456
sample estimates:
mean of x 
      180  
```


avant de faire un test de student, il faut faire un test de variance pour s'assurer que les variances des echantillons sont égales.

D'autre test comme de test de ? Welsh ? sont plus permissifs.


```R
plot(pressure$temperature, pressure$pressure)
```

Il existe quelques paramètre de mise en forme tel que :

```R
plot(pressure$temperature, pressure$pressure, col="red", main = "Titre du graphique", xlab="label des X", breaks = seq(0, 900, 10))
``` 

La fonction seq permet de créer des sequence de nombre entre deux bornes.
Par exemple : 


```R
seq(0, 100, length.out=5)
```

donnera 

```
[1]   0  25  50  75 100
```
et 

```R
seq(min(pressure$pressure), max(pressure$pressure), length.out=5)
```

```
[1]   0.0002 201.5002 403.0001 604.5000 806.0000
```

Le paramètre pch d'un plot permet de changer le type de marqueur de chaque point. pch = 16 donnera un cercle pour chaque point. (15 donne un carré)

Le paramètre cex permet de gérer la taille du point.

col = rgb(0.5,0.5,1,0.2) permet de set une couleur personnalisé.

```R
plot(pressure$temperature, pressure$pressure, col=rgb(1,0.4,0.4,0.5), main = "Graphe Thermodynamique", xlab="label des X", breaks = seq(0, 900, 10), pch=15, cex=3)
```
donnera

![](./images/plot.png)

---

## Modélisation

Pour créer un modèle ) partir de donneé, il y a une fonction pour la régression linéaire.

```R
mold <- lm(pressure~temperature, data = pressure)
summary(modl)
```

```R
[1]
Call:
lm(formula = pressure ~ temperature, data = pressure)

Residuals:
    Min      1Q  Median      3Q     Max 
-158.08 -117.06  -32.84   72.30  409.43 

Coefficients:
             Estimate Std. Error t value Pr(>|t|)    
(Intercept) -147.8989    66.5529  -2.222 0.040124 *  
temperature    1.5124     0.3158   4.788 0.000171 ***
---R
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 150.8 on 17 degrees of freedom
Multiple R-squared:  0.5742,	Adjusted R-squared:  0.5492 
F-statistic: 22.93 on 1 and 17 DF,  p-value: 0.000171
```

Note : se documenter sur les DoF en statistique et en modèlisation
Check : Test de fisher

On peut tester avec un polynome : 
```R
modl <- lm(pressure~poly(temperature,2), data = pressure)
summary(modl)
```

```R
Call:
lm(formula = pressure ~ poly(temperature, 2), data = pressure)

Residuals:
    Min      1Q  Median      3Q     Max 
-95.142 -54.391  -1.353  48.238 170.374 

Coefficients:
                      Estimate Std. Error t value Pr(>|t|)    
(Intercept)             124.34      17.07   7.283 1.83e-06 ***
poly(temperature, 2)1   722.17      74.42   9.704 4.16e-08 ***
poly(temperature, 2)2   545.95      74.42   7.336 1.67e-06 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 74.42 on 16 degrees of freedom
Multiple R-squared:  0.9024,	Adjusted R-squared:  0.8902 
F-statistic:    74 on 2 and 16 DF,  p-value: 8.209e-09
```

On peut executer un test d'analyse de variance sur le modèle avec la fonction `anova(modl)`

On peut aussi executer un test d'analyse de variance comparative sur les deux modèle pour comclure sur la pertinence d'utilisation d'un degré supplémentataire.

lorsqu'on calcul le log d'un jeu de donnée on peut utiliser cette astuse pour eviter le -Inf

log(pressure$temperature + 1). Ceci va ajouter +1 à tout la serie statistique avant d'agir.

Pour les cours de prod ca peut être pas mal.

[[1]] sert à retourner la valeur d'un élément plutôt que sa l'objet qui l'englobe.

le parametre type="n" permet d'afficher un graph vide

![](./images/plot2.png)

Dernière données Diapo Iris jeud de donnée

13 Mai