# Vidéos :

## Printing methods introduction

??? info "Material jetting basics"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/_VcJfe-Dm0c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Vat polymerization basics"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/fql_2zRIxI0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Powder bed fusion basics"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/fql_2zRIxI0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Design thinking by markforged"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/U0xxd70g0y0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Design thinking for AM"
    <iframe width="800" height="713" src="https://www.youtube.com/embed/33vzF_pce0A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Advanced Science News

??? info "Advanced Intelligent Systems – Vol. 3 No.10 – October 2021"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/XGLU-sl8K1c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Unsorted :

??? info "Viscoelastic ink"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/Vt7rol8rYkQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Futuristic Modeling Strategies for Additive Manufactured Multi Material Structure"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/RmPKzJq_wHY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Coaxiale extruder"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/JCINI5pHP4Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Prusa XL"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/9peACH52KTo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Dépot céramique"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/NkDGLtryPPM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Multi-nozzle"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/2WL4b03Tfjg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Bioprinting MIT"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/gL_KuEu9ABQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Piezo electric printing"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/CznsKwlUwWg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Opendrop - Electrowetting"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/6j_ecB5RcNI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Carbon fiber"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/KfcJrGX5oqY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

??? info "Metal atomization"
    <iframe width="800" height="500" src="https://www.youtube.com/embed/Oz3-GSmLwTE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Channels :

[CNC Kitchen channel](https://www.youtube.com/channel/UCiczXOhGpvoQGhOL16EZiTg)

[Electrowetting](https://www.youtube.com/channel/UCze-aBdbS0B_oJMPvCs85dA)

[Markforged](https://www.youtube.com/channel/UCg90L1Q_pjC4faYedwVAyRw)