
# Tasks list

## Blog related
- [x] Create a blog to organize and share advancement
- [x] Create a home page presenting the subject and my state of mind.
- [x] Create a page dedicated to bibliography structure
- [x] Create a page with all experiment I'm doing
- [x] Create a todo-list page

## Machine
- [x] Meet final users of the research platform
- [x] Identify key manufacturing process
- [x] Create methods to create new tools fast
- [x] Assemble the E3D Machine
- [x] Calibrate the E3D Machine
- [x] Test software and evaluate the whole workflow of each
- [x] Print missing extruder parts (4/4)

## State of the art
- [x] Identify major actors, writer and researcher working and the thematic
- [ ] Write a something about known and tested fabrication process
- [ ] Identify plus and weakness of each process
- [ ] Study and write about these subjects : 
  - [ ] Polymer and organic chemistry
    - Charged polymer
    - Photo reactive polymer
    - Carbon fiber, Carbon nanotube and graphene
  - [x] Digital fabrication
    - 3D printing battery
    - Hybrid manufacturing
    - Fused deposition modeling FDM
    - Stereo lithography SLA
    - Material jetting MJ
    - Binder jetting 3DP
    - Gel deposition modeling
  - [ ] Smart materials
    - 4D material
    - Shape memory materials
    - Artificial muscles
    - 3D printed actuators
    - Self sensing materials


## Research and development
- [ ] Design & build a 6 extruder machine
  - CAD Design
  - CAM Programming
  - Manufacturing
  - Assembly
  - Test and calibration

- [ ] Simulation and modeling
  - [x] Implement a DEM Solver (CPU)
  - [x] Implement a PBD Solver (GPU)
  - [x] Implement a GPU XPBD Solver (GPU)
  - [x] Implement a GPU PBF Solver (GPU)
  - [ ] Implement a GPU MPM Solver (GPU)
  - [ ] Implement a GPU LBM Solver (GPU)

- [ ] Simulation experiments
  - [x] Mono-material softbody simulation (CPU)
  - [x] Mono-material incompressible fluid simulation 2D (CPU)
  - [x] Multi-material incompressible fluid simulation 2D (CPU)
  - [x] Mono-material incompressible fluid simulation 3D on (GPU)
  - [x] Mono-material incompressible fluid with heat transfer coupled to visco-elasticity simulation 3D on (GPU)

