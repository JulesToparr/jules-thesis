# Multi-Material Additive Manufacturing : Optimization through real-Time meshfree simulation

---
## Abstract

{++

The contemporary industrial landscape is undergoing transformative shifts, driven by pressing climatic, energy, and pandemic crises. These challenges have exposed the vulnerabilities of traditional manufacturing and supply chain systems, underscoring the necessity for more resilient and adaptive production methods. In response to these needs, additive manufacturing (AM) has emerged as a pivotal technology, offering the flexibility and localization required to address these disruptions effectively.

Additive manufacturing, particularly in its decentralized form, enables localized production, reducing dependency on global supply chains and enhancing the ability to quickly adapt to changing circumstances. This capability is crucial in mitigating the impacts of climate change, energy constraints, and pandemics, all of which demand more responsive and sustainable manufacturing practices. [@cutchergershenfel] [@Choong2020TheGR]

As the industrial sector moves towards more advanced and integrated technological solutions, the role of digital products, mechatronics, and cyber-physical systems becomes increasingly significant. Within this context, multi-material additive manufacturing (MMAM) stands out as a vital innovation. MMAM facilitates the creation of complex structures with diverse material properties in a single production process, enabling the development of advanced components that meet the rigorous demands of modern applications.

However, the integration of multiple materials in MMAM is fraught with challenges. Issues such as material compatibility, differential thermal expansion, and the warping effect complicate the production process, often leading to suboptimal results and reduced component quality. These challenges necessitate sophisticated control and optimization strategies to fully harness the potential of MMAM.

This thesis addresses these challenges by focusing on the optimization of MMAM processes through real-time simulation and simulation-driven control. By incorporating real-time data acquisition and predictive modeling, this research aims to improve the accuracy, efficiency, and reliability of MMAM. The goal is to develop a robust framework that can adaptively manage the complexities of multi-material production, thereby enhancing the overall capabilities of decentralized manufacturing systems.

```
or 

This research addresses these challenges by developing an innovative real-time simulation framework aimed at optimizing MMAM processes and reducing defects, with a special emphasis on the intricate warping phenomenon. The cornerstone of this framework is a GPU-based multiphysics simulation engine, which enables swift analysis and optimization of printing parameters. This engine is a blend of cutting-edge computational techniques borrowed from computer graphics and advanced robotics, including real-time simulation capabilities and reinforcement learning algorithms.
```

Through this work, we seek to contribute to the development of more resilient and adaptive industrial practices, aligning with the broader goal of creating sustainable and flexible manufacturing systems that can withstand the pressures of contemporary crises.

Chapter 1 provides an introduction to the concepts of MMAM, outlining its definitions, scope, and the historical evolution of additive manufacturing. This chapter reviews the state-of-the-art technologies and identifies the primary challenges faced in the field, including material compatibility, process control, and mechanical property optimization. The literature review within this chapter highlights significant advancements and gaps that this research aims to address.

In Chapter 2, the focus shifts to advanced manufacturing control strategies. This chapter delves into the current methods of controlling additive manufacturing machines and draws parallels with robotic systems. By analyzing the convergence of additive manufacturing and robotics, this chapter identifies new opportunities for integrating modern control strategies into AM machines. It emphasizes the need for fast and accurate simulations to enable inverse design, rapid iteration of machine designs, and control algorithms, along with the potential of reinforcement learning to optimize manufacturing processes.

Chapter 3 sets a framework for building a robust, fast simulation environment tailored for MMAM. It begins with a detailed explanation of continuum mechanics, distinguishing between fluid and solid mechanics. 

Chapter 4 is dedicated to numerical methods, such as Finite Element Methods (FEM), and identifies their limitations in the context of MMAM. To address these limitations, a set of alternative framework are proposed. The chapter also discusses real-time considerations of simulation and explores position-based dynamics and other relevant methods to accelerate simulations.

Chapter 5 details the implementation of real-time simulation by leveraging GPU computing. 

Chapter 6 focus on developping a real-time scientific vizualisation toolbox.

Chapter 7 is dedicated to our multiphysics framework. This chapter covers the architectural design and algorithms necessary for effective real time simulations along with integration schemes and implementation specifics. Performance analysis is conducted to demonstrate the effiency of the framework, with detailed metrics and comparative studies. The chapter also emphasizes the user-friendly approach of the framework, ensuring accessibility for practitioners.

Chapters 8 and 9 are dedicated to applying the developed simulation framework to address specific technological challenges in MMAM. These chapters present case studies demonstrating the practical applications and effectiveness of the framework in solving real-world problems. The impact of these solutions on the field of MMAM is analyzed, and potential future research directions are proposed.

Overall, this thesis aims to provide a comprehensive and educational exploration of MMAM, advanced control strategies, and numerical simulation methods, culminating in the development of a high-performance, real-time simulation framework. The research contributes to the advancement of MMAM by offering innovative solutions to existing challenges and paving the way for future developments in the field.


## Table of content

**Introduction**<br>

- **Chapter 1: Multimaterial Additive Manufacturing (MMAM)**
	- 1.1 Background  
	- 1.2 Recent Advances  
	- 1.3 Challenges and Opportunities  
	- 1.4 Theoretical Background in MMAM  

- **Chapter 2: Advanced Manufacturing Control Strategies**
	- 2.1 The Need for Advanced Control in MMAM  
	- 2.2 Principles of Robotics Control Applied to MMAM  
	- 2.3 Comparative Analysis of Control Systems  
	- 2.4 Implementation of Robotics Control in MMAM  
	- 2.5 Simulation and Inverse Design  
	- 2.6 Reinforcement Learning in MMAM  

- **Chapter 3: Numerical Methods for MMAM**
	- 3.1 Constitutive Equations  
	- 3.2 Simulation Methods in Additive Manufacturing  
	- 3.3 Meshfree Methods: Principles and Applications  
	- 3.4 SPH vs. FEM: A Comparative Analysis  
	- 3.5 Real-Time Considerations in Simulation  

- **Chapter 4: Implementing Real-Time Multiphysics Modeling in MMAM**
	- 4.1 Design and Architecture of the Simulation Framework  
	- 4.2 GPU Mass Parallelization  
	- 4.3 Detailed Algorithm Design  
	- 4.4 Integration Schemes and Implementation Details  
	- 4.5 Performance Analysis  

- **Chapter 5: Application to Technological Challenges**
	- 5.1 Case Study 1: Warping in MMAM  
		- 5.1.1 Problem Definition and Hypothesis  
		- 5.1.2 Highlighting the Influence of Toolpath  
		- 5.1.3 Simulation-Based Optimization  
	- 5.2 Case Study 2: Binder Jetting in MMAM  
		- 5.2.1 Problem Definition and Hypothesis  
		- 5.2.2 Highlighting the Influence of Toolpath  
		- 5.2.3 Simulation-Based Optimization  

- **Conclusion and Future Work**

**References**<br>
**Appendices**

++}
--- 


## Notation

- \( \vec{v} \) : A column vector
- \( \vec{v}^{\intercal} \) : The transpose of a column vector (row vector)
- \( v_i \) : The \(i_{th}\) element of a vector (first-order tensor)
- \( \vec{u} \cdot \vec{v} \) : The dot product of two vectors
- \( \vec{u} \times \vec{v} \) : The cross product of two vectors
- \( \mathbf{A} \) : A matrix (second-order tensor)
- \( \mathbf{A}^{-1} \) : The inverse matrix of \( \mathbf{A} \)
- \( \mathbf{A}^{\intercal} \) : The transpose matrix of \( \mathbf{A} \)
- \( \mathbf{A}_{ij} \) : The \(i_{th} j_{th}\) element of a matrix, considering \( \mathbf{A} \) as a second-order tensor
- \( \mathbf{I} \): The second-order identity tensor

### Tensor Operations

- \( \mathbf{A} \cdot \mathbf{B} = \mathbf{A}_{ij} \mathbf{B}_{jl} \) : Matrix multiplication (summation over repeated indices is implied)
- \( \mathbf{A} : \mathbf{B} = \mathbf{A}_{ij} \mathbf{B}_{ji} \): The inner product of two second-order tensors (double contraction)
- \( \mathbf{A} \otimes \mathbf{B} = \mathbf{A}_{ij} \mathbf{B}_{kl} \): The outer product of two second-order tensors

### Invariants

- \( \text{tr}(\boldsymbol{\sigma}) \): The trace of a tensor \( \boldsymbol{\sigma} \) (sum of the diagonal elements)

- $I_1$ = First invariant of a tensor
- $I_2$ = Second invariant of a tensor
- $I_3$ = Third invariant of a tensor


Math Toolbox :

Sets :

- \( \{1, 2\}\) : is a set containing two element : 1 and 2
- \(\mathbb{N}\) : define the natural numbers set \(\mathbb{N} = \{1,2,3,...\}\)
- \(\mathbb{Z}\) : define the integers set \(\mathbb{Z} = \{...,-2,-1, 0, 1, 2, ...\}\)
- \(\mathbb{Q}\) : define the rational numbers set \(\mathbb{Q} = \{\frac{a}{b} | a, b \in  \mathbb{Z}, b \neq 0\}\)
- \(\mathbb{R}\) : define the real numbers set \(\mathbb{N} = (-\infty , \infty)\)

Symbols

- \(  \in   	\) : contained in
- \(  \subset   \) : is a subset of
- \(  \cup   	\) : union
- \(  \cap   	\) : intersection
- \(  \forall   \) : for every
- \(  \exists   \) : exist
- \(  \exists ! \) : exist and unique

Components

- \(  x \in R^2  \) : two component vector
- \(  x \in R^3    \) : three component vector
- \(  M \in M^3    \) : M is a \(3 \times 3\) matrix

Tools : 

\( \delta_{ij} =
\begin{cases}
    1,& i=j \\
    0,& i\neq j
\end{cases}
\) 

(Kronecker delta)

\( \mathcal{E}_{ijk} =
\begin{cases}
    1,& \text{cyclic permutation of ijk} \\
    -1,& \text{non cyclic permutation of ijk }\\
    0,& \text{otherwise}
\end{cases}
\)

(Permutation)


