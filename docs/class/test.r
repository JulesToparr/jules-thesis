library(tidyverse)
library(readxl)
library(httpgd)

# Démarrer le serveur httpgd
#hgd()

# Lire le fichier Excel
adia <- read_excel("docs/class/adiabatique.xlsx")
print(head(adia))

# Renommer les colonnes
adia <- rename(adia, 
               T_deg = "TmoyAnn",
               Alt_m = "Alt",
               Massif = "MassMont",
               Xlb_m = "Xlamb2",
               Ylb_m = "Ylamb2")

# Manipuler les données pour obtenir min, max et mean
summary_massif <- adia %>%
  select(c(Massif, T_deg, Alt_m)) %>%
  group_by(Massif) %>%
  summarize(across(c(T_deg, Alt_m), list(min = min, max = max, mean = mean), .names = "{col}_{fn}"),
            N = n())

adia$Massif <- as.factor(adia$Massif)

# Manipuler les données pour obtenir min, max et mean
summary_massif <- adia %>%
  select(c(Massif, T_deg, Alt_m)) %>%
  group_by(Massif) %>%
  summarize(
    T_deg_min = min(T_deg, na.rm = TRUE),
    T_deg_max = max(T_deg, na.rm = TRUE),
    T_deg_mean = mean(T_deg, na.rm = TRUE),
    Alt_m_min = min(Alt_m, na.rm = TRUE),
    Alt_m_max = max(Alt_m, na.rm = TRUE),
    Alt_m_mean = mean(Alt_m, na.rm = TRUE),
    N = n()
  )

# On ajoute les données max min mean sur la gauche du tableau
#adia_extended <- left_join(adia, summary_massif, by = "Massif")

print(head(summary_massif))

# Créer et afficher un graphique ggplot
# p <- ggplot(adia_extended, aes(x = Alt_m, color = Massif)) +
#   geom_histogram(breaks = seq(0, 2600, 250)) + facet_wrap(vars(Massif)) +
#   labs(title = "Température en fonction de l'altitude par massif",
#        x = "Altitude (m)")
library(viridis)

# ggplot(adia_extended, aes(x = Alt_m, fill = Massif)) +
#   geom_histogram(breaks = seq(0, 2600, 20), position="stack") +
#   labs(title = "Température en fonction de l'altitude par massif",
#        x = "Altitude (m)")  +
#       scale_color_viridis(discrete = TRUE, option = "D") +
#       scale_fill_viridis(discrete = TRUE) +
#       theme_classic() -> p


# print(p)  # Utilisez print() pour afficher le ggplot

# ggplot(adia, aes(x = Alt_m, y = T_deg, col = Massif)) +
#   geom_point(alpha = 0.1) +
#   geom_smooth(method = "lm", formula = y ~ poly(x, 1), se = FALSE) + 
#   scale_fill_manual(values = palette(),
#                     name = "Massif",
#                     labels = c("Alpes", 
#                                "Corse", 
#                                "Jura",
#                                "Massif Central",
#                                "Pyrénées",
#                                "Vosges")) +
#   labs(title = "Température en fonction de l'altitude par massif",
#        x = "Altitude (m)", y = "Temperature (°)")  +
#       theme_classic() -> p2

# print(p2)

ggplot(NULL, aes(x = Alt_m, y = T_deg, col = Massif)) +
  geom_point(data = adia, alpha = 0.1) +
  geom_smooth(data = adia, method = "lm", formula = y ~ poly(x, 1), se = FALSE) + 
  scale_fill_manual(values = palette(),
  name = "Massif",
    labels = c("Alpes", 
                "Corse", 
                "Jura",
                "Massif Central",
                "Pyrénées",
                "Vosges")) +
  labs(title = "Température en fonction de l'altitude par massif",
       x = "Altitude (m)", y = "Temperature (°)")  +
  geom_point(aes(x = Alt_m_mean, y = T_deg_mean, size = 4, shape = factor(Massif)), data = summary_massif, inherit.aes = TRUE) +
  theme_classic() -> p2

# print(p2)

print(ggplot(adia, aes(x = T_deg, col = "blue")) + geom_histogram())
