# Documentation MERLIN

Merlin is a GPU computing and rendering engine designed as a sandbox as an alternative of CUDA or OpenCL.
It uses Compute Shaders to perform parallel computation while providing basic rendering functionality to prototype real-time physics applications.

## Prerequisites

As Merlin is built for GPU computing. Many projects require a high end GPU that supports OpenGL 4.5.
Even if, it is possible to run the applications on the CPU integrated graphics chipset. We recommend using a dedicated GPU. 

Sometimes Merlin application start running on the wrong GPU. We are still investigating this issue. The project was configured to desactivate this kind of driver optimization but the issue is still occuring. You can still desactivate one of the GPU using the OS settings.

## 1. Getting Started

To start using MERLIN, we first need to clone the project using [Git](https://git-scm.com/). Merlin is already provided with all third party libraries and tools as submodules.

### Clone repository

Run this command to install the framework and its dependencies : 

```
git clone --recurse-submodules https://github.com/yourusername/Merlin.git
```
### Project Configuration

Then the project can be compiled using your favorite compiler. Automatic configuration script is provided based on (Premake)[https://premake.github.io/]

Premake is already embed in Merlin so you don't have to install it manually. However, if you plan to use make many new projects, feel free to install it permanently. To run the configuration, you can execute permake by running : 

```
./extern/premake5/premake5.exe vs2022
```

You can use any compiler configuration using the supported compilers actions provided by Premake. Merlin is currently only supported on Windows platform but can be configured for cross platform compilation as well. 

On Windows :

| Visual Studio Code |
| ------ | 
| vs2022 |
| vs2019 |
| vs2017 |
| vs2015 |
| vs2013 |
| vs2012 |
| vs2010 |
| vs2008 |
| vs2005 |


Cross-platform : 

| Other compilers | Description |
| ------ | -------------------- |
| gmake2 | Generate GNU Makefiles(including Cygwin and MinGW) |
| xcode4 |	XCode projects |
| codelite | CodeLite projects |

### Building Merlin

After the automated configuration, you can open the generated project files using your following IDE. On Windows, open the `.sln` file.

Merlin is a static library supplied with many existing demo-projects. Starting any demo project will start the compilation of the library. 

If you want to use the library outside of the Merlin project. Generate the merlin.core project and browse the `bin` folder at the root of the project to find the generated `.lib` files. To incldue Merlin headers in you project, just include the `Merlin.h` located in `Merlin\merlin.core\src`

## 2. Architecture

Merlin is a big project with many features. Although it is still in active development and the architecture may evolved quite a bit in the future. Anyway, we will introduce the current architecture in this section. 

Merlin is essentially an OpenGL wrapper, originally based the work of [TheCherno](https://github.com/TheCherno/OpenGL/tree/master) available under [Apache2 licence](https://www.apache.org/licenses/LICENSE-2.0).

The library proveide OpenGL features using OOP programming methodology. [OpenGL](https://www.opengl.org/) features were integrated into C++ classes to provide a high level interface to lower the programming overhead of raw OpenGL functions. However, the OpenGL features are still accessible and can be used seamlessly with the library.

Merlin is based on [GLFW](https://www.glfw.org/) to manage windows, context, mouse and keyboard inputs. It is also embeding ImGui to create Applications user interface.

### Project structure

The architecture is divided into ten fundational bricks. 
The library sources can be found in the following folder (from root directory) : 

`./merlin.core/src`

The application frame, user inputs, framerate and OpenGL configurations are implemented in the following folders :

`merlin.core/src/merlin/core`
`merlin.core/src/merlin/events`

ImGui is integrated to provide a convenient way to create user interface in Merlin application. The code related to the UI is available here :

`merlin.core/src/merlin/ui`

The code regarding OpenGL objects such as textures, SSBO, VBO, EBO, VAO, Shaders... Is implemented in the following folders : 

`merlin.core/src/merlin/memory`
`merlin.core/src/merlin/shaders`
`merlin.core/src/merlin/textures`

The main renderer is available in :

`merlin.core/src/merlin/scene`
`merlin.core/src/merlin/graphics`

The code reponsible for particleSystem simulation is located in the physics folder :

`merlin.core/src/merlin/physics`

Additionally tools such as ModelLoader, TextureLoader are implemented in the Utilities folder :

`merlin.core/src/merlin/utils`

This folder also contains the code for voxelization, and other specific algorithms.

Finnaly, the framework provide existing shaders code used rather for rendering of computing.

All of them are available is the `assets` folder under `merlin.core`.

## 3. Practical example
### Application

To better understand how Merlin is made, let's dive into a basic application `cpp` file.

```c++
#include <merlin.h>
#include "Examplelayer.h"

using namespace Merlin;

class Example : public Application
{
public:
	Example()
		: Application("OpenGL Examples")
	{
		pushLayer(new ExampleLayer());
	}
};

int main()
{
	std::unique_ptr<Example> app = std::make_unique<Example>();
	app->run();
}
```

As you can see, the main file isn't too heavy. We first need to include the `merlin.h` library. Then an Application Object is declared by inheriting the `Merlin::Application` base class.

Application can be made of multiple rendering layers. Meaning it is possible to render overlays on top of each other. Which is important for the UI system.

Here we use a single explicit layer and a ImGui layer that is generated automatically.

In the `main` function, we can see that the application instance is stored in a unique pointer to ensure it is instanciated only after OpenGL has setup.

Then the `App->run()` method will be running until the application is closed.

### Layers

Merlin provide three ways to create layers. `Layer2D` and `Layer3D` are made to create 2D and 3D context respectively. While the `Layer` class provides a generic implementation in case that the user need to manage the Camera and the Camera controller manually.

The two first class provide a preconfigured camera object along with their controller to be able to move into the 2D/3D scene.

The Layer class also provide methods to retrieve the framerates and frame time easily using the `fps()` and `frameTime()` methods.

Layers are built around 5 execution steps.
First as we can see in the application definition, the `onAttach()` method is called once when the Layer is attached to the application. This method is used to propagated OpenGL dependent objects instanciations. This ensure that Meshes, Shaders... Are instantiated after the OpenGL context is ready. For this reason, every object have to be allocated dynamically using pointers. Merlin is implemented arround smart pointers from the standard library. These are accessible though the aliases in the `core.h` file

```c++
#pragma once

#include <merlin.h>
using namespace Merlin;

class ExampleLayer : public Layer3D
{
public:
	ExampleLayer();
	virtual ~ExampleLayer() {};

	virtual void onAttach() override;
	virtual void onUpdate(Timestep ts) override;
	virtual void onImGuiRender() override;
private:

	Scene scene;
	Renderer renderer;

	Shared<PointLight>  light;
};
```

In the above file, we can see that `ExampleLayer` inherits from `Layer3D` meaning the camera and camera controller are also implicitly created.

In the private section, we declare the Scene Object and the Renderer which aren't OpenGL dependent and can be instantiated directly.  

The PointLight object however is OpenGL dependent and can't be instantiated. Thus it need to be stored in a pointer using the `Shared<>` macro. 

The implementation of the class methods are where we actually create the scene and the behaviors of our applications.

In this example for instance, we create a simple scene with a 3D stanford bunny model. We also create a bunch of lights to demonstrate how to add multiple objects to the scene. However, the renderer has already defaults lights.

```c++
void ExampleLayer::onAttach(){
	Layer3D::onAttach();

	renderer.initialize();
	renderer.setEnvironmentGradientColor(0.903, 0.803, 0.703);
	renderer.showLights();

	Shared<Model> bunny = ModelLoader::loadModel("./assets/common/models/bunny.stl");
	bunny->meshes()[0]->smoothNormals();
	bunny->setMaterial("pearl");
	bunny->scale(0.2);
	bunny->translate(glm::vec3(0,0,-0.5));
	scene.add(bunny);

	light = createShared<PointLight>("light0");
	light->translate(glm::vec3(radius, radius, 3));
	light->setAttenuation(glm::vec3(0.6, 0.08, 0.008));
	light->setAmbient(0.09, 0.05, 0.05);
	light->setDiffuse(1, 1, 1);
	scene.add(light);

	Shared<DirectionalLight>  dirlight;

	dirlight = createShared<DirectionalLight>("light1", glm::vec3(-0.5f, 0.5f, -0.8f));
	dirlight->translate(dirlight->direction() * glm::vec3(-10));
	dirlight->setDiffuse(glm::vec3(0.2, 0, 0));
	scene.add(dirlight);

	dirlight = createShared<DirectionalLight>("light2", glm::vec3(0.5f, 0.5f, -0.8f));
	dirlight->translate(dirlight->direction() * glm::vec3(-10));
	dirlight->setDiffuse(glm::vec3(0.0, 0.2, 0));
	scene.add(dirlight);

	dirlight = createShared<DirectionalLight>("light3", glm::vec3(0.0f, -0.5f, -0.8f));
	dirlight->translate(dirlight->direction() * glm::vec3(-10));
	dirlight->setDiffuse(glm::vec3(0.0,0,0.2));
	scene.add(dirlight);

	Shared<AmbientLight> amLight = createShared<AmbientLight>("light4");
	amLight->setAmbient(glm::vec3(0.1));
	scene.add(amLight);

	scene.add(Primitives::createFloor(50, 0.5));

}
```

This example also demonstrates the use of utilities function such as `Primitives.create` methods used to create simple objects such as cubes, sphere, or in this case the scene floor.

The rendering is done in the `onUpdate` methods using the following syntax :

```c++
void ExampleLayer::onUpdate(Timestep ts){
	Layer3D::onUpdate(ts);
	renderer.clear();
	renderer.renderScene(scene, camera());
}
```

Note that other methods such as `onDetach` or `onEvent` could also be overridden to customize the application behavior.

Finally, we can create the Application UI by implementing the `onImGuiRender` method:

```c++
void ExampleLayer::onImGuiRender()
{
	ImGui::Begin("Camera");
	static glm::vec3 model_matrix_translation = camera().getPosition();
	if (ImGui::DragFloat3("Camera position", &model_matrix_translation.x, -100.0f, 100.0f)) {
		camera().setPosition(model_matrix_translation);

	}
	ImGui::End();

	// Define a recursive lambda function to traverse the scene graph
	std::function<void(const std::list<Shared<RenderableObject>>&)> traverseNodes = [&](const std::list<Shared<RenderableObject>>& nodes){
		for (auto& node : nodes){
			bool node_open = ImGui::TreeNode(node->name().c_str());
			if (node_open){
				
				if (node != nullptr){
					ImGui::Text(node->name().c_str());
				}

				traverseNodes(node->children());
				ImGui::TreePop();
			}
		}
	};

	// draw the scene graph starting from the root node
	ImGui::Begin("Scene Graph");
	traverseNodes(scene.nodes());
	ImGui::End();
}
```

### Result

As a result we get the following application ! 

![](./images/merlin.app.jpg)

This does not look wonderful, but, this is actually a short and simple code for a such application.

