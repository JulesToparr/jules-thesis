library(tidyverse)
library(kableExtra)
library(ggplot2)
library(ggrepel)
library(patchwork)
library(sf)
#library(rgdal)
library(leaflet)
library(ade4)
library(factoextra)
library(lme4)
library(MuMIn)

haie <- read.table("https://figshare.com/ndownloader/files/18058913", header = TRUE, sep = "\t")

print(dim(haie))

print(class(haie))

haie %>%
  as_tibble() -> haie

print(class(haie))

haie_coord_l93 <- tibble(id = haie$id_haie, 
                         lon = haie$l93_x, 
                         lat = haie$l93_y,
                         ann = haie$annee_med)

haie_coord_l93 %>%
  st_as_sf(coords = c("lon", "lat")) %>%
  st_set_crs(2154) %>%
  st_transform(4326) -> haie_coord_lonlat

leaflet(width = "80%") %>%
  addProviderTiles("Esri.WorldImagery") %>%
  addMarkers(data = haie_coord_lonlat,
             popup = ~ as.character(ann)) -> carte_haies

# Le provider Esri.WorldImagery provient d'ici : 
# https://leaflet-extras.github.io/leaflet-providers/preview/


#print(carte_haies)

haie %>%
  select(c(annee_med,
           h_moy,
           longueur,
           largeur,
           dist_for,
           surf_for_500,
           surf_for_1000,
           ipa_adj)) -> haie_acp


# Cette commande permet de labeliser une ligne. Ce label n'est pas inclut dans l'analyse. C'est comme un commentaire.
#rownames(haie_acp) <- haie$id_haie
haie_acp <- as.data.frame(haie_acp)
rownames(haie_acp) <- haie$id_haie

haie_acp %>%
  dudi.pca(scannf = FALSE, nf = 2) -> res_acp

plot_vp <- fviz_eig(res_acp, addlabels = TRUE)
plot(plot_vp)
#l93x standard de format de carte (format des coord géographiques)

#wgs84 standard longitude lattidtude.

#Arcgis etc s'appuie sur ces format.


mon_theme <- theme(panel.grid.major = element_blank(),
                   panel.grid.minor = element_blank(),
                   panel.background = element_rect(fill = "white"),
                   panel.border = element_rect(colour = "black", 
                                               fill = NA,
                                               size = 2),
                   plot.title = element_text(size = 18, 
                                             face = "bold", 
                                             hjust = 0.5),
                   plot.caption = element_text(size = 10,
                                               face = "italic", 
                                               color = "grey"),
                   legend.title = element_text(size = 12,
                                               face = "bold"),
                   legend.text = element_text(size = 12),
                   axis.title = element_text(size = 14, 
                                             face = "bold"),
                   axis.text = element_text(size = 12))

plot_vp <- fviz_eig(res_acp, 
                    addlabels = TRUE,
                    xlab = "Axes de l'ACP",
                    ylab = "Pourcentage de variance expliquée (%)",
                    title = "Diagramme en éboulis des valeurs propres",
                    caption = "Source : Lenoir et al. (2021)")

plot(plot_vp + mon_theme)

print(res_acp)

#On remarque que les deux premier axes explique déjà 50% de l'information 



# Tableau dijoncté des valeurs 

# Tableau dijoncté en créer autant de colone qu'il y a de choix;
# ex au lieu d'avoir une colonne avec la couleur des yeux par exemple. 
# Sinon il faut créer autant de colone que de choix et mettre un oui/non dans chacune.

# L4acp diagonalise notre matrice pour trouver les vecteur propres de facon e exprimer nos données dans une autre transformation.



# Analyse factorielle des conrespondance


# en explorant la donnée de cette façon  on perd de l'information mais on met aussi en valeur certain aspect des donées.
# Si une ACP permet de reprsenter les valeurs en conservant 70 de l'information c'est quadn même interressant.


print(get_eigenvalue(res_acp))

ind <- get_pca_ind(res_acp)

print(head(ind$contrib))

plot_ind <- fviz_pca_ind(res_acp,
                         addlabels = TRUE,
                         pointsize = 3,
                         col.ind = ind$contrib[, "Dim.2"], 
                         alpha.ind = 0.5,
                         legend.title = "Contrib.",
                         gradient.cols = c("blue", "pink", "red"),
                         repel = TRUE,
                         xlab = "Axe 1",
                         ylab = "Axe 2",
                         title = "Contributions des individus à l'axe 1",
                         caption = "Source : Lenoir et al. (2021)")

plot(plot_ind + mon_theme)

#Les poids des contrib peut être vulgariser en disant que l'axe principal est une balance.

# les individus les plus éloingés du centre on le plus de contributin au composante principale.
#Parfois il convient de supprimer les noeuds aux etreme de facon à y voir plus clair.

var <- get_pca_var(res_acp)
row.names(res_acp$li) <- haie$id_haie
#print(head(res_acp))

plot_var <- fviz_pca_var(res_acp,
                         pointsize = 3,
                         col.var = var$contrib[, "Dim.2"], 
                         alpha.var = 0.5,
                         legend.title = "Contrib.",
                         gradient.cols = c("blue", "orange", "red"),
                         repel = TRUE,
                         xlab = "Axe 1",
                         ylab = "Axe 2",
                         title = "Contributions des variable à l'axe 1",
                         caption = "Source : Lenoir et al. (2021)")

plot(plot_var + mon_theme)

print("-----------------")
print(head(res_acp))

# On remarque que l'axe 2 est ttrès corrélée avec la distance des forêt.

connect <- as.factor(haie$connection)

levels(connect) <- c("non", "oui")

plot_group <- fviz_pca_biplot(res_acp,
                              pointsize = 3,
                              alpha.ind = 0.5,
                              col.ind = connect, 
                              palette = c("darkorange", "darkolivegreen"),
                              addEllipses = TRUE,
                              ellipse.type = "convex",
                              col.var = "deepskyblue",
                              legend.title = "Haies\nconnectées",
                              repel = TRUE,
                              xlab = "Axe 1",
                              ylab = "Axe 2",
                              title = "Connectivité des haies à un bois",
                              caption = "Source : Lenoir et al. (2021)")
plot(plot_group + mon_theme)



ancien <- 2021 - haie$annee_med

plot_group <- fviz_pca_biplot(res_acp,
                              pointsize = ancien,
                              gradient.cols = c("blue", "orange", "red"),
                              alpha.ind = 0.5,
                              col.ind = ancien, 
                              legend.title = "Ancienté (années)",
                              repel = TRUE,
                              xlab = "Axe 1",
                              ylab = "Axe 2",
                              title = "Ancienté des haies",
                              caption = "Source : Lenoir et al. (2021)")
plot(plot_group + mon_theme)

# On peut modifier la légenge et la taille des points en utilisan scale_size. Voir corrigé. Sur le site de M LEnoir.