# NeoTech AMT - 15XBT Printer Documentation

## Introduction

The NeoTech AMT - 15XBT is a sophisticated multimaterial 3D printer combining various printing technologies with 5-axis motion control. It is designed for creating complex 3D Printed Electronics (3D PE), making it ideal for R&D, prototyping, and low-volume manufacturing of advanced electronic structures. The modular configuration and high-performance control system allow the integration of additional functionalities, paving the way for High Volume Manufacturing (HVM).

## Training Overview

We received training on the 15XBT multimaterial 3D printer following the schedule below:

| Time       | Monday       | Tuesday             | Wednesday               | Thursday              | Friday                |
|------------|--------------|---------------------|-------------------------|-----------------------|-----------------------|
| 09:00-10:30| Basic Machine | PiezoJet | Tool Calibration Unit | Extraction Unit, UV Curing | Heating Table, Fused Filament Fabrication |
| 10:30-12:00| Basic Machine | PiezoJet | Dispenser 1K | Milling | Fused Granulate Fabrication |
| 13:00-14:30| Substrate Camera, Confocal Sensor | Pneumatic Spray | IR Curing, IR Thermal Camera |   |   |
| 14:30-16:00| Pick & Place, Component Camera |   | Pneumatic Spray |   |   |

## Key Features and Capabilities

- **3D Functional Printing**: Combines structural build, surface-mount device placement, and printed circuits.
- **Modular Design**: Expandable with upgrades for novel process chains.
- **Intuitive Software**: Motion 3D software offers path and machine motion simulation, along with customizable cycle times.
- **Application Areas**: Includes 3D Printed Electronics, Fully Additive Structural Electronics, and functional material deposition.

### Specifications

- **Motion Range**: 400 mm x 300 mm x 140 mm (X-Y-Z)
- **Print Speed**: Up to 100 mm/s
- **Axis Accuracy**: X, Y, Z repeatability: ±10 μm; A & B Axis deviation accuracy: ±1‘20‘‘
- **Machine Dimensions**: 769 mm x 834 mm x 1370 mm
- **Software**: Motion 3D supports 3+2 indexed printing through 5-axis simultaneous motion with ISO standard G-code support.
- **Utilities**: 240V/10A or 110V/10A electrical input, solvent facilities for print head cleaning.

## Getting Started

### Frame of Reference

The machine’s frame of reference is set at the rear bottom left corner of the printer. As the 15XBT can utilize multiple tools, it is critical to understand and manage reference frames correctly to avoid collisions. In CNC machining, frame transformation is used to shift the coordinate system from one tool to another.

**Frame of Reference Example**:

![](https://www.researchgate.net/publication/337936397/figure/fig1/AS:836238126891008@1576386114890/Machining-center-reference-system-schema_W640.jpg)

**Pro Tip**: When working manually, keep the speed low and observe movements carefully.

### Control System - B&R Control

The NeoTech 15XBT uses B&R Control software, an intuitive web-based interface. Below are the initial steps to operate the machine:

1. **Log In**:
   - Turn on the machine using the black rotary switch on the front panel.
   - Once the embedded PC is ready, click the Start button and access the B&R Control interface.
   - Log in as a Service User with the password `4711BT`.

2. **Moving the Carriage Manually**:
   - Ensure the door is closed and press the refresh button.
   - Activate the motors by pressing the power button on the screen or control panel.
   - Enter JOG Mode by clicking the joystick icon.
   - Set a low speed and carefully move the X, Y, and Z axes using the arrow buttons.

3. **MDI Mode**:
   - Use MDI (Manual Data Input) to write machine code, such as M-commands for specific machine operations.

### Machine Tools and Calibration

#### Tool Overview

- **PiezoJet**: Utilizes a single nozzle for precise material deposition.
- **FFF (Filament and Pellet Feed)**: Allows for printing structural parts with filament or granulate.
- **SMD Pick & Place**: Mounts components onto printed circuits.
- **Pneumatic and Ultrasonic Spray**: Ideal for high-precision functional material deposition.

#### Calibration and Setup

1. **Pick & Place Tool Calibration**:
   - Mount the tool and align it to the reference pin.
   - Manually jog the machine to focus the camera on the tool’s tip, then start the calibration in Motion3D.

2. **Confocal Sensor Calibration**:
   - Power up the confocal sensor and allow it to warm up for 30 minutes.
   - Perform a dark reference adjustment and use the sensor for adaptive height correction.

3. **Inkjet Printhead Calibration**:
   - Assemble the printhead and attach an ink cartridge. Always turn off the pressure before handling.
   - Adjust the pressure and set the ink flow cycle parameters based on ink viscosity and speed.
   - Test print an X pattern to manually fine-tune the offset.
   - Regularly check the printhead for proper alignment and cleanliness.

### Maintenance and Cleaning

#### Inkjet Printhead Cleaning

- Always set the inkjet nozzle pressure to zero before performing any maintenance.
- Use the purge function to remove air and initialize the nozzle.
- Regularly inspect and clean the piezo mechanism using approved solvents, following the manufacturer's guidelines.
- Ensure the cleaning process does not leave aluminum arms in acetone for too long.

**Pro Tip**: When using UV curing, a Z-offset of 10 mm is recommended.

### Collision Prevention and Safety

- The NeoTech 15XBT does not include automatic collision detection, so manual care is essential.
- Always monitor the toolpath visually during initial runs.
- Keep the feedrate low when experimenting with new tools or calibrations.
- If a collision occurs, stop the machine immediately and verify the tool’s frame of reference.

### Advanced Usage

1. **Using the Confocal Camera**:
   - Automatically document components using the built-in camera. The camera can record dimensional data and detect defects in substrates.
   - Create adaptive camera paths by selecting points and letting the machine program the path.

2. **5-Axis Movement Considerations**:
   - The machine supports combined movement of primary XYZ axes along with rotations of A and B axes.
   - Ensure that your designs account for high-speed requirements to maintain accuracy during 5-axis movements.
   - Utilize the built-in simulator to visualize 5D movements and avoid unexpected issues during the printing process.

## Frequently Asked Questions (FAQ)

1. **Q**: What should be done if a collision occurs?
   - **A**: Immediately stop the machine, reduce speed, and manually verify the tool’s frame of reference.

2. **Q**: How can the inkjet parameters be adjusted?
   - **A**: Parameters can be customized in the inkjet control interface within the rack or embedded within the G-code.

3. **Q**: Is there a rule linking ink viscosity to inkjet timing?
   - **A**: NeoTech provides recommended starting parameters for specific viscosities. Consult the manufacturer’s documentation.

4. **Q**: Should calibration be done in specific environmental settings?
   - **A**: Yes, temperature and humidity can affect calibration, so it’s best to maintain consistent environmental conditions.

5. **Q**: Can thermal expansion affect calibration accuracy?
   - **A**: Yes, thermal expansion can cause minor shifts in the calibration, especially after prolonged operation. Regular checks are recommended.

## Contact Information

For additional technical support or detailed process inquiries, please reach out to:

**Email**: support@neotech-amt.com  
**Phone**: +49-89-12345678

---

## Documentation

Here you'll find several documentation about machines and tools I used during the PhD.

## NeoTech AMT - 15XBT Printer

We just had a training on the 15XBT multimaterial 3D printer from NeoTech.

The training was organized in the following section : 

|       | Monday       | Tuesday             | Wednesday               | Thursday              | Friday                |
|-------|--------------|---------------------|-------------------------|-----------------------|-----------------------|
| 09:00-10:30 | Basic Machine | PiezoJet | Tool Calibration Unit | Extraction Unit, UV Curing | Heating Table, Fused Filament Fabrication |
| 10:30-12:00 | Basic Machine | PiezoJet | Dispenser 1K | Milling | Fused Granulate Fabrication |
| 13:00-14:30 | Substrate Camera, Cofocal Sensor | Pneumatic Spray | IR Curing, IR Thermal Camera |   |   |
| 14:30-16:00 | Pick & Place, Component Camera |   | Pneumatic Spray |   |   |


### Basic machines informations

![](./images/15xbt.jpg){align="left"}

The Neotech 15X BT is an updated model of the PJ15X. It is an economic Rapid Prototyping system
for a wide range of 3D Printed Electronics. This unique
system combines multiple printing technologies with
5-axis motion control enabling complex 3D printing.
It is ideally suited for a wide range of R&D, Prototyping
and Product Development operations. The system has
a modular configuration and uses a standardised high
performance control system offering a clear path to
future High Volume Manufacturing. The base system is
scalable with optional upgrades that can be added
at the time of order or in the customer facility that allow the creation of novel process chains : 


| Print/Functionalising Tools  | Pre-/Post-Processing        |
|-----------------------------|-----------------------------|
| Piezo Jetting               | CNC Machining               |
| Ink Jetting (Single Nozzle and Classic multi-nozzle*) | Plasma Cleaning |
| Aerosol Based Light Beam Sintering | Dispensing UV Curing     |
| Ultrasonic Spray            | Advanced Machine Vision     |
| FFF (Filament and Pellet feedstock)||
| SMD Pick & Place ||

With the extensive range of print and post-processing
tools the 15X BT supports a wide variety of functional
materials. Conductive nanoparticle inks and micron
scale pastes can be accurately deposited onto complex shaped non-planar substrates and combined with
SMD Pick & Place for manufacturing mechatronic
systems. Structural bodies can also be generated
via the FFF and dispensing modules enabling “Fully
Additive” 3D electronics.

Features :

- Full 3D Functional Printing capability
- 3D PE Combining Structural Build,
 Surface Mount Devices & printed circuits
- Intuitive and easy to use Software
- Cost effective Prototyping to Low Volume Manufacturing

Applications

- 3D Printed Electronics
- “Fully Additive” Structural Electronics
Printable Materials (Base Configuration)
- Nanoparticle solutions, microparticle inks (to ca. 50 μm)
- Dielectrics
- Resistive inks
- Ink viscosity range: 50 to 200,000 mPas

Alternative print methods extend range of materials.
Contact Neotech for details.

Motion Module

- Print Speed: 100 mm/s max.
- Motion Range: 400-300-140 mm (X-Y-Z)
- X, Y and Z – Axis repeatability: +/-10 μm
- A & B Axis position accuracy – Angle deviation 0° 1‘ 20‘‘
- A & B Axis repeatability – Angle deviation 0° 0‘ 6‘‘
- Stand alone system dimensions 769 mm x 834 mm x 1370 mm
 (X-Y-Z) – control case & monitor extra
- Stand alone system weight ca. 350 kg

CAD/CAM Software Motion 3D

- 3+2 indexed printing through 5-axis simultaneous
- Optimised cycle times via free definition of the print sequence
- Printing, Pick & Place, Pre- and Post processing, path & machine motion
 simulation (collision detection)
- Machine specific ISO Standard G-Code post processor
- Look ahead function giving clean end to printed feature
- CAM Check Function – check programmed tool-path vs. hardware limits

Utilities Required (Base System)

- Electrical: 240V/10A or 110V/10A
- Print Head Cleaning Facilities/Solvents
- Final utilities depend on end configuration


### Getting started

The __15 BT__ is very similar to usual CNC machine. You'd better to have a first experience on CNC machines to get started. However, you'll find the fundamentals informations required to operate the machine in the following section.

One of the most important things to understand is "frame of reference". Has the machine could have multiple tools mounted on the same carriage, each of the tools are offset from the machine frame of reference. Thus, we might change the frame of reference quite often to switch between the tools. 

In CNC machining, a "frame of reference" is a critical concept that defines a fixed and consistent coordinate system within which the machine operates. It serves as a point of origin and orientation for all machining operations. The frame of reference typically relates to the machine's primary structure or base, which remains stationary during machining processes.

As mentioned, CNC machines often have multiple tools mounted on the same carriage or tool changer. Each of these tools has its own unique offset or position relative to the machine's frame of reference. These offsets are crucial for accurately controlling the tool's position and orientation when executing machining operations.

Frame transformation in CNC machining refers to the process of changing or shifting the reference coordinate system from one tool to another. This transformation is necessary because, as tools are swapped or changed during machining, the machine needs to understand and work with the new tool's specific offset or position relative to the machine's frame of reference.

For example, if a CNC machine is initially set up to use Tool A, the machine's controller understands the tool's position and orientation in relation to the frame of reference. When switching to Tool B, which may be mounted slightly differently or have a different shape, a frame transformation is performed to ensure that the machine now operates within the reference frame of Tool B. This transformation involves adjusting the machine's coordinates, so it accurately interprets and executes machining commands based on Tool B's offset and position.

In summary, a frame of reference in CNC machining establishes a consistent coordinate system for machining operations, while frame transformation allows the machine to adapt to the specific offset and position of different tools. This ensures precision and accuracy when using multiple tools on the same CNC machine.

Frame of reference example : 

![](https://www.researchgate.net/publication/337936397/figure/fig1/AS:836238126891008@1576386114890/Machining-center-reference-system-schema_W640.jpg)

In the 15BT machine, the machine reference point is located at the __rear bottom left__ of the machine.

!!! warning
    Make sure to understand this concept to avoid any collisions. Use low speed when operating the machine could be safer if you're not very used to this :)

Controlling the machine mannulaly is really straightforward. It comes with a custom control sofware embedded in the pc called B&R Control.
B&R Control is basically a Webpage running in fullscreen mode where you can find all the tools and settings to operate the machine.

Let's setup the machine to move the carriage.

#### Log in

1. To start, first turn on the machine using the black rotating swith on the very right of the front panel.
![](./images/15xbt-panel-power.jpg)

2. Once the machine is turned on, wait for the PC to start. Once windows is ready. Click the start button


IMG start button


1. After a while, you should see the Home menu of the B&R control sofware. Now you can log in using the :material-account: icon at the top right of the screen.
2. Select Service user and use this password to login : 4711BT
3. Then click the login button under the textbox and go back to the B&R Home screen
4. You could create new users but that's not really necessary for now.

You're now logged as a service user (Expert mode !)

#### Moving the carriage mannually

1. To move the machine, make sure the door is closed and press :material-refresh: button on the top left of the screen.
2. Then press the :material-power: button either on the control panel of on the machine front panel.
3. If the :material-power: button turn green, and you here the motors engaging then the machine is ready to move. If not, check the error message at the top right of the screen or try to :material-reset: and :material-power: once again.
4. Then press the IMG JOYSTICK button to enter JOG Mode.
5. Lower the speed using the :material-minus: button at the bottom, next the the :material-sort-reverse-variant: bar
6. Try to move the X axis carefully

!!! info 
    If you go back to the home screen :material-home:. You should see a bunch of coordinates. X, Y, Z... 
    Note that the big numbers are indicating the current position in the current selected frame of reference. The smaller numbers indicate the current position in machine frame of reference.



### Raw notes : 

Cam block : one op in the process. FDM + a picture= two blocks

Each CAM block has a tool ID that change the frame of reference

Can change the calibration settings manually using the table third icon on the right bar from below.
However this can be done automatically using the laser calibration tool

We can use an already milled piece.
Thermal expansion can cause calibration to change.

Q/A : should we calibrate in specific environment settings ?

Several calibration routine :
We can calibrate using the calibration sensor or the camera ?

For the piezojet (quite big)
Nozzle diameter is very small. The nozzle itself cannot be offset. So if you print with it it may have some deviation

So we print an X to calibrate manually. Indirect calibration.
Calibrate when we change the tool.

5 axis !
Movement unpredictable
Some processes have fixed flowrate so the machine may need very fast velocity to acheived 5D small movement.
Sometimes the machine cannot acheive this velocities.
Think while designing.

There is a simulator. In 5D 

Primary axis XYZ + rotation of the tools to have combined movement.




---- Soft presentation -------
Jog : manual movement. 
If you want to move while it's open press the Ack button while moving.

Auxiliary axis : E for extrusion.

Use the machine alone. 
MDI to write machine code. M command for instance.

Q/A : any overload detection in the axis in case of collisions? -> No collision detection.

Tools menu :  Heating Tuning.
Pick and place...
To calibrate the first SMD component : 
Jog manually or use the camera
Two USB slots in the front. To open the USB folder click on the hard drive icon in the File menu.
We can edit or open in an editor from Motion3D

On the IO menu. You can see active services
And machine code as well. So you can use Mxx to enable or disable services according the the table in IO menu..

Stop software limit.
If collision Happen turn down the speed. And the Feedrate to override the speed. Recommend anyways. 



----- Camera :
Automatic documentation using the camera. You can define automatic camera path. 
Historical data of the parts. 
Useful if wires are later covered by the FDM process for instance.

The camera is used for pick and place and it is able to do dimensional measure. 

Adaptative toolpath of the camera. You can click on two point and will create a program for you.
If you detect a defect you ça fill the defect then.
Fiducial recognition.
To do calibration and place
Also useful to detect substrate. Microscope panel for instance.

The update could lower the load time of neotech control.

To calibrate the doc camera. Remove the cap. Open neotech control and then move to the center of the pin on the calibration tool. Then using Tools calibration in Motion3D start Zeropoint calibration. Warning machine may move.
Then activate the Zeropoint in the Zeropoint menu on the left panel 

Start confoncal Sensor from the rack before starting neotech control.

Sensor should warm up 30mi'utes !.

To calibrate the confocal camera
Settings >  sensor > dark reference 
Press start

Range : 10mm
+-5mm range

It used for surface height meseaure. 
Even to do height correction.
Adaptative mesh correction using the confocal sensor.

Used for bed leveling.

Calibration :
Move to the pin again using the confocal sensor

Pick and place tool.

Mount the tool. Wire it.
Mount a pipette

Then calibrate. 
To calibrate the pipette. Move the tip of the pipette in the center of the calibration.
It should cross one beam

Calibrate the component camera. Note : the tip of the pick n place tool should be around 50mm away from the camera..
Try to focus with jog and then use the calibration tool in Motion


323.59

Be wäre of obstacles and mind about the Zeropoint before picking. Switching between camera and pick n place may not be automatisch


-----Inkjet printhead-----

Mount a nozzle and the adapter to be able to mount a ink tank. 

Always turn off the pressure before opening the inkjet mechanism.

Then fill a cartridge. 
Turn on the piezojet in the rack.
Then test the pressure. 
Set the pressure back to zero before plugin the cartridge tube.

Use paper tape as adhesive layer for the bed

The screen in the rack related to the inkjet tool can be use to adjust a bunch of settings.
See cycle curve for reference. This cycle curve contain every timing that can be customized. Those settings could be use to tune cycle timing depending on the matérial and deposition speed.
We can preview the cycle. 
Every ink has its own 

Q/A do we have a rule linking viscosity to inkjet timing ?

They could Provide a list of starting  Parameters.

Q/A max temperature of the inkjet nozzle ?

Purge button will raise the piston and let the ink flow. Make sure you use a piece of paper. It is used to initialize the nozzle to remove air that inside.
To add a new material click the pen next to the Ink Droplist. This will open a notepad where you can add a name into the file. Save and close and this will update the drop-down menu.

On the Neotech AMT website there is documentation with predefined curves.
When changing the curve setting you can see the curve updated in the control box in the rack

We can set the parameters inside the gcode. Or we could set them 

For the cleaning they have a pdf.


After testing pulse 




138.184
140.182
-8.931
-5.931


8.9

Current : 96.257

Cleaning : attention de ne pas laisser les palonnier alu orange XY dans l'acétone 




Use 10mm z offset for the uv curing



Q/A should we correct the center of rotation of the two additional axis. Since they are defined in the gcode using a shift op

Contact : support@neotech-amt.com












### Inkjet printhead calibration

- Printhead assembly process (See paper documentation)
- Purge, cycle ... See phone notes


#### Inkjet cleaning

- See paper instructions

#### 