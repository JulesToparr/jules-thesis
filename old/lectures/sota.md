---
hide:
  - navigation
---

# :material-bookmark: Thesis
<font size="5">State of the art</font><br> 
<font size="4">Multi-Material Additive Manufacturing of Intelligent Components: Closed loop control through Real-Time Simulation</font><br> 
<font size="3"> __Jules Topart__</a></font><br>
<font size="2"> Laboratoire des Technologies Innovantes, UR UPJV 3899, Avenue des Facultés, Le Bailly,
80025 Amiens, France<br>
<font size="2"> UniLaSalle, SYMADE, 80082 Amiens, France<br>

<div markdown="1" style="column-count: 2; column-gap: 30px;">

<font style="font-variant:caps; font-weight: bold; margin: 0 !important;">Key words:</font> *3D Printing, Computationnal physics, Multi-material additive manufacturing, Real-time Simulation, Machine learning*<br>

!!! warning 
    For some reason some web browser are not displaying Latex correctly when loading the document. 
    (Test equation :  \( \sum{x_i} \)) 
    If the test equation is not be displayed properly please __refresh the page__.

### 1. Introduction

<!---
    - Overview: A brief introduction of the domain you're focusing on and why it's important.
    - Aim of the review: Describe the purpose of the state-of-the-art review in the context of your thesis.
--->

The rapid advancement of Additive Manufacturing (AM), more commonly known as 3D printing, is significantly altering various industries, ranging from aerospace to biomedical, automotive to consumer goods. This technique, which builds objects layer-by-layer, has opened up unprecedented opportunities for producing complex geometries that are challenging, if not impossible, to realize with conventional manufacturing methods. AM provides substantial benefits such as customization, design flexibility, speed, and cost-effectiveness, particularly for low-volume production, prototyping, and more importantly for custom production (or batch production)[[Bacciaglia et al.]][1] [[Jordan, J. M.]][9].

Building on the potential of digital fabrication, the emergence of Multimaterial Additive Manufacturing (MMAM) stands as a remarkable progression in the field. MMAM combines the inherent advantages of AM, but it broadens the spectrum by allowing the creation of parts comprised of multiple materials in a single build process. By harnessing diverse mechanical, thermal, and electrical properties [[Tibbits, S.]][22], MMAM enables the production of functionally graded materials, personalized products, and intricate multi-material components. This extends the capabilities of AM and opens avenues for groundbreaking applications [[Nazir, A.]][16] such has complex 3D batteries [[Maurel, A.]][14] or soft-robotics.

With the digital technologies developing, many industries started a transformation toward a new production paradigm (eg. called Industry 4.0 in Europe). This transformation is a significant driving force behind the AM advancements. The integration of digital technologies into manufacturing processes, often referred to as digital fabrication, has enabled more efficient, flexible, and customized production. AM and MMAM are key components of this digital transformation, leveraging digital design and fabrication tools to create products that were previously unimaginable. Furthermore, the use of digital technologies facilitates the collection and analysis of data, which can be used to optimize manufacturing processes, improve product quality, and drive innovation. [[Prashar, G.]][19].

In the broader context of manufacturing, the work of Professor Neil Gershenfeld on distributed digital fabrication lays a crucial foundation. Gershenfeld propounds the future where every household owns fabrication machines capable of creating an array of products, an evolution of the concept of 'personal fabrication'. He envisions a world where production is democratized, and the boundaries of centralized production and transportation are eroded due to global challenges.
The value of distributed digital fabrication became particularly evident during the global pandemic, where traditional supply chains were disrupted. In response, local digital fabrication initiatives played a critical role in building resilience, enabling the swift production of essential supplies ranging from personal protective equipment to parts for medical devices [[Cutcher-Gershenfeld, J.]][4].

The following sections of this review deal with the evolution and current state-of-the-art in AM and MMAM. We'll explore the present challenges that the industry confronts, and how existing research endeavors to surmount these obstacles. The aim is to set the stage for our research, which strives to enhance MMAM through the application of real-time simulation based closed loop control.

### 2. Methodology for Literature Review

<!---
    - Search strategy: Explain how you identified and selected your sources (e.g., databases, keywords, time period, language, etc.).
    - Criteria for inclusion and exclusion: Define what type of sources you included and why.
--->

The literature review has been conducted systematically, with the aim of gathering and synthesizing the most relevant research in the areas of Additive Manufacturing (AM), Multimaterial Additive Manufacturing (MMAM), and real-time simulation techniques. The objective is to paint a comprehensive picture of the current state of knowledge in these domains, identify the gaps in existing research, and lay the groundwork for our proposed study.

We leveraged multiple scholarly databases to conduct our literature search, including Research Rabbit, SpringerDOI, ScienceDirect, Google Scholar, and various open-access databases such as MDPI and DOAJ (Directory of Open Access Journals). These databases were chosen for their extensive coverage of technical and engineering-related research, ensuring a wide range of sources for a comprehensive review. Open-access sources were particularly favored in line with our commitment to the principles of open science.

To ensure the relevance of our search, we used keywords related to our research domains. These keywords included "Additive Manufacturing," "3D Printing," "Multimaterial Additive Manufacturing," "Real-Time Simulation," "MMAM Challenges," and combinations of these. We prioritized recent publications to capture the most current advancements in the field, focusing on papers published within the last decade.

Regarding the inclusion criteria, we targeted sources that offered substantial insight into the technical aspects, applications, challenges, and current solutions in AM, MMAM, and real-time simulation. We particularly emphasized research articles, review papers, technical reports, and open-source resources. Sources were excluded if they were not in English, if they were published over ten years ago (unless they were seminal works), or if they focused on areas not directly applicable to our research domains.

Moreover, in line with open science practices, we prioritized studies that provided open data, open-source software, or clear and reproducible methodologies, promoting transparency, accessibility, and further collaboration in scientific inquiry.

Our approach ensured a methodical and thorough review process, leading to an in-depth understanding of the state of the art in MMAM and real-time simulation techniques. It reflects our commitment to open science and the belief that our research should contribute to and benefit from this collaborative and transparent approach to scientific discovery.

### 3. Core Concepts and Definitions

<!---
    - Definitions: Define terms related to AM, MMAM, real-time simulation, etc.
    - Principles of AM and MMAM: Explain the principles behind these processes and how they work.
--->

Before diving into the detailed analysis of the literature collected, it is crucial to define and clarify the core concepts related to Additive Manufacturing (AM), Multimaterial Additive Manufacturing (MMAM), and Real-Time Simulation. Understanding these terms is fundamental for readers unfamiliar with the subject matter and to ensure clarity in the discussions that follow.

#### Digital Fabrication: 
This term refers to the processes used to produce physical items from digital models. Where computer assisted machining refers to the action of creating a sequence of machining operation using a computer, digital fabrication. The concept is integral to the notion of distributed digital fabrication, where production capabilities are widely distributed and easily accessible [[Anderson, C.]][0] [[Soomro, S. A.]][21].

#### Additive Manufacturing (AM):
Also known as 3D printing, AM is a process that creates three-dimensional objects from a digital model (CAD). Unlike traditional manufacturing methods that often remove material (subtractive manufacturing), AM works by sequentially adding layers of material, adhering to the geometry defined in the digital model. This process enables the production of complex shapes with high precision.[[Jordan, J. M.]][9].

#### Multimaterial Additive Manufacturing (MMAM):
An extension of AM, MMAM is the capability to create objects composed of more than one material in a single build process. This approach allows for parts with varying mechanical, thermal, and electrical properties within the same component, leading to functionally graded materials and advanced, multi-functional components[[Tibbits, S.]][22]. Using different color of a same material during the manufacturing could indeed be considered as multi-material 3D printing since it usually require changing the feeding materials during the manufacturing process. However, there is a great difference between color graded compound and interfacing physically different materials. We will refer to this as multi-color AM and multi-material AM.

#### Real-Time Simulation:
Real-time simulation refers to the process where a system model's simulation runs at the same speed as the actual system. In the context of AM and MMAM, real-time simulation can help predict the manufacturing process's behavior and outcomes, enabling real-time adjustments and control to improve the product quality [[Khelafa et al.]][5]. This kind of simulation usually rely on simplified models and parallelization using Graphics processing units (GPU).

#### Distributed Digital Fabrication:
A concept that refers to a future manufacturing paradigm where fabrication devices are ubiquitous and accessible to everyone. This concept envisions individuals creating their custom products, significantly decentralizing and democratizing the manufacturing process

#### Artificial Intelligence (AI) :
The term AI has many definitions depending on the context. The most relevant definition in our context is to consider AI as a branch of computer science that aims to create systems capable of performing tasks that would normally require human intelligence. These tasks include learning from experience, understanding natural language, recognizing patterns, solving problems, making decisions, and exhibiting perception (such as vision or speech recognition).

#### Machine Learning (ML) :
ML is a subfield of AI. It involves the development of algorithms that allow computers to learn from and make decisions or predictions based on data. Reinforcement Learning, is a type of machine learning where an agent learns how to behave in an environment by performing actions and observing the results or rewards.

In the following sections, these concepts will be referred to extensively as we explore the evolution, current state, and future prospects of AM, MMAM, and real-time simulation."

### 4.  Historical Development of the Field

<!---
    - Present a historical overview of significant developments in the field.
    - This will provide context for understanding the current state of the art.
--->

The journey towards the modern era of Additive Manufacturing (AM) and Multimaterial Additive Manufacturing (MMAM) has been a progressive evolution, shaped by innovations in material science, digital technology, and manufacturing processes.

The concept of AM dates back to 1981 when Hideo Kodama of Nagoya Municipal Industrial Research Institute published the first account of a functional rapid-prototyping system using photopolymers. The technology advanced rapidly, and in 1986, Charles Hull was granted a patent for Stereolithography (SLA), a process that solidifies thin layers of ultraviolet light-sensitive liquid polymer using a laser. This invention marked the official birth of AM. 

In the following decades, AM technologies proliferated and diversified, leading to various techniques such as Selective Laser Sintering (SLS), Fused Deposition Modeling (FDM), and others. These techniques expanded the range of materials used in AM processes, from thermoplastics and metals to ceramics and composites, enabling a wider scope of applications. The rise of open-source movements in the late 2000s, like the RepRap project, accelerated the adoption of AM, making it accessible for personal use and research. These new manufacturing techniques enabled new design possibilities. Thus,  additive manufacturing is highly related to innovation in various fields that developped simultaneously.

The advent of MMAM is a more recent development, sparked by the increasing demand for advanced functional components and complex designs. The initial exploration of MMAM started with the blend of similar materials or simple composites, evolving to more complex systems where components with varying properties could be manufactured in a single print.
The ability to combine different materials in one print allowed for the design of products with graded or varying properties within a single structure. This opened up the possibility for new applications in diverse fields, such as biomedical devices, aerospace, and electronics.

Parallel to the growth of AM and MMAM, the concept of digital fabrication evolved, referring to the processes that translate digital models into physical objects. The evolution of this concept eventually led to the idea of distributed digital fabrication. Promoted by scholars like Pr N. Gershenfeld, this concept envisions a future where digital fabrication tools are widespread, enabling local, personalized production and potentially revolutionizing the traditional centralized manufacturing paradigm. 
One most important marker of the AM evolution is probably the RepRap project, a cornerstone of the Maker Movement, that emerged with the goal of creating a self-replicating 3D printer, emphasizing open-source collaboration and democratizing manufacturing by making these powerful tools accessible to anyone.
The continuous development and reduction in cost of technologies such as 3D printers, computer numerical control (CNC) machines, and microcontrollers like Arduino and Raspberry Pi, have made these tools more accessible to the general public. More people can now afford to have a personal fabrication laboratory, or "fab lab", at home. In addition, new software has made these machines easier to use and to learn about, broadening their appeal. [[Anderson, C.]][0]
This popularization of the AM process tackle down the AM machines costs drastically. As a result, the Fused deposition modeling process is the most popular additive manufacturing process even through it's not the most popular in an industrial context.  

The COVID-19 pandemic underscored the potential benefits of such a distributed manufacturing system. When traditional supply chains faltered, locally available digital fabrication resources, including AM, were employed to meet urgent needs, from personal protective equipment to ventilator parts, demonstrating the resilience of a distributed system. [[Cutcher-Gershenfeld, J.]][4].

These historical developments have set the stage for the current state of AM and MMAM, where the focus has now shifted to address the existing challenges and harness the untapped potential of these technologies. This includes the incorporation of real-time simulation techniques, a topic of growing interest in the field of AM and the primary focus of this thesis.


### 5. Problem statement

One of the primary challenges of the MMAM process is ensuring consistent quality in the produced parts. Due to the complex nature of MMAM, which involves the use of multiple materials with diverse mechanical, thermal, and electrical properties, maintaining uniform quality across the entire component is a significant challenge. This issue is further complicated by the difficulty in tracking in-situ material-related data such as temperature, strain, and deformation during the manufacturing process.
The volume of production is another challenge. While AM is highly beneficial for low-volume production and prototyping, scaling up the process for high-volume production is a complex task. The process could indeed be difficult to accelerate since the layerwise nature of the process is both the core of the technology and it's greatest downside.
Manufacturing time is a critical issue. The layer-by-layer approach of AM, while enabling the creation of complex geometries, is inherently time-consuming. This makes the process less suitable for applications requiring rapid production.
However the machines could easyly be duplicated to raise the production volume. Even if it may require important investment, the development of print farms (Large amount of AM machine) raise in propularity.

The fail rate in AM is relatively high due to the complexity of the process and the multitude of variables involved. This leads to increased costs and reduced efficiency, making the process less viable for large-scale industrial applications.
Due to the long nature of the process, failure is not an option since keeping a machine busy even when it is producing a faulty part result in significant performance loss.
Furthermore, the AM process is largely based on a trial and error approach. This is due to the lack of comprehensive understanding and control over the process variables, leading to inefficiencies and inconsistencies in the produced parts.

To address these challenges, the application of real-time simulation and closed-loop control in MMAM will be evaluated in the following section. Real-time simulation can provide valuable insights into the behavior and outcomes of the manufacturing process, enabling proactive adjustments and control without complicated instrumentation. This can significantly reduce the trial and error nature of the process, leading to improved efficiency and consistency.

Closed-loop control, on the other hand, can provide continuous feedback during the manufacturing process, allowing for real-time adjustments and optimization. This can help in maintaining consistent quality, reducing the fail rate, and improving the overall efficiency of the process.

### 6. Current State of the Art

<!---
    - Current MMAM Technologies: Detail the most used or promising MMAM techniques, their applications and limitations.
    - Current Real-Time Simulation Methods: Discuss existing simulation techniques used for AM and MMAM.
--->

In this section, we explore the current state of the art in both Additive Manufacturing (AM) and Multimaterial Additive Manufacturing (MMAM), highlighting existing challenges and discussing how they are currently addressed.

Today, AM is widely recognized as a transformative technology. Many materials ranging from polymers, metals, ceramics to composites can be used in AM, offering a vast array of possibilities. However, several challenges persist. These include limitations in the dimensional accuracy and surface finish of printed parts, the high cost and limited availability of some print materials, and the need for post-processing steps. Moreover, controlling and predicting the properties of printed parts, given the influence of process parameters on final product attributes, remains a significant challenge. We can therefore conclude that the AM manufacturers had made significant progress by adressing speed, repeatability and materials. However, these progress concern mono-material machines. Reliable multi-material manufacturing is rare in terms of machines and use cases.

MMAM has introduced a new dimension of complexity and potential to AM. The ability to combine different materials in a single build process has opened up the possibility to create parts with graded properties, ranging from structural components with varying stiffness to electronic devices with embedded circuits. MMAM is poised to revolutionize various sectors, such as biomedical engineering (with personalized prosthetics and implants), electronics (with integrated circuits), and aerospace (with functionally graded components). AM Industry leader developped machines such as the Stratasys J55 that is able to print multimaterial parts using material deposition and photopolmimerization. The user could used patented resin that could have different color, texture, or even mechanical properties. However, this process is limited to UV curable resin. 
Other company such as Neotech of Nskrypt own patented machine that are more sophisticated and close to the MMAM fundamental definition. Those machines are able to print in different materials using multiple deposition methods depending on the material which elarge the material pallet drastically.
We can also observe the ability to prick and place electronics components in an ongoing printed part. This process could be refer to as inclusion.

However, MMAM faces several obstacles. Managing the interaction between different materials, especially those with contrasting properties (like metals and polymers), is complex. Furthermore, the simultaneous control of multiple material properties in a single print requires advanced process monitoring and control strategies. 
The introduction of real-time simulation approaches is one potential solution to address some of these challenges. By modeling the AM or MMAM process in real-time, it's possible to predict potential defects or deviations in the final product during the manufacturing process itself. This predictive capability allows for adjustments and corrections, improving the final product's quality and reducing waste. 
Despite the potential advantages, real-time simulation in AM and MMAM is still in its infancy. Significant challenges exist in terms of computational power and the complexity of mathematical models needed to accurately simulate the AM processes. Additionally, the simulation of multiple materials with differing properties in MMAM is notably challenging.

In this evolving landscape, open science has a crucial role to play. Sharing data, methodologies, and findings in an accessible manner will accelerate research and innovation in AM and MMAM. Initiatives like open-source 3D printing platforms and shared material property databases exemplify this approach, driving faster solutions to existing challenges. This is why the developpment of public tools to both understand and improve the MMAM process is critical. In other words, the main objective of this thesis is to provide a comprehensive framework for MMAM simulation and control.

More recently, on key technology is showing promising result in different domain include AM. AI has seen massive strides is Reinforcement Learning (RL). RL, shows considerable promise in this context. [Piovarci, M.][18] It has been instrumental in solving complex, dynamic problems where information is typically incomplete. Particularly in AM and MMAM, RL can enhance decision-making processes such as optimal print path planning, parameter tuning, or fault detection, by continually learning from the manufacturing environment. Thus, RL is poised to introduce unprecedented levels of adaptability, efficiency, and precision to the AM and MMAM processes, setting a transformative path forward for autonomous digital fabrication. [[Deneault, J. R.]][6]

In summary, while significant strides have been made in the field of AM and MMAM, numerous challenges need to be addressed. 
The following sections will delve into these issues in more detail, examining the current solutions and identifying areas for future research.


### 7. State of the Art Analysis

<!---
    - Strengths and Weaknesses: Analyze the strengths and weaknesses of current methodologies, models, technologies, and applications.
    - Opportunities and Challenges: Discuss current opportunities and challenges in the field.
--->

!!! warning 
    The following sections are work in progress

Given the complexities and challenges associated with Additive Manufacturing (AM) and Multimaterial Additive Manufacturing (MMAM), a multitude of solutions have been proposed and tested, though each with its own set of limitations. 

**Additive Manufacturing (AM):** For improving the dimensional accuracy and surface finish of AM parts, techniques like thermal treatment, machining, and polishing have been used as post-processing steps. However, these methods add to the overall production time and cost. Efforts are ongoing to minimize these requirements by optimizing the printing process itself, such as by fine-tuning process parameters or developing advanced print heads and nozzles.

The high cost and limited availability of some print materials have prompted research into alternative materials. For instance, the use of recycled plastic feedstock in Fused Deposition Modeling (FDM) printers is gaining popularity. Efforts to control and predict printed parts' properties have led to the development of advanced process monitoring systems. These systems use sensors and machine learning algorithms to monitor the printing process, correlating process parameters with the final product attributes.

**Multimaterial Additive Manufacturing (MMAM):** The complexity of MMAM is due, in part, to the interaction between different materials. Researchers have proposed methods such as using interface materials or transitional gradients between different materials to manage this interaction. Moreover, advanced process monitoring and control strategies are under development, seeking to enable simultaneous control of multiple material properties. 

**Real-Time Simulation in AM and MMAM:** Real-time simulation techniques are still being refined. The computational demands of these simulations are significant, particularly for MMAM, which involves multiple materials with differing properties. Current solutions primarily rely on high-performance computing systems and the simplification of mathematical models to reduce computational load. 

Despite these advances, there is a pressing need for faster and more efficient simulation techniques. Hybrid models, combining physics-based and data-driven approaches, are emerging as potential solutions. These hybrid models use machine learning algorithms trained on experimental or simulation data to speed up the simulation process while maintaining a reasonable accuracy level.

These existing solutions, while promising, are not without their limitations and are far from achieving the full potential of AM and MMAM technologies. Addressing the current challenges demands novel approaches and continued research, which this thesis aims to contribute to, focusing on real-time simulation techniques to enhance the MMAM process.

### 8. Real-time Simulation Methods

Graphics Processing Units (GPUs) and parallel computing techniques have revolutionized the field of simulations. A GPU is a hardware component designed to quickly manipulate and alter memory to accelerate the creation of images in a frame buffer intended for output to a display. While CPUs (Central Processing Units) are designed for general-purpose computing and have a few cores optimized for sequential serial processing, GPUs, on the other hand, are specifically built for handling large blocks of data simultaneously, possessing hundreds or thousands of smaller cores.

The principle of simulation parallelization involves partitioning computational tasks into smaller, often similar parts that can be processed simultaneously across multiple cores or threads in a CPU or GPU. This is particularly beneficial in simulations where individual components' interactions can be computed independently.

However, despite the reduced computation time, the complexity of simulations does not necessarily decrease. This is because the complexity is not solely a function of computation time. It also depends on the algorithmic complexity and the problem's intrinsic complexity, such as the number of interacting particles or elements, their properties, and the dynamics of the system.

In the world of simulations, there are several numerical techniques each with their strengths and weaknesses. The most important simulation nowdays is probably the Finite Element Method (FEM): FEM uses a variational approach, based on the minimization of the potential energy of the system. The problem is formulated as follows:

Governing Equation: \( \nabla \cdot (\sigma) + b = 0 \) where \( \sigma \) is the stress tensor and \( b \) is the body force vector.

The Galerkin method is used to approximate this equation, resulting in the weak form:
\( \int_V B^T \sigma dV - \int_V N^T b dV = 0 \)
where B is the strain-displacement matrix and N is the shape function matrix.

In FEM, the problems are usually described using a volumetric mesh as an imput that will be used to define the system of equations. This kind of simulation is used widely for a variety of applications. However, there are not suitable for fast interactive simulation. In our context the simulation is not clearly defined. It is dynamically related to environment and control outputs.

To adress this issue, several other simulation methods were envisaged.

Discrete Element Method (DEM): In DEM, the motion of each particle is tracked. A simple Newton's second law is applied to each particle:

\( m_i \frac{d^2 \textbf{x}_i}{dt^2} = \textbf{F}_{\text{ext}, i} + \sum_{j \neq i} \textbf{F}_{ij} \)
where \(\textbf{x}_i\) is the position of the ith particle, \(m_i\) is its mass, \(\textbf{F}_{\text{ext}, i}\) is the external force, and \textbf{F}_{ij} is the force on ith particle due to the jth particle.

Finite Volume Method (FVM): This method conserves the quantities through a control volume. For a simple diffusion problem, the conservation law is:

\( \frac{d}{dt} \int_V \phi dV + \int_S \textbf{F} \cdot d\textbf{S} = 0 \)
where \phi is the quantity of interest, \(\textbf{F}\) is the flux of \(\phi\), and the integrals are over volume \(V\) and surface \(S\), respectively.

Position-Based Dynamics (PBD): The main idea of PBD is to solve for positions directly, and it does not use explicit forces. Instead, it uses constraint functions C:

\( \Delta \textbf{p}_i = -\frac{1}{w_i} \sum_{C \in \mathcal{C}_i} \lambda_C \nabla_{\textbf{p}_i} C \)
where \(\Delta \textbf{p}_i\) is the position correction, \(w_i\) is the inverse mass, \(\lambda_C\) are Lagrange multipliers, \(\mathcal{C}_i\) is the set of all constraints affecting particle i, and C are the constraint functions.

Material Point Method (MPM): MPM uses particles to carry material state and a background grid for computation. It has two primary steps, particle-to-grid (P2G) transfer and grid-to-particle (G2P) transfer. The equation for momentum update in the grid phase is:

\( \textbf{v}^{n+1}_g = \textbf{v}^{n}_g + \frac{\Delta t}{m^n_g} \left( \textbf{f}^n_g + m^n_g \textbf{g} \right) \)
where \(\textbf{v}^{n+1}_g\) and \(\textbf{v}^{n}_g\) are the velocities of the grid node g at times n+1 and n, respectively, \Delta t is the timestep, m^n_g is the mass of the grid node at time n, \(\textbf{f}^n_g\) is the internal force on the grid node at time n, and \[\textbf{g}\) is the acceleration due to gravity.

Position-Based Fluids (PBF): An offshoot of PBD, PBF allows efficient simulation of incompressible fluids.

However, when it comes to enhancing MMAM, the Material Point Method (MPM) stands out. MPM can handle complex materials, large deformations, and phase changes, making it potentially very suitable for MMAM. MPM discretizes materials into a set of material points with mass and volume, allowing for the simultaneous modeling of both the discrete and continuum characteristics of materials. This characteristic of MPM makes it particularly effective in dealing with the complexities inherent in the MMAM process.

### 9. Critical Discussion

<!---
    - Gaps in the Current Literature: Identify gaps or unaddressed questions in the current state of the art.
    - Implications for Future Research: Discuss the potential implications of these gaps for your thesis and future research.
--->

### 10. Future work

With a clear understanding of the challenges faced in Additive Manufacturing (AM) and Multimaterial Additive Manufacturing (MMAM), several future directions emerge. This thesis aims to make a significant contribution in these areas, specifically focusing on the role of real-time simulation in enhancing MMAM processes.

**Future Directions in Additive Manufacturing (AM) and Multimaterial Additive Manufacturing (MMAM):** To overcome existing hurdles in AM and MMAM, future research and development efforts will likely focus on several key areas:

* Material development and optimization: More extensive research into materials that can be used effectively in MMAM will likely yield new opportunities, such as developing novel composites or biocompatible materials.

* Process control and monitoring: As the complexity of AM and MMAM processes grows, so does the need for sophisticated control systems. This might include advanced sensor systems, artificial intelligence (AI) algorithms, and feedback mechanisms to optimize the process in real-time.

* Post-processing and quality control: As the technology advances, improved post-processing methods and comprehensive quality control measures will be critical to ensuring the reliability and repeatability of AM and MMAM produced parts.

**Role of Real-time Simulation in MMAM:** Real-time simulation can provide invaluable insight into the complex dynamics of MMAM processes, allowing for predictive adjustments and corrections that could significantly improve the quality of the final product. Therefore, the focus of this thesis will be to develop, validate, and optimize a real-time simulation model for MMAM. 

We intend to explore the potential of hybrid models combining physics-based and data-driven approaches, leveraging machine learning techniques to speed up the simulation process without compromising accuracy. We will also investigate how these real-time simulations can be incorporated into a feedback control system, paving the way for a truly adaptive MMAM process.

**Open Science and AM/MMAM Research:** As part of our commitment to the open science movement, all software developed during this research will be open source, and the research data and findings will be made publicly available. This not only aligns with the ethical principles of transparency and reproducibility but also contributes to accelerating innovation in the AM and MMAM community.

In conclusion, this thesis aims to contribute to the broader scientific effort towards optimizing and advancing AM and MMAM technologies. Through focused exploration of real-time simulation techniques and an open science approach, we hope to bring us one step closer to realizing the full potential of these transformative technologies.


### 11. Conclusion

In conclusion, Additive Manufacturing (AM) and Multimaterial Additive Manufacturing (MMAM) are pivotal technologies, poised to revolutionize the manufacturing industry. With their capacity for customization, material versatility, and potential for local, on-demand production, these technologies hold promise to disrupt traditional manufacturing paradigms. 

Nevertheless, AM and MMAM face substantial challenges that need to be addressed to fully exploit their potential. These challenges span from material development and optimization, process control and monitoring, to post-processing and quality control. 

The role of real-time simulation is emerging as a promising avenue to address some of these challenges. By providing a dynamic model of the process, real-time simulations can enable predictive adjustments and improvements in the manufacturing process, enhancing the quality of the final product and minimizing waste. This thesis intends to make a significant contribution in this area, with a particular focus on real-time simulations for MMAM.

Aligned with the principles of open science, all findings and software developed as part of this research will be made publicly available, contributing to the broader scientific effort towards optimizing AM and MMAM. 

As we embark on this journey, we are mindful of the words of Isaac Newton: "If I have seen further, it is by standing on the shoulders of Giants." It is only by building on the work of those who came before us and collaborating with those who work alongside us, that we can push the boundaries of what is possible in AM and MMAM. This thesis represents one step in that direction, and we look forward to the challenges and discoveries that lie ahead.

### 12. References

<font size="2">

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Anderson, C.</font> (2012). Makers: The new industrial revolution. Random House. [DOI][0]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Bacciaglia, A., Ceruti, A., & Liverani, A.</font> (2019, September). Additive manufacturing challenges and future developments in the next ten years. In International Conference of the Italian Association of Design Methods and Tools for Industrial Engineering (pp. 891-902). Cham: Springer International Publishing. [DOI][1]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Chebotar, Y., Handa, A., Makoviychuk, V., Macklin, M., Issac, J., Ratliff, N., & Fox, D.</font> (2019, May). Closing the sim-to-real loop: Adapting simulation randomization with real world experience. In 2019 International Conference on Robotics and Automation (ICRA) (pp. 8973-8979). IEEE. [DOI][2]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Chentanez, N., Müller, M., Macklin, M., Makoviychuk, V., & Jeschke, S.</font> (2018, November). Physics-based motion capture imitation with deep reinforcement learning. In Proceedings of the 11th ACM SIGGRAPH Conference on Motion, Interaction and Games (pp. 1-10). [DOI][3]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Cutcher-Gershenfeld, J., Gershenfeld, A., & Gershenfeld, N.</font> (2020). The promise of self-sufficient production. MIT Sloan Management Review, 62(2). [DOI][4]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;De Vaucorbeil, A., Nguyen, V. P., Sinaie, S., & Wu, J. Y.</font> (2020). Material point method after 25 years: Theory, implementation, and applications. Advances in applied mechanics, 53, 185-398. [DOI][5]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Deneault, J. R., Chang, J., Myung, J., Hooper, D., Armstrong, A., Pitt, M., & Maruyama, B.</font> (2021). Toward autonomous additive manufacturing: Bayesian optimization on a 3D printer. MRS Bulletin, 46, 566-575. [DOI][6]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Gao, M., Wang, X., Wu, K., Pradhana, A., Sifakis, E., Yuksel, C., & Jiang, C.</font> (2018). GPU optimization of material point methods. ACM Transactions on Graphics (TOG), 37(6), 1-12. [DOI][7]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Gravert, S. D., Michelis, M. Y., Rogler, S., Tscholl, D., Buchner, T., & Katzschmann, R. K.</font> (2022, October). Planar modeling and sim-to-real of a tethered multimaterial soft swimmer driven by peano-hasels. In 2022 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS) (pp. 9417-9423). IEEE. [DOI][8]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Jordan, J. M.</font> (2019). 3D Printing. MIT Press.[DOI][9]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Khelafa, I., Ballouk, A., & Baghdad, A.</font> (2021). Control algorithm for the urban traffic using a realtime simulation. Int. J. Electr. Comput. Eng, 11, 3934-3942. [DOI][10]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Macklin, M., & Müller, M.</font> (2013). Position based fluids. ACM Transactions on Graphics (TOG), 32(4), 1-12. [DOI][11]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Macklin, M., Müller, M., Chentanez, N., & Kim, T. Y.</font> (2014). Unified particle physics for real-time applications. ACM Transactions on Graphics (TOG), 33(4), 1-12. [DOI][12]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Macklin, M., Müller, M., & Chentanez, N.</font> (2016, October) XPBD: position-based simulation of compliant constrained dynamics. In Proceedings of the 9th International Conference on Motion in Games (pp. 49-54). [DOI][13]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Maurel, A., Grugeon, S., Fleutot, B., Courty, M., Prashantha, K., Tortajada, H., ... & Dupont, L.</font> (2019). Three-dimensional printing of a LiFePO4/graphite battery cell via fused deposition modeling. Scientific reports, 9(1), 18031. [DOI][14]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Müller, M., Heidelberger, B., Hennix, M., & Ratcliff, J.</font> (2007). Position based dynamics. Journal of Visual Communication and Image Representation, 18(2), 109-118. [DOI][15]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Nazir, A., Gokcekaya, O., Billah, K. M. M., Ertugrul, O., Jiang, J., Sun, J., & Hussain, S.</font> (2023). Multi-material additive manufacturing: A systematic review of design, properties, applications, challenges, and 3D Printing of materials and cellular metamaterials. Materials & Design, 111661. [DOI][16]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Oropallo, W., & Piegl, L. A.</font> (2016). Ten challenges in 3D printing. Engineering with Computers, 32, 135-148. [DOI][17]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Piovarci, M., Foshey, M., Xu, J., Erps, T., Babaei, V., Didyk, P., ... & Bickel, B.</font> (2022). Closed-loop control of direct ink writing via reinforcement learning. arXiv preprint arXiv:2201.11819. [DOI][18]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Prashar, G., Vasudev, H., & Bhuddhi, D.</font> (2022). Additive manufacturing: expanding 3D printing horizon in industry 4.0. International Journal on Interactive Design and Manufacturing (IJIDeM), 1-15. [DOI][19]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Shih, B., Christianson, C., Gillespie, K., Lee, S., Mayeda, J., Huo, Z., & Tolley, M. T.</font> (2019). Design considerations for 3D printed, soft, multimaterial resistive sensors for soft robotics. Frontiers in Robotics and AI, 6, 30.[DOI][20]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Soomro, S. A., Casakin, H., & Georgiev, G. V.</font> (2021). Sustainable design and prototyping using digital fabrication tools for education. Sustainability, 13(3), 1196. [DOI][21]

<font size="3" style="font-variant:small-caps">&nbsp;&nbsp;&nbsp;Tibbits, S.</font> (2021). Things fall together: A guide to the new materials revolution. Princeton University Press. [DOI][22]

</font>

[0]: https://en.wikipedia.org/wiki/Makers:_The_New_Industrial_Revolution "Makers: The New Industrial Revolution"
[1]: https://doi.org/10.1007/978-3-030-31154-4_76 "Additive manufacturing challenges and future developments in the next ten years"
[2]: https://arxiv.org/pdf/1810.05687.pdf "Closing the sim-to-real loop: Adapting simulation randomization with real world experience"
[3]: https://matthias-research.github.io/pages/publications/a1-chentanez.pdf "Physics-based motion capture imitation with deep reinforcement learning"
[4]: https://cba.mit.edu/docs/papers/20.12.SMR.pdf "The promise of self-sufficient production"
[5]: https://doi.org/10.1016/bs.aams.2019.11.001 "Material point method after 25 years: Theory, implementation, and applications"
[6]: https://doi.org/10.1557/s43577-021-00051-1 "Toward autonomous additive manufacturing: Bayesian optimization on a 3D printer"
[7]: https://pages.cs.wisc.edu/~sifakis/papers/GPU_MPM.pdf "GPU optimization of material point methods"
[8]: https://arxiv.org/pdf/2208.00731.pdf "Planar modeling and sim-to-real of a tethered multimaterial soft swimmer driven by Peano-hasels"
[9]: https://mitpress.mit.edu/9780262536684/3d-printing/  "3D Printing. MIT Press"
[10]: https://doi.org/10.11591/IJECE.V11I5.PP3934-3942 "Control algorithm for the urban traffic using a real-time simulation"
[11]: https://matthias-research.github.io/pages/publications/pbf_sig_preprint.pdf "Position-based fluids"
[12]: http://mmacklin.com/uppfrta_preprint.pdf "Unified particle physics for real-time applications"
[13]: https://matthias-research.github.io/pages/publications/XPBD.pdf "XPBD: position-based simulation of compliant constrained dynamics"
[14]: https://www.nature.com/articles/s41598-019-54518-y "Three-dimensional printing of a LiFePO4/graphite battery cell via fused deposition modeling"
[15]: http://kucg.korea.ac.kr/new/seminar/2014/ppt/ppt-2014-10-21.pdf "Position-based dynamics"
[16]: https://doi.org/10.1016/j.matdes.2023.111661 "Multi-material additive manufacturing: A systematic review of design, properties, applications, challenges, and 3D Printing of materials and cellular metamaterials"
[17]: https://doi.org/10.1007/s00366-015-0407-0 "Ten challenges in 3D printing"
[18]: https://arxiv.org/pdf/2201.11819 "Closed-loop control of direct ink writing via reinforcement learning"
[19]: https://DOI.springer.com/content/pdf/10.1007/s12008-022-00956-4.pdf "Additive manufacturing: expanding 3D printing horizon in industry 4.0"
[20]: https://www.frontiersin.org/articles/10.3389/frobt.2019.00030/full "Design considerations for 3D printed, soft, multimaterial resistive sensors for soft robotics"
[21]: https://doi.org/10.3390/su13031196 "Sustainable design and prototyping using digital fabrication tools for education"
[22]: https://doi.org/10.1515/9780691189710 "Things fall together: A guide to the new materials revolution"


</div>