# Lectures : 

## Multimaterial Additive manufacturing

[Recent advances in multi-material additive manufacturing: methods and applications](./files/1-s2.0-S2211339820300204-main.pdf)

[Multiple material additive manufacturing – Part 1: a review](./files/MultimaterialPrinting-Survey-2013.pdf)

[A Review of Multi-material and Composite Parts Production by Modified Additive Manufacturing Methods](./files/review-multimat-fab.pdf)


[The m4 3D printer: A multi-material multi-method additive manufacturing platform for future 3D printed structures](./files/1-s2.0-S2214860419301812-main.pdf)

[Multi-materials and multi-functionality enabled by hybrid additive manufacturing](./files/XJUHASZ_232293[1131].pdf)

[HydraX, a 3D printed robotic arm for Hybrid Manufacturing. Part I: Custom Design, Manufacturing and Assembly](./files/1-s2.0-S2351978920318710-main.pdf)

[3D-Printed Mechanochromic Materials](./files/mechanochromy.pdf)

Multimaterial  :
Polymère - Polymère
Polymère - Métaux
Métaux - Métaux



## Compatibilité des polymères

[Etat de l’art concernant la compatibilité des matières plastiques](Rapport_record00-0904_1A.pdf)

!!! info "grade ou Melt Flow d'un polymère"
    Le grade ou Melt Flow Index d’un polymère est le poids de matière extrudée en 600s à travers une
    filière normalisée (AFNOR). C’est donc une valeur qui diminue lorsque la masse molaire
    augmente
    Modèle de Palierne

https://www.researchgate.net/publication/292141814_Compatibilite_chimique_et_comportement_mecanique_et_thermique_d'un_nouveau_materiau_composite_charge_d'un_bio-polymere_fibreux

[Compatibilité chimique database]{https://compatibilite-chimique.com/index.php}

Logiciel Avogadro

Différent type d'adhesion Chemical Dispersive Mechanical

## 4D Printing 

[Design for 4D printing: A voxel-based modeling and simulation of smart materials](./files/voxelbasedMultimaterial.pdf)

[Design for 4D printing: Modeling and Computation of Smart Materials Distributions](./files/voxel-simulation-smartmaterial.pdf)

### Electro-striction

[ELECTROSTRICTIVE POLYMER ARTIFICIAL MUSCLE ACTUATORS](./files/ROBOT.1998.680638.pdf)

[Matériaux électrostrictifs pour actuateurs](./files/materiaux-electrostrictifs.pdf)

### Electro-reaction

[Electroactive polymers for sensing](./files/selfsensing.pdf)

[Stretchable pumps for soft machines](./files/s41586-019-1479-6.pdf)

### Thermo-reaction

[Self-expanding/shrinking structures by 4D printing](./files/3Dprinting-expandable.pdf)

[3D printing of smart materials: A review on recent progresses in 4D printing](./files/4Dprinting-review-2015.pdf)

[Active control of 4D prints: Towards 4D printed reliable actuators and sensors]("./files/Active control of 4D prints self sensing material.pdf")

[Hydraulically amplified self-healing electrostatic actuators with muscle-like performance](./files/haselActuators.pdf)

[Inkjet Printing for Silicon Solar Cells](./files/Inkjet-Printing-Silicon-Solar-Cells.pdf)


### Magneto-striction

[Magnetostriction and magnetostrictive materials for sensing applications](./files/MagnetoStriction-deformation sensor.pdf)

!!! info 
    La magnétostriction contribue au ronronnement qu'on peut entendre près des transformateurs et des appareils électriques fonctionnant sous haute tension, d'une fréquence fondamentale égale à deux fois celle du courant, c'est-à-dire 2×50 Hz = 100 Hz en Europe. La force électromécanique I×B, produit du courant I par le champ B, participe aussi à ce bruit en agissant sur les conducteurs baignant dans le champ magnétique qu'ils génèrent.

### Deformable parts

[4D Printing Inflatable Silicone Structures](./files/4DInflatables.pdf)


## Microfluidic & DIW

[ Microfluidic Printheads for Multimaterial 3D Printing of Viscoelastic Inks](./files/adma.201500222.pdf)

[ A New 3D Printing Strategy by Harnessing Deformation, Instability, and Fracture of Viscoelastic Inks](./files/adma.201704028.pdf)












