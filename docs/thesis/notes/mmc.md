
Objects can handle several types of displacements.

##### Rigid body displacements
Rigid body displacements describe how solids move into space without local deformation.

The rigid body displacements is defined as \(x = X + c\) where \(c\) is a constant vector.

The rigid body rotation is defined as \(x = QX\) where \(Q\) is a rotation matrix.

Thus generalized rigid body motion is defined as \(x = QX + c\).

##### Volumetric deformation

Our continuum can also handle uniform extension and contraction. 
such deformation could be defined in \(R^3\) as 

\[
x = 
\left[\begin{array}{c}
x_1 \\
x_2 \\
x_3
\end{array}\right]
=
\begin{bmatrix}
k_1 & 0 & 0 \\
0 & k_2 & 0\\
0 & 0 & k_3
\end{bmatrix}\cdot
\left[\begin{array}{c}
X_1 \\
X_2 \\
X_3
\end{array}\right]
=
\left[\begin{array}{c}
k_1 \cdot X_1 \\
k_2 \cdot X_2 \\
k_3 \cdot X_3
\end{array}\right]
\]

Three scenario : 

1. Contraction \((0 < k < 1)\)
2. Expansion \((k > 1)\)
3. No change in length \((k = 1)\)
4. If \((k \leqslant 0)\), the motion is not physically possible (inversion)


##### Shear deformation

An object can also be loaded to shear. Simple shear is defined as 

\[
x = 
\begin{bmatrix}
1 & tan(\theta) & 0 \\
0 & 1 & 0\\
0 & 0 & 1
\end{bmatrix}\cdot
\left[\begin{array}{c}
X_1 \\
X_2 \\
X_3
\end{array}\right]
=
\left[\begin{array}{c}
X_1 + tan(\theta) X_2 \\
X_2 \\
X_3
\end{array}\right]
\]

Pure shear is defined as : 

\[
x = 
\begin{bmatrix}
1 & tan(\theta/2) & 0 \\
tan(\theta/2) & 1 & 0\\
0 & 0 & 1
\end{bmatrix}\cdot
\left[\begin{array}{c}
X_1 \\
X_2 \\
X_3
\end{array}\right]
=
\left[\begin{array}{c}
X_1 + tan(\theta/2) X_2 \\
X_2 + tan(\theta/2) X_1 \\
X_3
\end{array}\right]
\]

##### Strain

Strain is defined as relative deformation, compared to a reference position configuration.
It is a dimensionless mesure of the deformation relative to the initial configuration.
When the basis we are using is not orthonormal, the strain is measure according to the metric tensor.

Strain is defined as the spatial derivative of displacement :

\(
\varepsilon = \frac{\partial }{\partial X} (x - X)
\)

<div id="sketch3"></div><script>
{
let sketch = function(p) {
  p.yoff = 0.0;
  p.setup = function() {
	console.log("createCanvas");
	p.createCanvas(300, 300)
	p.textFont('Cambria');
	p.clear();
	p.background(255, 0);

	p.noiseSeed(12);
	p.drawPotato(80, 70, 70)
	p.noiseSeed(64);
	p.drawPotato(220, 160, 70)

	p.text("Ω", 60, 35);
	p.text("°", 68, 43);
	p.text("Ω", 230, 130);
	p.drawNamedArrow(15, p.height -20, 50, 0, "e1", 1, 5,5);
	p.drawNamedArrow(15, p.height -20, 50, -90, "e2", 1, 5,0);
	p.drawNamedArrow(15, p.height -20, 20, 120, "e3", 1, 8, -1);

	p.drawAbsNamedArrow(15, p.height -20, 70, 80, "X", 0.5, 5, 5);
	p.drawAbsNamedArrow(15, p.height -20, 200, 190, "x(X)", 0.5, 3, 10);
	p.drawAbsNamedArrowDot(70, 80, 200, 190, "u(x)", 0.5, 3, -2);
	p.drawAbsNamedArrow(70, 80, 90, 50, "dX", 0.5, -15, -2);
	p.drawAbsNamedArrow(200, 190, 250, 160, "dx", 0.5, -15, -2);
	p.drawAbsNamedArrowDot(90, 50, 250, 160, "u(X + dX)", 0.5, -15, -20);

  };
};window.addEventListener('load', () => { let p5_instance = new p5(sketch, "sketch3");});
}
</script>

##### Stress

Stress expresses the internal forces that neighbouring particles of a continuous material exert on each other, while strain is the measure of the relative deformation of the material.
Stress is related to strain from the generalized Hooke's law for elasticity, which in its most basic form, correlates stresses and strains through a linear relationship. However, this law expands to accommodate anisotropic and non-linear materials by employing a tensorial framework:

\[ \sigma_{ij} = C_{ijkl} \epsilon_{kl} \]

\[ \sigma_{ij} = C_{ijkl} \epsilon_{kl} \]


---