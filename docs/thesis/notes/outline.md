# Notes
## Introduction

**Global Introduction**
1. **Global Challenges and the Need for Agile Manufacturing**
Global Challenges and the Need for Agile Manufacturing
- _Keywords:_ Global challenges, Agility, Resource optimization
- _Discussion Points:_ The necessity to adapt manufacturing processes in response to global challenges and the importance of developing new materials and products.

2. **The Rise of Additive Manufacturing During and Post-COVID-19**
- _Keywords:_ Additive manufacturing, COVID-19 resilience, Industrial R&D, Patent surge
- _Discussion Points:_ The development and growth of additive manufacturing as a resilient manufacturing process during the COVID-19 pandemic, and the subsequent industrial investment and innovation in this field.

**Multimaterial Additive Manufacturing (MMAM)**
3. **Challenges and Opportunities in Multimaterial AM**
- _Keywords:_ Multimaterial, Sensor integration, Actuator integration, Material interaction
- _Discussion Points:_ The shift towards multimaterial additive manufacturing to include sensors and actuators, the challenges involved such as material interaction, and the need for combining multiple processes in design.

**Inverse Design in MMAM**
4. **The Concept and Importance of Inverse Design**
- _Keywords:_ Inverse design, Product performance, Computational models, Optimal solutions
- _Discussion Points:_ Introduction to inverse design, starting from desired product performance to achieve optimal manufacturing solutions, its significance in MMAM.

5. **Challenges in Implementing Inverse Design for MMAM**
- _Keywords:_ Machine-specific requirements, Slicer specificity, Control complexity
- _Discussion Points:_ The complexity of implementing inverse design in MMAM due to the need for process-specific machines and control systems.

**Simulation and Optimization in MMAM**
6. **Leveraging Machine Learning and Fast Simulation**
- _Keywords:_ Generative AI, Machine learning, Fast simulation, Experimentation efficiency
- _Discussion Points:_ The role of machine learning and AI in automating inverse design, the need for fast simulation methods to support both manual and automated design processes.

**Contributions of the PhD**
7. **Addressing Warping in Additive Manufacturing**
- _Keywords:_ Warping defect, Automated optimization, GPU computing framework
- _Discussion Points:_ The focus of the PhD on developing a fast simulation method and GPU computing framework to address warping, a common defect in AM processes.

---

# I. Multimaterial additive manufacturing
## 1. Background
### 1.1 Additive Manufacturing
#### 1.1.1 History and Evolution
#### 1.1.2 Core Principles and Technologies
### 1.2 Multi-material Additive Manufacturing
#### 1.2.1 Definition and Importance
#### 1.2.2 Key Techniques and Methods
## 2. Recent Advance
### 2.1 Technological innovation in MMAM
### 2.2 Advances in Material Science
### 2.3 Interdisciplinary innovations
## 3. Challenges and Opportunities
### 3.1 Technical Challenges
### 3.2 Economic and Industrial Challenges
### 3.3 Opportunities
## 4. Theoritical background in MMAM
### 4.1 Fundamentals of Additive manufacturing
#### 4.1.1 Basic principles
### 4.2 Key Theories and Models
#### 4.2.1 Material science principles
#### 4.2.2 Continuum mechanics
#### 4.2.3 Thermomechanical modeling
#### 4.2.4 Interfacial bonding
#### 4.2.5 Numerical Methods
##### 4.2.5.1 Finite Element Method (FEM)
##### 4.2.5.2 Finite Volume Method (FVM)
##### 4.2.5.3 Finite Difference Method (FDM)
##### 4.2.5.4 Meshfree Methods


<!--
**1. Introduction to Multimaterial Additive Manufacturing (MMAM)**
- _Overview:_ Define MMAM and its significance in the broader context of additive manufacturing.
- _Keywords:_ MMAM, additive manufacturing, multimaterial.

**2. Existing Processes in MMAM**
- _Discussion Points:_ Detailed description of various MMAM processes, including material jetting, powder bed fusion, and fused deposition modeling, emphasizing their capabilities and material compatibilities.
- _Keywords:_ Material jetting, powder bed fusion, fused deposition modeling, process capabilities.

**3. Recent Advances and Challenges**
- _Overview:_ Overview of recent technological advancements in MMAM, focusing on innovations that enhance the capabilities of multimaterial printing.
- _Challenges:_ Discuss the technical and operational challenges faced by MMAM, such as material compatibility, process control, and the integration of dissimilar materials.
- _Keywords:_ Technological advancements, material compatibility, process control.

**4. Innovations Leveraging MMAM**
- _Discussion Points:_ Highlight specific innovations and applications that have been made possible through MMAM, such as complex medical implants and advanced aerospace components.
- _Keywords:_ Medical implants, aerospace components, application-specific innovations.

**5. Dynamical Behavior During the Printing Process**
- _Overview:_ Explore the dynamic behaviors encountered during MMAM processes, focusing on how these affect print quality and material properties.
- _Case Study: Warping Effect:_ Illustrate the challenges of dynamical behavior by examining the warping effect when combining materials with differing thermomechanical properties.
- _Keywords:_ Dynamical behavior, warping effect, thermomechanical properties.

**6. Static vs. Dynamic Control in MMAM**
- _Comparison:_ Compare the static nature of GCODE used in 3D printing with the dynamic control strategies employed in modern robotics.
- _Discussion Points:_ Analyze the limitations of GCODE in handling the dynamic behaviors of materials during MMAM processes and propose the need for more adaptive and responsive control mechanisms.
- _Keywords:_ GCODE, dynamic control, robotics, 3D printing.

**7. Transitioning Toward Dynamic Control Strategies**
- _Conclusion:_ Argue for a paradigm shift in how 3D printers are controlled, drawing parallels with the adaptability and flexibility seen in robotics.
- _Transition to Chapter 2:_ Set the stage for the next chapter, which will delve into the development of new control strategies for MMAM that align with the dynamic strategies employed in modern robotics.
- _Keywords:_ Control strategies, adaptability, robotics, paradigm shift.

Chapter 1 establishes a comprehensive foundation for understanding MMAM, detailing its processes, recent advancements, and the unique challenges it faces, particularly in terms of dynamical behavior during printing. The comparison between static GCODE programming and dynamic robotics control systems underscores the need for innovation in control strategies, leading directly into the focus of Chapter 2 on advancing MMAM control techniques.

-->

---

## Chapter 2: Advancing Control Strategies in Multimaterial Additive Manufacturing (MMAM)

**1. The Need for Advanced Control in MMAM**
- _Overview:_ Highlight the complexity of MMAM processes and the necessity for control systems that can adapt to the dynamic interactions of multiple materials.
- _Keywords:_ Complex systems, adaptive control, MMAM challenges.

**2. Principles of Robotics Control Applied to MMAM**
- _Discussion Points:_ Discuss how principles from robotics, such as real-time feedback and adaptability, can be applied to enhance MMAM control systems.
- _Keywords:_ Robotics principles, feedback systems, adaptability.

**3. Comparative Analysis of Control Systems**
- _Overview:_ Compare the static nature of traditional GCODE-based systems with the dynamic control mechanisms found in robotics, underscoring the benefits of the latter.
- _Keywords:_ GCODE limitations, dynamic control, comparative analysis.

**4. Case Studies: Implementing Robotics Control in MMAM**
- _Discussion Points:_ Present examples where advanced control strategies have led to significant improvements in MMAM, highlighting the role of real-time adjustments and adaptive control.
- _Keywords:_ Real-time adjustments, adaptive control, case studies.

**5. Challenges in Integrating Advanced Control Systems**
- _Overview:_ Address the hurdles in applying robotics control principles to MMAM, including software, hardware, and system compatibility issues.
- _Keywords:_ Integration challenges, system compatibility, technical hurdles.

**6. Emerging Technologies in MMAM Control**
- _Discussion Points:_ Explore how emerging technologies, such as AI and sensors, are enabling more sophisticated control strategies in MMAM, paving the way for real-time process adjustments.
- _Keywords:_ AI technologies, sensor integration, emerging technologies.

**7. Preparing for Multiphysics Modeling in MMAM**
- _Overview:_ Discuss the importance of advanced control strategies as a foundation for implementing multiphysics modeling in MMAM, emphasizing how these controls facilitate the accurate simulation of complex interactions in real-time.
- _Keywords:_ Multiphysics modeling, simulation accuracy, real-time control.

**8. Conclusion and Transition to Chapter 3**
- _Conclusion:_ Summarize the chapter, emphasizing the critical role of advanced control strategies in overcoming the dynamic challenges of MMAM and setting the stage for more sophisticated modeling techniques.
- _Transition to Chapter 3:_ Introduce the next chapter's focus on leveraging a multiphysics approach to model MMAM processes, highlighting how this approach builds on the advanced control strategies discussed, aiming to implement a real-time solver for improved process optimization and efficiency.
- _Keywords:_ Advanced modeling, real-time solver, process optimization.

Chapter 2 delves into the critical need for innovative control strategies in MMAM, drawing parallels with robotics to illustrate the potential benefits of adopting more dynamic and adaptive control systems. Through comparative analysis, case studies, and a discussion of emerging technologies, this chapter lays out a roadmap for integrating advanced control mechanisms into MMAM, setting the stage for exploring specific applications and capabilities enabled by these advancements in the subsequent chapter.
By highlighting the preparation for multiphysics modeling as a key aspect of advancing MMAM controls, the transition to Chapter 3 becomes natural, providing a logical flow from discussing the theoretical and practical aspects of control strategies to the mathematical and computational models that enable these advances.

---

## Chapter 3: Modeling Methodologies in Multimaterial Additive Manufacturing (MMAM)

**1. Overview of Additive Manufacturing Processes**
- _Introduction:_ Begin with a comprehensive list of AM processes such as Fused Filament Fabrication (FFF), Selective Laser Sintering (SLS), etc., setting the stage for in-depth modeling discussions.
- _Keywords:_ AM processes, FFF, SLS.

**2. Physical Models in AM**
- _Discussion Points:_ Detail the physical models commonly utilized to study various AM processes, emphasizing the importance of accurately capturing the physics of material transformation.
- _Keywords:_ Physical models, material transformation, study AM processes.

**3. The Transformation of Material Phases**
- _Overview:_ Explain how AM machines transform materials from various phases (liquid, solid, granular) into a solid product, necessitating fast and accurate modeling of this process.
- _Keywords:_ Material phases, transformation, AM machines.

**4. Modeling Physical Behaviors in AM**
- _Discussion Points:_ Introduce and explain the required physical behaviors for modeling AM processes, including heat transfer, material behavior, and continuum mechanics.
- _Simplification Hypotheses:_ Discuss assumptions and simplifications made to streamline the modeling process without significantly compromising accuracy.
- _Keywords:_ Heat transfer, material behavior, continuum mechanics, simplification.

**5. Meshfree Methods for Dynamic Nature of AM**
- _Introduction to Meshfree Methods:_ Address the limitations of traditional Finite Element Method (FEM) due to the dynamic nature of AM processes, leading to the adoption of meshfree methods.
- _SPH Framework:_ Introduce the Smoothed Particle Hydrodynamics (SPH) framework as a preferred meshfree method for modeling AM processes, explaining its advantages in handling complex material behaviors and interactions.
- _Keywords:_ Meshfree methods, SPH, dynamic AM processes.

**6. Real-Time Modeling with Position-Based Dynamics**
- _Comparison of Solving Frameworks:_ Compare Position-Based Dynamics (PBD) with other methods such as force-based solvers, impulse-based solvers, and the distinctions between implicit and explicit methods, highlighting the advantages of PBD in real-time modeling.
- _Advantages of PBD:_ Discuss the benefits of using PBD in the context of AM, particularly its suitability for real-time applications and its ability to adaptively handle complex physical interactions.
- _Keywords:_ Position-Based Dynamics, real-time modeling, PBD advantages.

**7. Introduction to Chapter 4: Advancing Towards Mass Parallelization**
- _Transition:_ Conclude by outlining how the current modeling framework, particularly the SPH and PBD methods, stands to benefit significantly from mass parallelization to enhance computational efficiency and real-time capability.
- _Preview of Chapter 4:_ Introduce the next chapter's focus on implementing these modeling methodologies, leveraging mass parallelization techniques to achieve scalable, efficient, and accurate real-time modeling of MMAM processes.
- _Keywords:_ Mass parallelization, computational efficiency, Chapter 4 preview.

Chapter 3 delves deep into the modeling methodologies essential for understanding and optimizing MMAM processes. By starting from the basics of AM processes and moving through to sophisticated meshfree and real-time modeling techniques, this chapter lays a solid foundation for the technical advancements discussed in subsequent chapters. The transition to Chapter 4 promises a detailed exploration of how these methodologies can be implemented on a large scale, emphasizing the role of computational advances in the future of MMAM.

--- 

# Chapter 4: Implementing Real-Time Multiphysics Modeling in MMAM: GPU Computing and Performance Analysis

**1. Overview of the Global Architecture**
- _Introduction:_ Begin with a high-level overview of the global architecture designed for real-time multiphysics modeling in MMAM, detailing the components and their interactions.
- _Keywords:_ Global architecture, real-time modeling, MMAM.

**2. Fundamentals of GPU Computing**
- _Introduction to GPU:_ Explain what a Graphics Processing Unit (GPU) is, its role in high-performance computing, and its significance in accelerating MMAM modeling processes.
- _How GPUs Work:_ Delve into the operational principles of GPUs, highlighting their parallel processing capabilities and how these differ from traditional CPU processing.
- _Keywords:_ GPU computing, high-performance computing, parallel processing.

**3. Optimizing GPU Performance for MMAM**
- _Efficiency Strategies:_ Discuss strategies for maximizing GPU efficiency in the context of MMAM modeling, including the use of Structures of Arrays (SoA), concurrency, memory coherence, and optimizing occupancy.
- _Implementation Tips:_ Provide practical tips on implementing these strategies to ensure efficient use of GPU resources in complex MMAM simulations.
- _Keywords:_ GPU optimization, SoA, concurrency, memory coherence, occupancy.

**4. Performance Analysis of GPU-Implemented MMAM Modeling**
- _Comparative Analysis:_ Offer a detailed performance analysis comparing the impact of enabling/disabling specific features (e.g., SoA, multi-threading) on the modeling process.
- _Benchmarking:_ Present benchmark results to demonstrate the efficiency gains achieved through various optimization strategies.
- _Keywords:_ Performance analysis, benchmarking, feature comparison.

**5. Stability Analysis in MMAM Modeling**
- _Overview:_ Introduce the concept of stability in the context of real-time MMAM modeling, explaining why it's crucial for reliable simulations.
- _Stability Criteria:_ Describe the criteria used to assess the stability of a given simulation scenario, including the impact of different settings and configurations.
- _Scenario Analysis:_ Provide examples of simulation scenarios, discussing which configurations lead to stable results and why.
- _Keywords:_ Stability analysis, simulation scenarios, stability criteria.

**6. Conclusion and Transition to Chapter 5**
- _Summary:_ Recap the key points discussed in Chapter 4, emphasizing the importance of GPU implementation and optimization for advancing MMAM modeling.
- _Transition to Chapter 5:_ Introduce the next chapter, which will explore the practical applications of the developed MMAM modeling framework in real-world additive manufacturing scenarios, demonstrating how the performance and stability analyses contribute to enhancing MMAM processes.
- _Keywords:_ GPU computing, MMAM advancements, practical applications.

Chapter 4 delves into the technical implementation of real-time multiphysics modeling for MMAM, with a focus on leveraging GPU computing to achieve high performance and stability. Through an in-depth discussion of GPU architecture, optimization strategies, and comprehensive performance and stability analyses, this chapter lays the groundwork for applying these advancements in practical MMAM applications, setting the stage for further exploration in the subsequent chapter.

---

# Chapter 5: Application of Real-Time Multiphysics Modeling to MMAM Fused Deposition Modeling (FDM) Printing

**1. Introduction to MMAM FDM Printing Challenges**
- _Overview:_ Begin by describing the specific challenges faced in MMAM FDM printing, particularly when combining materials that lead to significant warping issues.
- _Keywords:_ MMAM FDM printing, material combination, warping challenges.

**2. Comprehensive Analysis of Warping Phenomena**
- _Warping Phenomena:_ Provide a detailed examination of warping in MMAM FDM printing, including the conditions under which it occurs and its effects on print quality.
- _Hypotheses Formation:_ Discuss several hypotheses regarding the causes of warping, based on material properties, thermal gradients, and mechanical stresses.
- _Keywords:_ Warping analysis, hypotheses, thermal gradients.

**3. Analytic Calculus for Warping**
- _Methodology:_ Explain the analytic calculus approach used to model warping phenomena, detailing the mathematical models and assumptions involved.
- _Simplifications:_ Describe any simplifications made for the sake of analytic tractability, and their implications on the model's accuracy.
- _Keywords:_ Analytic calculus, mathematical modeling, model simplifications.

**4. Simulation Framework Application**
- _Simulation Framework Overview:_ Introduce the simulation framework developed in previous chapters, emphasizing its application to warping in MMAM FDM printing.
- _Demonstration of Usefulness:_ Use the framework to demonstrate the practicality of the solution, showcasing how it can predict and mitigate warping.
- _Keywords:_ Simulation framework, warping prediction, solution demonstration.

**5. Warping Simulation and Optimization Problem Solving**
- _Warping Simulation:_ Present results from the warping simulation, detailing the parameters used and the outcomes observed.
- _Optimization Problem Solving:_ Discuss how the simulation framework is used to solve optimization problems, specifically aiming to minimize warping in MMAM parts.
- _Keywords:_ Warping simulation, optimization solving, minimization strategies.

**6. Comparison with Reality and FEM Analysis**
- _Real-world Comparison:_ Compare the simulation results with actual MMAM FDM printing outcomes, evaluating the accuracy of the simulation.
- _FEM Analysis Comparison:_ Contrast the simulation framework's predictions with those from traditional Finite Element Method (FEM) analyses, highlighting the advantages and limitations of each.
- _Keywords:_ Real-world comparison, FEM analysis, accuracy evaluation.

**7. Conclusion and Outlook**
- _Chapter Summary:_ Recapitulate the key findings and contributions of the chapter, emphasizing the significance of the simulation framework in addressing warping in MMAM FDM printing.
- _Future Directions:_ Suggest future research directions, including potential improvements to the simulation framework and its application to other MMAM challenges.
- _Keywords:_ Chapter summary, future research, simulation advancements.

Chapter 5 delves into the practical application of the real-time multiphysics modeling framework to tackle the challenge of warping in MMAM FDM printing. Through comprehensive analysis, analytic calculus, and simulation, this chapter demonstrates the framework's utility in predicting and mitigating warping, offering a significant advancement over traditional methods. Comparisons with real-world outcomes and FEM analysis further validate the framework's effectiveness, paving the way for future enhancements and broader applications.

---

# Chapter 6: Addressing Porosity in Selective Laser Sintering (SLS) Processes through Multiphysics Simulation

**1. Introduction to Porosity Challenges in SLS**
- _Overview:_ Start with an overview of the Selective Laser Sintering (SLS) process and the critical issue of porosity that affects the mechanical properties and quality of the final products.
- _Keywords:_ SLS process, porosity challenges, product quality.

**2. Analysis of Porosity Formation**
- _Formation Mechanisms:_ Delve into the mechanisms behind porosity formation in SLS, considering factors like particle size distribution, laser parameters, and thermal gradients.
- _Hypotheses Development:_ Propose hypotheses for the causes of porosity, focusing on the interaction between granular material behavior and melt pool dynamics.
- _Keywords:_ Porosity formation, granular materials, melt pool dynamics.

**3. Mathematical Modeling of Porosity**
- _Modeling Approach:_ Describe the mathematical models used to represent porosity in the SLS process, including the physics of granular material consolidation and laser-material interaction.
- _Simplification and Assumptions:_ Discuss the assumptions made to simplify the model without significantly compromising its predictive capability.
- _Keywords:_ Mathematical modeling, consolidation physics, laser-material interaction.

**4. Application of the Simulation Framework to SLS**
- _Simulation Framework Adaptation:_ Outline how the previously discussed simulation framework is adapted to model the SLS process and its effectiveness in predicting porosity.
- _Utility Demonstration:_ Show how the framework can be used to identify process parameters that minimize porosity, enhancing the quality of SLS-manufactured parts.
- _Keywords:_ Simulation framework, SLS modeling, porosity prediction.

**5. Porosity Simulation and Optimization Strategies**
- _Simulation Results:_ Present the outcomes of porosity simulations, detailing how different SLS process parameters influence porosity levels.
- _Optimization Problem Solving:_ Explain how the simulation framework aids in solving optimization problems aimed at reducing porosity in SLS parts.
- _Keywords:_ Porosity simulation, optimization strategies, process parameters.

**6. Validation Against Experimental Data and Comparative Analysis**
- _Experimental Validation:_ Compare simulation results with experimental data from SLS processes to validate the accuracy of the porosity models.
- _Comparison with Other Models:_ Contrast the simulation framework's performance with that of other modeling approaches, such as traditional FEM, in predicting and mitigating porosity.
- _Keywords:_ Experimental validation, comparative analysis, model performance.

**7. Conclusion and Future Work**
- _Chapter Recap:_ Summarize the chapter’s key insights and the impact of the simulation framework on understanding and controlling porosity in SLS processes.
- _Directions for Future Research:_ Suggest avenues for future investigation, potentially including the exploration of more complex material interactions and the integration of machine learning for process optimization.
- _Keywords:_ Chapter conclusion, future research directions, machine learning integration.

Chapter 6 extends the application of the real-time multiphysics modeling framework to tackle porosity in SLS, a prevalent issue affecting the integrity of printed parts. Through a detailed analysis of porosity formation, mathematical modeling, and optimization via simulation, this chapter showcases the framework's capability to significantly improve SLS process outcomes. Validation against experimental data and comparison with traditional modeling approaches underscore the framework’s value, setting the stage for further advancements in additive manufacturing research and practice.

**Conclusion and Future Work**

**Conclusion**
- _Summary of Contributions:_ Recapitulate the significant contributions of the dissertation, highlighting the development and application of the real-time multiphysics simulation framework to address critical challenges in multimaterial additive manufacturing (MMAM), such as warping in FDM and porosity in SLS processes.
- _Impact on MMAM:_ Emphasize the framework's role in advancing the understanding and optimization of MMAM processes, leading to improved quality and efficiency in additive manufacturing.

**Future Work**
1. **Optimization Techniques**
   - _Discussion Points:_ Explore advanced optimization techniques to further enhance the efficiency and effectiveness of the simulation framework. Consider the integration of machine learning algorithms for predictive modeling and optimization of AM processes.
   - _Keywords:_ Advanced optimization, machine learning, predictive modeling.

2. **Material Spreading and Collaboration**
   - _Overview:_ Investigate methods for accurately simulating material spreading in powder bed processes and the collaborative effects of materials in MMAM. This includes studying the physical interactions at the microscale and their impact on macroscopic properties.
   - _Keywords:_ Material spreading, physical interactions, microscale effects.

3. **Adaptive Refinement**
   - _Discussion Points:_ Develop adaptive refinement techniques within the simulation framework to dynamically adjust resolution and computational resources based on the complexity of the simulated process. This would improve accuracy and efficiency, particularly in regions of high gradient or complexity.
   - _Keywords:_ Adaptive refinement, dynamic adjustment, computational efficiency.

4. **Multi-GPU Support**
   - _Overview:_ Expand the simulation framework to support multi-GPU environments, enhancing its scalability and performance. This involves optimizing data distribution and parallel computations across multiple GPUs to handle larger, more complex simulations.
   - _Keywords:_ Multi-GPU support, scalability, data distribution.

5. **Integration in a Full AM Workflow**
   - _Discussion Points:_ Aim to fully integrate the simulation framework into a comprehensive AM workflow, from initial design and slicing to the final print process. This includes developing interfaces and protocols for seamless data exchange and process control across different stages of AM.
   - _Keywords:_ Workflow integration, process control, seamless data exchange.

**Closing Remarks**

- _Reflect on the Journey:_ Offer reflections on the journey of the research, acknowledging the complexities of MMAM and the potential for future innovations.
- _Call to Action:_ Encourage continued research and collaboration within the AM community to further explore the outlined future work areas, driving the field towards new heights of innovation and efficiency.
