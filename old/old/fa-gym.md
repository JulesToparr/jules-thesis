---
hide:
  - navigation
  - toc
---

# :material-bookmark: Article

<font size="5">Multi-Material Additive Manufacturing : Optimization through massively parallel simulation</font><br> 
<font size="4" color="black"> Jules TOPART, Nicolas DUPORT, Guillaume CHIVOT, Thomas FIOLET, Stéphane PANIER</font><br>
<font size="3"> Laboratoire de technologies innovantes (LTI), Université Picardie Jules Verne</font><br>
<font size="3"> B2R, Institut poly-technique UniLaSalle</font><br>
<font size="2">
__Key words__ : mecanical engineering, computationnal physics, material science, robotics, machine learning
</font><br>

<div markdown="1" style="column-count: 2; column-gap: 30px;">

## Context

In today's fast-paced world, we face numerous challenges, such as the COVID-19 pandemic, energy shortages, biodiversity loss, and climate change. These issues are rapidly shifting from urgent to critical, necessitating swift adaptation strategies. Concurrently, there's a rising consumer demand for personalized products in various industries. This shift highlights the need for factories to be more adaptable to meet future demands.

Amidst these challenges, additive manufacturing (AM) has emerged as a pivotal technology. It has gained significant momentum post-pandemic, transforming the industrial landscape. Additive manufacturing is no longer a niche process; it's now at the forefront of industrial innovation, evidenced by a surge in patents related to new materials, processes, and machinery. This rapid evolution signifies that additive manufacturing is reaching a peak in productivity. Our research delves into optimizing multi-material additive manufacturing through real-time simulation, an approach that could revolutionize manufacturing adaptability and efficiency.

Recent advancements in AI and robotics, particularly due to the GPU revolution and the development of new AI models and machine learning methods like Generative Adversarial Networks (GANs) and reinforcement learning, are creating groundbreaking opportunities. These innovations, although initially distinct, offer valuable insights for various fields, including manufacturing.

In this paper, we propose viewing digital fabrication machines, such as 3D printers, as a type of robot. While there are clear distinctions between conventional 3D printers and robots — with robots typically operating in complex, unpredictable environments and 3D printers functioning in more controlled settings — the line blurs when it comes to dynamic applications. For instance, when dealing with complex material compounds or printing on moving targets, additive manufacturing becomes more akin to robotics, requiring advanced algorithms for effective operation.

From this perspective, we explore the potential of fast simulation technologies. These technologies can address the existing bottlenecks in reinforcement learning in additive manufacturing and expedite the rapid prototyping process. By enabling quick simulation of machine programs, we can anticipate and prevent potential failures. This approach is not only beneficial for educational purposes but also makes complex additive manufacturing processes more accessible through a trial-and-error methodology.

In this paper we are working with a theoritical machine capable of MMAM deposition with no limitations. We assume this arbitrary machine to be capable of depositing powder, melted comound, resin.

Each material requiring different processes to be deposited, they may have different deposition conditions such as melt temperature, pressure, velocity in addition to their chemical composition. Moreover, material deposition could include another material interface to ensure the compatibility between two different materials.

For example POM is a thermoplastic with a very low friction coefficient. Thus it very difficult to print on a FFF printer due to adhesion problems. However combining POM with a chemically compatible polymer could result in new design opporunity.








## Physical modeling of MMAM

Navier stokes

Heat transfer

Viscoelastic material




<!-- Granular material
Chemical reactivity

Focusing ond viscoelasitc materials and material deposition. -->






Discrete element method -> 



Position based dynamics..
SPH..
EUlerian methods..
Lattice boltzman methods..



Digital fabrication process automation : 

- 1 : Gcode program written by hand
- 2 : 3D model -> slicing -> Gcode execution -> Data analysis and iteration
- 2.5 : 3D model -> slicing -> Gcode execution -> Partially automated quality feedback loop -> Data analysis and iteration
- 3 : 3D model -> Slicing -> Simulation -> Control loop and defect correction -> Data analysis and iteration
- 4 : Objective driven digital fabrication


Task list to acheive level 4
Defect detection
Loop feddback
Simulation Gym environment




</font>