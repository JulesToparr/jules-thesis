# Projets en cours

## Développement logiciel

- [Merlin engine](./merlin.md) Moteur de calcul et de rendu basée sur le GPU (OpenGL)


## Experiences

- [Imprimante multi-matière E3D Toolchanger](./toolchanger.md)
- [Amélioration de la fabricabilité du POM en FFF](./pom.coating.md)
- [Characterisation du phénomène de Warping](./warping.md)

## Conception et prototypage

- [ ] I plan to design and build my own Inkjet printhead to 3D print using high viscosity materials. This may be also interesting to print particle charged resin.

[DIY inkjet printhead](https://hackaday.io/project/167446-diy-inkjet-printer)

## Partenariats

- [SoSi](./sosi.md)
- [Convect](./convect.md)

